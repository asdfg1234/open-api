/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

import com.jimi.exception.format.ExceptionFormatter;
import com.jimi.exception.format.TextExceptionFormatter;

/**
 * @FileName SysParamsCheckException.java
 * @Description:
 *
 * @Date 2017年4月25日 上午11:33:08
 * @author li.shangzhi
 * @version 1.0
 */
public class SysParamsCheckException extends SysRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4860747877384304446L;

	public SysParamsCheckException(IErrorCode errorCode) {
		super(errorCode);
	}

	public SysParamsCheckException(IErrorCode errorCode, Throwable cause) {
		super(errorCode, new Throwable(cause.getMessage()));
	}

	public SysParamsCheckException(IErrorCode errorCode, String message) {
		super(errorCode, new Throwable(message));
	}

	public SysParamsCheckException(String message, Throwable cause) {
		super(message, cause);
	}

	public SysParamsCheckException(String message) {
		super(message);
	}

	public SysParamsCheckException(Throwable cause) {
		super(cause);
	}

	@Override
	public void printStackTrace(PrintStream s) {
		synchronized (s) {
			printStackTrace(new PrintWriter(s));
		}
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		synchronized (s) {
			ExceptionFormatter exceptionFormatter = new TextExceptionFormatter(this, errorCode);
			s.println(exceptionFormatter.Format());
			s.flush();
		}
	}

}
