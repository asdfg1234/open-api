/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.exception.format;

import java.util.List;

import com.jimi.exception.IErrorCode;
import com.jimi.exception.SysRuntimeException;

/**
 * @FileName TextExceptionFormatter.java
 * @Description:
 *
 * @Date 2017年4月25日 上午11:34:25
 * @author li.shangzhi
 * @version 1.0
 */
public class TextExceptionFormatter extends ExceptionFormatter {

	private Exception exception;
	private IErrorCode errorCode;
	private List<String> errorstackTraces;

	public TextExceptionFormatter(Exception exception) {
		super(exception);
		this.exception = exception;
	}

	public TextExceptionFormatter(Exception exception, IErrorCode errorCode) {
		super(exception);
		this.exception = exception;
		this.errorCode = errorCode;
	}

	@Override
	protected String writeDescription() {
		StringBuilder stringBuilder = new StringBuilder("\n");
		stringBuilder.append("异常简述：");
		stringBuilder.append(errorCode != null ? errorCode.getErrorMsg() : "");
		stringBuilder.append("\n");
		stringBuilder.append("异常编码：");
		stringBuilder.append(errorCode != null ? errorCode.getCode() : "");
		if (this.exception.getMessage() != null && this.exception.getMessage() != "") {
			stringBuilder.append("\n");
			stringBuilder.append("异常详情：");
			stringBuilder.append(this.exception.getMessage());
		}
		return stringBuilder.toString();
	}

	@Override
	protected String writeException(Exception exceptionType) {
		StringBuilder stringBuilder = new StringBuilder("\n");
		if (exceptionType instanceof SysRuntimeException) {
			SysRuntimeException myException = (SysRuntimeException) exceptionType;
			errorstackTraces = myException.getErrorstackTraces();
		}
		if (errorstackTraces != null && errorstackTraces.size() > 0) {
			int errorPointsSize = errorstackTraces.size();
			stringBuilder.append("异常栈：");
			for (int i = 0; i < errorPointsSize; i++) {
				stringBuilder.append(errorstackTraces.get(i));
				stringBuilder.append("\n");
			}
		}
		return stringBuilder.toString();
	}

}
