/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.exception.format;

/**
 * @FileName ExceptionFormatter.java
 * @Description:
 *
 * @Date 2017年4月25日 上午11:33:54
 * @author li.shangzhi
 * @version 1.0
 */
public abstract class ExceptionFormatter {
	
	private Exception exception;

	protected ExceptionFormatter(Exception exception) {
		this.exception = exception;
	}

	public String Format() {
		StringBuffer msg = new StringBuffer();
		msg.append(writeDescription());
		msg.append(writeException(this.exception));
		return msg.toString();
	}

	protected abstract String writeDescription();

	protected abstract String writeException(Exception exceptionType);

}
