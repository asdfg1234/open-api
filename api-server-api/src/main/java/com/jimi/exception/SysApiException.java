/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * @FileName ApiException.java
 * @Description:
 *
 * @Date 2017年4月10日 下午2:58:56
 * @author li.shangzhi
 * @version 1.0
 */
public class SysApiException extends SysException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SysApiException(IErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public SysApiException(IErrorCode errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
	}

	public SysApiException(String message, IErrorCode errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public SysApiException(String message, Throwable cause, IErrorCode errorCode) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	@Override
	public String getMessage() {
		String message = super.getMessage();
		Throwable cause = getCause();
		if (cause != null) {
			message = message + ";nested Exception is " + cause;
		}
		return message;
	}

	@Override
	public void printStackTrace(PrintStream s) {
		synchronized (s) {
			printStackTrace(new PrintWriter(s));
		}
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		StringBuilder stringBuilder = new StringBuilder("\n");
		stringBuilder.append("异常描述：");
		stringBuilder.append(errorCode.getErrorMsg());
		stringBuilder.append("\n");
		stringBuilder.append("异常编码：");
		stringBuilder.append(errorCode.getCode());
		synchronized (s) {
			s.println(stringBuilder.toString());
			s.flush();
		}
	}

}
