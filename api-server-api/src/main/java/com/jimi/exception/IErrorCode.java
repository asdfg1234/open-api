/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.exception;

/**
 * @FileName IErrorCode.java
 * @Description:
 *
 * @Date 2017年4月25日 上午11:23:19
 * @author li.shangzhi
 * @version 1.0
 */
public interface IErrorCode {

	/**
	 * @Title: getCode
	 * @Description: 获取错误码
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月25日 上午11:25:53
	 */
	public int getCode();

	/**
	 * @Title: getErrorKey
	 * @Description: 国际化资源描述Key
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月25日 上午11:24:25
	 */
	public String getErrorKey();

	/**
	 * @Title: getErrorMsg
	 * @Description: 错误描述，不需要国际化时，使用该方法获取错误描述
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月25日 上午11:24:50
	 */
	public String getErrorMsg();
}
