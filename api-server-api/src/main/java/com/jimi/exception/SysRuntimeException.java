/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * @FileName SysRuntimeException.java
 * @Description:
 *
 * @Date 2017年4月25日 上午11:30:23
 * @author li.shangzhi
 * @version 1.0
 */
public class SysRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected IErrorCode errorCode;
	protected List<String> errorstackTraces;

	public SysRuntimeException(IErrorCode errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public SysRuntimeException(IErrorCode errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
		if (cause.getStackTrace() != null) {
			if (errorstackTraces == null) {
				errorstackTraces = new ArrayList<>(20);
			}
			StackTraceElement[] traceElements = cause.getStackTrace();
			for (int i = 0; i < traceElements.length; i++) {
				errorstackTraces.add(traceElements[i].toString());
				if (i > 12) {
					break;
				}
			}
		} else {
			errorstackTraces = new ArrayList<>(20);
		}
	}

	public SysRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public SysRuntimeException(String message) {
		super(message);
	}

	public SysRuntimeException(Throwable cause) {
		super(cause);
	}

	public IErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(IErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public List<String> getErrorstackTraces() {
		return errorstackTraces;
	}

	public void setErrorstackTraces(List<String> errorstackTraces) {
		this.errorstackTraces = errorstackTraces;
	}

}