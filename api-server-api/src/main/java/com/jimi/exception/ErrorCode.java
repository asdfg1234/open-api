/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月11日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.exception;

/**
 * @FileName ErrorCode.java
 * @Description: 错误码
 *
 * @Date 2017年4月11日 下午2:34:44
 * @author li.shangzhi
 * @version 1.0
 */
public enum ErrorCode implements IErrorCode {

	ISBUSY(-1, "isBusy", "系统繁忙"),
	SUCCESS(0, "success", "成功"),
	PARAM_ERROR(1001, "param_error", "参数验证不合法"),
	INVALID_USER_ERROR(1002, "invalid_user_error", "非法用户"),
	INVALID_DEVICE_ERROR(1002, "invalid_device_error", "非法设备"),
	MULTI_SUBMIT_ERROR(1003, "multi_submit_error", "重复提交操作"),
	TOKEN_ERROR(1004, "token_error", "非法访问,token异常!"),
	IP_LIMIT_ERROR(1005, "ip_limit_error", "非法访问,ip访问超过限定次数!"),
	REQ_RATE_TOO_HIGH_ERROR(1006, "req_rate_too_high_error", "非法访问,请求频率过高!"),
	REQ_METHOD_ERROR(1007, "req_method_error", "非法访问,请求方法错误!"),
	REFERER_ERROR(1008, "referer_error","非法访问,来路异常!");
	
	private final int number;

	private final String errorKey;

	private final String errorMsg;

	private ErrorCode(int number, String errorKey, String errorMsg) {
		this.number = number;
		this.errorKey = errorKey;
		this.errorMsg = errorMsg;
	}

	@Override
	public int getCode() {
		return number;
	}

	@Override
	public String getErrorKey() {
		return errorKey;
	}

	@Override
	public String getErrorMsg() {
		return errorMsg;
	}

}
