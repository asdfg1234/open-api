/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月24日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.commons;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @FileName DatetimeUtil.java
 * @Description:
 *
 * @Date 2017年4月24日 下午2:04:31
 * @author li.shangzhi
 * @version 1.0
 */
public class DatetimeUtil {

	public static final String LONG_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	public DatetimeUtil() {
	}

	public static String getCurrentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat(LONG_DATE_TIME_PATTERN);
		String result = formatter.format(new Date());
		return result;
	}
	
	public static String getCurrentDate(String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String result = formatter.format(new Date());
		return result;
	}

	public static String getFormatDatetime() throws Exception {
		GregorianCalendar gCalendar = new GregorianCalendar();
		SimpleDateFormat formatter = new SimpleDateFormat(LONG_DATE_TIME_PATTERN);
		String strDateTime;
		try {
			strDateTime = formatter.format(gCalendar.getTime());
		} catch (Exception ex) {
			System.out.println("Error Message:".concat(String.valueOf(String.valueOf(ex.toString()))));
			String s = null;
			return s;
		}
		return strDateTime;
	}

	public static Date StringToDate(String s) {
		Date date = new Date(0L);
		try {
			Calendar calendar = Calendar.getInstance();
			int year = Integer.parseInt(s.substring(0, s.indexOf("-")));
			int month = Integer.parseInt(s.substring(s.indexOf("-") + 1, s.lastIndexOf("-")));
			int day = Integer.parseInt(s.substring(s.lastIndexOf("-") + 1, s.length()));
			calendar.set(year, month - 1, day);
			date.setTime(calendar.getTime().getTime());
		} catch (Exception e) {
			System.out.println(String.valueOf((new StringBuffer(String.valueOf(e))).append(",").append(s)));
		}
		return date;
	}

	public static String DateToString(Date dt, String fmtStr) {
		SimpleDateFormat format = new SimpleDateFormat(fmtStr);
		return format.format(dt);
	}

	public static String DateToString(Date dt) {
		SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
		return format.format(dt);
	}
}
