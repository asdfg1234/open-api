 /*
 * 版本信息
 
 * 日期 2017-05-09 17:58:45
 
 * 版权声明Copyright (C) 2016- 2017 jimi Information Technology Co.,Ltd
 * All Rights Reserved.
 * 本软件为深圳市jimi公司开发研制，未经本公司正式书面同意，其他任何个人、团体不得
 * 使用、复制、修改或发布本软件。
 */
package com.jimi.app.dto.output;

import java.util.Date;
import com.jimi.commons.DatetimeUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.jimi.dto.base.OutputDto;

/**
 * ApiAppPushSetOutputDto
 * @author jimi team
 * @Date 创建时间：2017-05-09 17:58:46
 */
public class ApiAppPushSetOutputDto1 extends OutputDto {
	
	private static final long serialVersionUID = 1L;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 用户账号
	 */
	private String account;
	/**
	 * 用户描述与appinfo一样
	 */
	private String appDesc;
	/**
	 * 推送的url地址
	 */
	private String pushUrl;
	/**
	 * 是否启用，1-启用 0-不启用
	 */
	private Integer enabled;

	public ApiAppPushSetOutputDto1(){
	}

	public ApiAppPushSetOutputDto1(
		Integer userId
	){
		this.userId = userId;
	}
	public void setUserId(Integer value) {
		value = value == null ? 0 : value;
		this.userId = value;
	}
	
	public Integer getUserId() {
		return this.userId == null ? 0 : this.userId;
	}
	public void setAccount(String value) {
		this.account = value;
	}
	
	public String getAccount() {
		return this.account == null ? null : this.account.trim();
	}
	public void setAppDesc(String value) {
		this.appDesc = value;
	}
	
	public String getAppDesc() {
		return this.appDesc == null ? null : this.appDesc.trim();
	}
	public void setPushUrl(String value) {
		this.pushUrl = value;
	}
	
	public String getPushUrl() {
		return this.pushUrl == null ? null : this.pushUrl.trim();
	}
	public void setEnabled(Integer value) {
		value = value == null ? 0 : value;
		this.enabled = value;
	}
	
	public Integer getEnabled() {
		return this.enabled == null ? 0 : this.enabled;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}

