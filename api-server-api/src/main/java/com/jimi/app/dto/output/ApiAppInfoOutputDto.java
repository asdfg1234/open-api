/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月24日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.dto.output;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.commons.DatetimeUtil;
import com.jimi.dto.base.OutputDto;

/**
 * @FileName ApiAppInfoOutputDto.java
 * @Description:
 *
 * @Date 2017年4月24日 下午6:15:45
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiAppInfoOutputDto extends OutputDto {

	private static final long serialVersionUID = 1L;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 用户账号
	 */
	private String account;
	/**
	 * 项目标识
	 */
	private String projectCode;
	/**
	 * proejct_code+user_id生成
	 */
	private String appKey;
	/**
	 * app密码随机生成
	 */
	private String appSecret;
	/**
	 * app描述
	 */
	private String appDesc;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 最后更新时间
	 */
	private Date lastUdpateTime;
	/**
	 * 是否启用，1-启用 0-不启用
	 */
	private Boolean enabled;

	public ApiAppInfoOutputDto() {
	}

	public ApiAppInfoOutputDto(Integer userId, String projectCode) {
		this.userId = userId;
		this.projectCode = projectCode;
	}

	public void setUserId(Integer value) {
		value = value == null ? 0 : value;
		this.userId = value;
	}

	public Integer getUserId() {
		return this.userId == null ? 0 : this.userId;
	}

	public void setAccount(String value) {
		this.account = value;
	}

	public String getAccount() {
		return this.account == null ? null : this.account.trim();
	}

	public void setProjectCode(String value) {
		this.projectCode = value;
	}

	public String getProjectCode() {
		return this.projectCode == null ? null : this.projectCode.trim();
	}

	public void setAppKey(String value) {
		this.appKey = value;
	}

	public String getAppKey() {
		return this.appKey == null ? null : this.appKey.trim();
	}

	public void setAppSecret(String value) {
		this.appSecret = value;
	}

	public String getAppSecret() {
		return this.appSecret == null ? null : this.appSecret.trim();
	}

	public void setAppDesc(String value) {
		this.appDesc = value;
	}

	public String getAppDesc() {
		return this.appDesc == null ? null : this.appDesc.trim();
	}

	public void setCreateTime(Date value) {
		this.createTime = value;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public String getStringCreateTime() {
		if (this.createTime == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.createTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setLastUdpateTime(Date value) {
		this.lastUdpateTime = value;
	}

	public Date getLastUdpateTime() {
		return this.lastUdpateTime;
	}

	public String getStringLastUdpateTime() {
		if (this.lastUdpateTime == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.lastUdpateTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
