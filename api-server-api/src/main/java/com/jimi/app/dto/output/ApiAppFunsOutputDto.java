/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月24日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.dto.output;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.dto.base.OutputDto;

/**
 * @FileName ApiAppFunsOutputDto.java
 * @Description:
 *
 * @Date 2017年4月24日 下午6:15:36
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiAppFunsOutputDto extends OutputDto {

	private static final long serialVersionUID = 1L;
	/**
	 * app_key
	 */
	private String appKey;
	/**
	 * 方法ID
	 */
	private String funId;
	/**
	 * 方法描述
	 */
	private String funName;
	/**
	 * 每秒最高访问量
	 */
	private Integer reqPerSecond;
	/**
	 * 每天最高访问量
	 */
	private Integer reqPerDay;

	public ApiAppFunsOutputDto() {
	}

	public ApiAppFunsOutputDto(String appKey, String funId) {
		this.appKey = appKey;
		this.funId = funId;
	}

	public void setAppKey(String value) {
		this.appKey = value;
	}

	public String getAppKey() {
		return this.appKey == null ? null : this.appKey.trim();
	}

	public void setFunId(String value) {
		this.funId = value;
	}

	public String getFunId() {
		return this.funId == null ? null : this.funId.trim();
	}

	public void setFunName(String value) {
		this.funName = value;
	}

	public String getFunName() {
		return this.funName == null ? null : this.funName.trim();
	}

	public void setReqPerSecond(Integer value) {
		value = value == null ? 0 : value;
		this.reqPerSecond = value;
	}

	public Integer getReqPerSecond() {
		return this.reqPerSecond == null ? 0 : this.reqPerSecond;
	}

	public void setReqPerDay(Integer value) {
		value = value == null ? 0 : value;
		this.reqPerDay = value;
	}

	public Integer getReqPerDay() {
		return this.reqPerDay == null ? 0 : this.reqPerDay;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
