/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月9日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.dto.output;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.dto.base.OutputDto;

/**
 * @FileName ApiAppPushSetOutputDto.java
 * @Description:
 *
 * @Date 2017年5月9日 下午6:02:24
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiAppPushSetOutputDto extends OutputDto {

	private static final long serialVersionUID = 1L;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 用户账号
	 */
	private String account;
	/**
	 * 用户描述与appinfo一样
	 */
	private String appDesc;
	/**
	 * 推送的url地址
	 */
	private String pushUrl;
	/**
	 * 是否启用，1-启用 0-不启用
	 */
	private Boolean enabled;

	public ApiAppPushSetOutputDto() {
	}

	public ApiAppPushSetOutputDto(Integer userId) {
		this.userId = userId;
	}

	public void setUserId(Integer value) {
		value = value == null ? 0 : value;
		this.userId = value;
	}

	public Integer getUserId() {
		return this.userId == null ? 0 : this.userId;
	}

	public void setAccount(String value) {
		this.account = value;
	}

	public String getAccount() {
		return this.account == null ? null : this.account.trim();
	}

	public void setAppDesc(String value) {
		this.appDesc = value;
	}

	public String getAppDesc() {
		return this.appDesc == null ? null : this.appDesc.trim();
	}

	public void setPushUrl(String value) {
		this.pushUrl = value;
	}

	public String getPushUrl() {
		return this.pushUrl == null ? null : this.pushUrl.trim();
	}

	public void setEnabled(Boolean value) {
		this.enabled = value;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}