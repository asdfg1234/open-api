/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.dto.input;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.dto.base.InputDto;

/**
 * @FileName ApiOpenFunctionsInputDto.java
 * @Description:
 *
 * @Date 2017年4月25日 下午2:55:33
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiOpenFunctionsInputDto extends InputDto {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private String id;
	/**
	 * 路由表Id
	 */
	private String routeId;
	/**
	 * 方法名标识
	 */
	private String method;
	/**
	 * 方法完整请求路径
	 */
	private String url;
	/**
	 * 需要数据权限
	 */
	private String dataFilter;
	/**
	 * 方法描述
	 */
	private String methodDesc;
	/**
	 * 是否启用，1-启用 0-不启用
	 */
	private Boolean enabled;
	/**
	 * 是否认证，1-认证 0-不认证
	 */
	private Boolean auth;
	/**
	 * 是否限流，1-限流 0-不限流
	 */
	private Boolean limit;

	public ApiOpenFunctionsInputDto() {
	}

	public ApiOpenFunctionsInputDto(String id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return this.id == null ? null : this.id.trim();
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public String getRouteId() {
		return this.routeId == null ? null : this.routeId.trim();
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getMethod() {
		return this.method == null ? null : this.method.trim();
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return this.url == null ? null : this.url.trim();
	}

	public void setDataFilter(String dataFilter) {
		this.dataFilter = dataFilter;
	}

	public String getDataFilter() {
		return this.dataFilter == null ? null : this.dataFilter.trim();
	}

	public void setMethodDesc(String methodDesc) {
		this.methodDesc = methodDesc;
	}

	public String getMethodDesc() {
		return this.methodDesc == null ? null : this.methodDesc.trim();
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getAuth() {
		return auth;
	}

	public void setAuth(Boolean auth) {
		this.auth = auth;
	}

	public Boolean getLimit() {
		return limit;
	}

	public void setLimit(Boolean limit) {
		this.limit = limit;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
