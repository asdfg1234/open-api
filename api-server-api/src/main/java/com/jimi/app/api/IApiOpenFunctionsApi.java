/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api;

import java.util.List;

import com.jimi.app.dto.input.ApiOpenFunctionsInputDto;
import com.jimi.app.dto.output.ApiOpenFunctionsOutputDto;
import com.jimi.dto.base.PageModel;

/**
 * @FileName IApiOpenFunctionsApi.java
 * @Description:
 *
 * @Date 2017年4月25日 下午2:50:23
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiOpenFunctionsApi {

	/**
	 * 通过id删除记录
	 */
	public int removeById(String id);

	/**
	 * 保存单条记录
	 */
	public String insert(ApiOpenFunctionsInputDto apiOpenFunctionsDto);

	/**
	 * 获取分页数据
	 */
	public PageModel<ApiOpenFunctionsOutputDto> findPage(ApiOpenFunctionsInputDto pageInputDto, PageModel pageModel);
	
	/**
	 * 获取集合
	 */
	public List<ApiOpenFunctionsOutputDto> findList(ApiOpenFunctionsInputDto inputDto);

	/**
	 * 更新记录
	 */
	public int update(ApiOpenFunctionsInputDto apiOpenFunctionsDto);

	/**
	 * 通过id查询数据
	 */
	public ApiOpenFunctionsOutputDto getById(String id);
	
	/**
	 * 通过method查询数据
	 */
	public ApiOpenFunctionsOutputDto getByMethod(String method);
}
