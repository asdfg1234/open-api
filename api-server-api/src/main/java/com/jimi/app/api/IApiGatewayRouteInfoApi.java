/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.app.api;

import java.util.List;

import com.jimi.app.dto.input.ApiGatewayRouteInfoInputDto;
import com.jimi.app.dto.output.ApiGatewayRouteInfoOutputDto;

/**
 * @FileName IApiGatewayRouteInfoApi.java
 * @Description: 
 *
 * @Date 2017年4月25日 下午2:50:16
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiGatewayRouteInfoApi {

	/**
	 * 通过id删除记录
	 */
	public int removeById(String id);
	
	/**
	 * 获取集合
	 */
	public List<ApiGatewayRouteInfoOutputDto> findList(ApiGatewayRouteInfoInputDto inputDto);
	
	/**
	 * 保存单条记录
	 */
	public int insert(ApiGatewayRouteInfoInputDto apiGatewayRouteInfoDto);
	
	/**
	 * 更新记录
	 */
	public int update(ApiGatewayRouteInfoInputDto apiGatewayRouteInfoDto);
	
	/**
	 * 通过id查询数据
	 */
	public ApiGatewayRouteInfoOutputDto getById(String id);
}
