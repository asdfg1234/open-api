/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api;

import java.util.List;

import com.jimi.app.dto.input.ApiAppInfoInputDto;
import com.jimi.app.dto.output.ApiAppInfoOutputDto;
import com.jimi.dto.base.PageModel;

/**
 * @FileName IApiAppInfoApi.java
 * @Description:
 *
 * @Date 2017年4月25日 下午2:49:59
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiAppInfoApi {

	/**
	 * 通过id删除记录
	 */
	public int removeById(String id);

	/**
	 * 保存单条记录
	 */
	public int insert(ApiAppInfoInputDto apiAppInfoDto);

	/**
	 * 获取分页数据
	 */
	public PageModel<ApiAppInfoOutputDto> findPage(ApiAppInfoInputDto pageInputDto, PageModel pageModel);
	
	/**
	 * 获取集合
	 */
	public List<ApiAppInfoOutputDto> findList(ApiAppInfoInputDto inputDto);

	/**
	 * 更新记录
	 */
	public int update(ApiAppInfoInputDto apiAppInfoDto);

	/**
	 * 通过id查询数据
	 */
	public ApiAppInfoOutputDto getById(String id);
	
	/**
	 * 通过appKey查询数据
	 */
	public ApiAppInfoOutputDto getByAppKey(String appKey);
}
