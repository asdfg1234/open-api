/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月9日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api;

import java.util.List;

import com.jimi.app.dto.input.ApiAppPushSetInputDto;
import com.jimi.app.dto.output.ApiAppPushSetOutputDto;

/**
 * @FileName IApiAppPushSetApi.java
 * @Description:
 *
 * @Date 2017年5月9日 下午7:55:25
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiAppPushSetApi {

	/**
	 * 获取集合
	 */
	public List<ApiAppPushSetOutputDto> findList(ApiAppPushSetInputDto inputDto);

	/**
	 * 通过id删除记录
	 */
	public int removeById(String id);

	/**
	 * 保存单条记录
	 */
	public int insert(ApiAppPushSetInputDto apiAppPushSetDto);

	/**
	 * 更新记录
	 */
	public int update(ApiAppPushSetInputDto apiAppPushSetDto);

	/**
	 * 通过id查询数据
	 */
	public ApiAppPushSetOutputDto getById(String id);
}
