/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.app.api;

import java.util.List;

import com.jimi.app.dto.input.ApiAppFunsInputDto;
import com.jimi.app.dto.output.ApiAppFunsOutputDto;
import com.jimi.dto.base.PageModel;

/**
 * @FileName IApiAppFunsApi.java
 * @Description: 
 *
 * @Date 2017年4月25日 下午2:49:49
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiAppFunsApi {

	/**
	 * 通过id删除记录
	 */
	public int removeById(String id);
	
	/**
	 * 获取分页数据
	 */
	public PageModel<ApiAppFunsOutputDto> findPage(ApiAppFunsInputDto pageInputDto, PageModel pageModel);
	
	/**
	 * 获取集合数据
	 */
	public List<ApiAppFunsOutputDto> findList(ApiAppFunsInputDto inputDto);

	/**
	 * 保存单条记录
	 */
	public int insert(ApiAppFunsInputDto apiAppFunsDto);

	/**
	 * 更新记录
	 */
	public int update(ApiAppFunsInputDto apiAppFunsDto);

	/**
	 * 通过id查询数据
	 */
	public ApiAppFunsOutputDto getById(String id);
	
	/**
	 * 通过appKey和funId查询数据
	 */
	public ApiAppFunsOutputDto getByAppFun(String appKey, String funId);
}
