/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月28日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api;

import com.jimi.app.dto.input.ApiAppAccessingLogInputDto;

/**
 * @FileName IApiAppAccessingLogApi.java
 * @Description:
 *
 * @Date 2017年4月28日 下午2:50:39
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiAppAccessingLogApi {

	/**
	 * 通过id删除记录
	 */
	public int removeById(String id);

	/**
	 * 保存单条记录
	 */
	public String insert(ApiAppAccessingLogInputDto apiAppAccessingLogDto);

	/**
	 * 更新记录
	 */
	public int update(ApiAppAccessingLogInputDto apiAppAccessingLogDto);

}
