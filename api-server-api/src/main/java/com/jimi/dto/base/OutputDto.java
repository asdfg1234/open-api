/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月24日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.dto.base;

/**
 * @FileName OutputDto.java
 * @Description:
 *
 * @Date 2017年4月24日 下午6:10:20
 * @author li.shangzhi
 * @version 1.0
 */
public class OutputDto extends BaseDto {

	private static final long serialVersionUID = 1L;

}