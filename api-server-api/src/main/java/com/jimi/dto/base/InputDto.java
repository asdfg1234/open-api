/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月24日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.dto.base;

/**
 * @FileName InputDto.java
 * @Description:
 *
 * @Date 2017年4月24日 下午6:08:07
 * @author li.shangzhi
 * @version 1.0
 */
public class InputDto extends BaseDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 远程调用者IP
	 */
	private String remoteHost;

	/**
	 * 远程调用者端口
	 */
	private String remotePort;

	private UserContext userContext;

	public String getRemoteHost() {
		try {
			// RpcContext rpcContext = RpcContext.getContext();
			// remoteHost = rpcContext.getRemoteHost();
		} catch (Exception ex) {
		}
		return remoteHost;
	}

	public String getRemotePort() {
		try {
			// RpcContext rpcContext = RpcContext.getContext();
			// remotePort = rpcContext.getRemotePort();
		} catch (Exception ex) {
		}
		return remotePort;
	}

	public UserContext getUserContext() {
		return userContext;
	}

	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}

}