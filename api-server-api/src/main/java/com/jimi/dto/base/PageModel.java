/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.dto.base;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * @FileName PageModel.java
 * @Description: 分页对象
 *
 * @Date 2017年4月25日 下午2:38:27
 * @author li.shangzhi
 * @version 1.0
 */
public class PageModel<T> extends BaseDto {

	private static final long serialVersionUID = 1L;

	/**
	 * 每页的记录数
	 */
	private int limit = 20;

	/**
	 * 当前页中存放的数据
	 */
	private List<T> items;

	/**
	 * 总记录数
	 */
	private int totalCount;

	/**
	 * 页数
	 */
	private int pageCount;

	/**
	 * 跳转页数
	 */
	private int page = 1;

	/**
	 * 排序字段
	 */
	private String orderSort;

	/**
	 * 排序方式
	 */
	private Order order = Order.DESC;

	public enum Order {
		ASC("ASC"), DESC("DESC");
		private String value;

		private Order(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public PageModel() {

	}

	public PageModel(int page, int totalCount) {
		this.page = page;
		this.totalCount = totalCount;
		this.pageCount = getTotalPageCount();
	}

	public PageModel(int page, int limit, int totalCount) {
		this.page = page;
		this.limit = limit;
		this.totalCount = totalCount;
		this.pageCount = getTotalPageCount();
	}

	public PageModel(int page, int limit, int totalCount, List<T> items) {
		this.page = page;
		this.limit = limit;
		this.totalCount = totalCount;
		this.pageCount = getTotalPageCount();
		this.items = items;
	}

	/**
	 * 取总页数
	 */
	private final int getTotalPageCount() {
		if (totalCount % limit == 0)
			return totalCount / limit;
		else
			return totalCount / limit + 1;
	}

	/**
	 * 取每页数据数
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * 取当前页中的记录.
	 */
	public Object getResult() {
		return items;
	}

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getOrderSort() {
		if (StringUtils.isNotEmpty(orderSort)) {
			String orderStr = orderSort.split("\\.")[0];
			StringBuilder sb = new StringBuilder();
			char[] buffer = orderStr.toCharArray();
			for (int i = 0; i < buffer.length; i++) {
				char ch = buffer[i];
				if (Character.isUpperCase(ch)) {
					sb.append("_").append(Character.toLowerCase(ch));
				} else {
					sb.append(ch);
				}
			}
			return sb.toString();
		}
		return "";
	}

	public void setOrderSort(String orderSort) {
		this.orderSort = orderSort;
	}

	@Override
	public String toString() {
		return "PageModel [limit=" + limit + ", totalCount=" + totalCount + ", pageCount=" + pageCount + ", page=" + page + ", orderSort="
				+ orderSort + ", order=" + order + "]";
	}

	public Order getOrder() {
		if (StringUtils.isNotEmpty(orderSort)) {
			if (orderSort.split("\\.")[1].equalsIgnoreCase(Order.ASC.getValue())) {
				return Order.ASC;
			} else {
				return Order.DESC;
			}
		}
		return Order.DESC;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}
