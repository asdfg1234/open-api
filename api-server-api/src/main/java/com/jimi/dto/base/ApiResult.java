/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月24日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.dto.base;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @FileName ApiResult.java
 * @Description:
 *
 * @Date 2017年4月24日 下午6:06:24
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiResult<T> extends OutputDto {

	private static final long serialVersionUID = 1L;

	private int resultCode;

	private String resultMsg;

	private T result;

	public ApiResult() {

	}

	public ApiResult(int resultCode, String resultMsg, T result) {
		super();
		this.resultCode = resultCode;
		this.resultMsg = resultMsg;
		this.result = result;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
