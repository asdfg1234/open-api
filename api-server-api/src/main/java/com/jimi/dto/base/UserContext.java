/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月24日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.dto.base;

/**
 * @FileName UserContext.java
 * @Description: 用户客户端环境上下文
 *
 * @Date 2017年4月24日 下午6:08:29
 * @author li.shangzhi
 * @version 1.0
 */
public class UserContext extends InputDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 客户端ip(必填) **/
	private String clientIp;

	/** User-Agent **/
	private String userAgent;

	/** 请求方法(必填) **/
	private String requestMethod;

	/** sessionId(必填) **/
	private String sessionId;

	/** 来路 **/
	private String referer;

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "UserClientContext [clientIp=" + clientIp + ", userAgent=" + userAgent + ", referer=" + referer + ", requestMethod="
				+ requestMethod + ", sessionId=" + sessionId + "]";
	}
}