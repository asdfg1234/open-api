/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月26日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.dto.output;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.dto.base.OutputDto;

/**
 * @FileName GpsPointOutputDto.java
 * @Description:
 *
 * @Date 2017年4月26日 下午4:37:07
 * @author li.shangzhi
 * @version 1.0
 */
public class GpsPointOutputDto extends OutputDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * imei
	 */
	private String imei;

	/**
	 * 车牌号
	 */
	private String vehicleNumber;
	/**
	 * 纬度
	 */
	private double lat;
	/**
	 * 经度
	 */
	private double lng;
	/**
	 * GPS时间
	 */
	private Date gpsTime;
	/**
	 * 方位
	 */
	private int direction;
	/**
	 * GPS速度
	 */
	private double gpsSpeed;

	/**
	 * 地标名称
	 */
	private String geoname;

	private String speedType;

	// 定位类型 1是卫星定位 2是基站定位 3是wifi定位
	private int posType;

	public int getPosType() {
		return posType;
	}

	public void setPosType(int posType) {
		this.posType = posType;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Date getGpsTime() {
		return gpsTime;
	}

	public void setGpsTime(Date gpsTime) {
		this.gpsTime = gpsTime;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public double getGpsSpeed() {
		return gpsSpeed;
	}

	public void setGpsSpeed(double gpsSpeed) {
		this.gpsSpeed = gpsSpeed;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getGeoname() {
		return geoname;
	}

	public void setGeoname(String geoname) {
		this.geoname = geoname;
	}

	public String getSpeedType() {
		return speedType;
	}

	public void setSpeedType(String speedType) {
		this.speedType = speedType;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
