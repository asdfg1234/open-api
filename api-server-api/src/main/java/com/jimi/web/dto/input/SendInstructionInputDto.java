/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.dto.input;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.dto.base.InputDto;

/**
 * @FileName SendInstructionInputDto.java
 * @Description:
 *
 * @Date 2017年7月25日 下午8:06:02
 * @author li.shangzhi
 * @version 1.0
 */
public class SendInstructionInputDto extends InputDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 用户ID
	 */
	private String userId;
	/**
	 * 用户账号
	 */
	private String account;
	/**
	 * 设备imei
	 */
	private String imei;
	/**
	 * 设备机型
	 */
	private String mcType;
	/**
	 * 指令ID
	 */
	private String instId;
	/**
	 * 指令模板
	 */
	private String instTemplate;
	/**
	 * 指令参数
	 */
	private String[] params;
	/**
	 * 是否覆盖已存在的离线指令
	 */
	private boolean isCover;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getMcType() {
		return mcType;
	}

	public void setMcType(String mcType) {
		this.mcType = mcType;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getInstTemplate() {
		return instTemplate;
	}

	public void setInstTemplate(String instTemplate) {
		this.instTemplate = instTemplate;
	}

	public String[] getParams() {
		return params;
	}

	public void setParams(String[] params) {
		this.params = params;
	}

	public boolean isCover() {
		return isCover;
	}

	public void setCover(boolean isCover) {
		this.isCover = isCover;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
