/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月26日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.dto.output;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.dto.base.OutputDto;

/**
 * @FileName LbsPointOutputDto.java
 * @Description:
 *
 * @Date 2017年4月26日 下午4:56:28
 * @author li.shangzhi
 * @version 1.0
 */
public class LbsPointOutputDto extends OutputDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 纬度
	 */
	private double lat;
	/**
	 * 经度
	 */
	private double lng;

	/**
	 * 精准度，值越大越好
	 */
	private int accuracy;

	/**
	 * POI地址
	 */
	private String addr;

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public int getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(int accuracy) {
		this.accuracy = accuracy;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
