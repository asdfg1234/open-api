/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.dto.output;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.dto.base.OutputDto;

/**
 * @FileName InstructionDetailsOutput.java
 * @Description: 
 *
 * @Date 2017年7月25日 下午2:10:00
 * @author li.shangzhi
 * @version 1.0
 */
public class InstructionDetailsOutputDto extends OutputDto {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private Integer id;
	/**
	 * 指令类型ID
	 */
	private Integer typeId;
	/**
	 * 指令顺序
	 */
	private Integer orderCode;
	/**
	 * 名称
	 */
	private String orderName;
	/**
	 * 协议ID
	 */
	private Integer protocolId;
	/**
	 * 协议编码
	 */
	private String protocolCode;
	/**
	 * 内容
	 */
	private String orderContent;
	/**
	 * 说明
	 */
	private String orderExplain;
	/**
	 * 提示
	 */
	private String orderMsg;
	/**
	 * 是否需要验证密码 0:否 1:是
	 */
	private String isUsePwd;
	/**
	 * 是否支持离线指令 0:是否 1:是
	 */
	private String isOffLine;
	/**
	 * 是否等待设备响应 0否 1: 是
	 */
	private String waitRespond;
	/**
	 * 标志(1可用 0不可以)
	 */
	private Integer enabledFlag;

	public void setId(Integer id) {
		id = id == null ? 0 : id;
		this.id = id;
	}

	public Integer getId() {
		return this.id == null ? 0 : this.id;
	}

	public void setTypeId(Integer typeId) {
		typeId = typeId == null ? 0 : typeId;
		this.typeId = typeId;
	}

	public Integer getTypeId() {
		return this.typeId == null ? 0 : this.typeId;
	}

	public void setOrderCode(Integer orderCode) {
		orderCode = orderCode == null ? 0 : orderCode;
		this.orderCode = orderCode;
	}

	public Integer getOrderCode() {
		return this.orderCode == null ? 0 : this.orderCode;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderName() {
		return this.orderName == null ? null : this.orderName.trim();
	}

	public void setProtocolId(Integer protocolId) {
		protocolId = protocolId == null ? 0 : protocolId;
		this.protocolId = protocolId;
	}

	public Integer getProtocolId() {
		return this.protocolId == null ? 0 : this.protocolId;
	}

	public String getProtocolCode() {
		return this.protocolCode == null ? null : this.protocolCode.trim();
	}

	public void setProtocolCode(String protocolCode) {
		this.protocolCode = protocolCode;
	}

	public void setOrderContent(String orderContent) {
		this.orderContent = orderContent;
	}

	public String getOrderContent() {
		return this.orderContent == null ? null : this.orderContent.trim();
	}

	public void setOrderExplain(String orderExplain) {
		this.orderExplain = orderExplain;
	}

	public String getOrderExplain() {
		return this.orderExplain == null ? null : this.orderExplain.trim();
	}

	public void setOrderMsg(String orderMsg) {
		this.orderMsg = orderMsg;
	}

	public String getOrderMsg() {
		return this.orderMsg == null ? null : this.orderMsg.trim();
	}

	public void setIsUsePwd(String isUsePwd) {
		this.isUsePwd = isUsePwd;
	}

	public String getIsUsePwd() {
		return this.isUsePwd == null ? null : this.isUsePwd.trim();
	}

	public void setIsOffLine(String isOffLine) {
		this.isOffLine = isOffLine;
	}

	public String getIsOffLine() {
		return this.isOffLine == null ? null : this.isOffLine.trim();
	}

	public void setWaitRespond(String waitRespond) {
		this.waitRespond = waitRespond;
	}

	public String getWaitRespond() {
		return this.waitRespond == null ? null : this.waitRespond.trim();
	}

	public void setEnabledFlag(Integer enabledFlag) {
		enabledFlag = enabledFlag == null ? 0 : enabledFlag;
		this.enabledFlag = enabledFlag;
	}

	public Integer getEnabledFlag() {
		return this.enabledFlag == null ? 0 : this.enabledFlag;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
