/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年6月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.dto.input;

import com.jimi.dto.base.InputDto;

/**
 * @FileName UserInfoInputDto.java
 * @Description: 
 *
 * @Date 2017年6月12日 下午3:42:48
 * @author li.shangzhi
 * @version 1.0
 */
public class UserInfoInputDto extends InputDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 上级userId
	 */
	private String userId;
	
	/**
	 * 上级账号
	 */
	private String topAccount;
	
	/**
	 * 客户名称
	 */
	private String nickName;
	
	/**
	 * 账号类型
	 */
	private String accountType;
	
	/**
	 * 客户账号
	 */
	private String account;
	
	/**
	 * 客户密码
	 */
	private String password;
	
	/**
	 * 手机号
	 */
	private String phone;
	
	/**
	 * 邮箱
	 */
	private String email;
	
	/**
	 * 联系人
	 */
	private String contact;
	
	/**
	 * 公司名
	 */
	private String companyName;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTopAccount() {
		return topAccount;
	}

	public void setTopAccount(String topAccount) {
		this.topAccount = topAccount;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
