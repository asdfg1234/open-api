/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月26日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.dto.output;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.dto.base.OutputDto;

/**
 * @FileName DeviceInfoOutput.java
 * @Description: 
 *
 * @Date 2017年4月26日 下午4:34:41
 * @author li.shangzhi
 * @version 1.0
 */
public class DeviceInfoOutputDto extends OutputDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 设备IMEI号（16位）
	 */
	private String imei;
	/**
	 * 所属用户ID
	 */
	private String userId;
	/**
	 * 用户账号
	 */
	private String account;
	/**
	 * 设备名称
	 */
	private String deviceName;
	/**
	 * 设备类型
	 */
	private String mcType;
	/**
	 * 机型使用范围(aotomobile:汽车 electromobile:电动车 personal:个人 pet:宠物 plane:飞机
	 * others:其他)
	 */
	private String mcTypeUseScope;
	/**
	 * SIM卡号
	 */
	private String sim;
	/**
	 * 到期时间
	 */
	private Date expiration;
	/**
	 * 激活时间
	 */
	private Date activationTime;
	/**
	 * 备注
	 */
	private String reMark;
	/**
	 * 箱号
	 */
	private String sn;
	/**
	 * 车辆名称
	 */
	private String vehicleName;
	/**
	 * 车辆图标
	 */
	private String vehicleIcon;
	/**
	 * 车牌号
	 */
	private String vehicleNumber;
	/**
	 * 车辆品牌
	 */
	private String vehicleModels;
	/**
	 * 车架号
	 */
	private String carFrame;
	/**
	 * 司机名称
	 */
	private String driverName;
	/**
	 * 司机电话
	 */
	private String driverPhone;
	/**
	 * 司机身份证号码
	 */
	private String idCard;
	/**
	 * 是否启用（0：不启用；1：启用）
	 */
	private Long enabledFlag;
	/**
	 * 0 终身 1 不终身
	 */
	private String isLife;
	/**
	 * bindUserId
	 */
	private String bindUserId;
	/**
	 * 电机发动机号
	 */
	private String engineNumber;
	/**
	 * 超速速度
	 */
	private String overSpeed;
	/**
	 * 安装时间
	 */
	private Date installTime;
	/**
	 * 安装地址
	 */
	private String installAddress;
	/**
	 * 安装公司
	 */
	private String installCompany;
	/**
	 * 安装位置
	 */
	private String installPosition;
	/**
	 * 安装人员
	 */
	private String installPersonnel;
	/**
	 * 安装图片
	 */
	private String installImage;
	/**
	 * iccid
	 */
	private String iccid;
	/**
	 * imsi
	 */
	private String imsi;
	
	/**
	 * orgIdForAttention
	 */
	private String orgIdForAttention;

	public DeviceInfoOutputDto() {
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getMcType() {
		return mcType;
	}

	public void setMcType(String mcType) {
		this.mcType = mcType;
	}

	public String getMcTypeUseScope() {
		return mcTypeUseScope;
	}

	public void setMcTypeUseScope(String mcTypeUseScope) {
		this.mcTypeUseScope = mcTypeUseScope;
	}

	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public Date getActivationTime() {
		return activationTime;
	}

	public void setActivationTime(Date activationTime) {
		this.activationTime = activationTime;
	}

	public String getReMark() {
		return reMark;
	}

	public void setReMark(String reMark) {
		this.reMark = reMark;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getVehicleName() {
		return vehicleName;
	}

	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}

	public String getVehicleIcon() {
		return vehicleIcon;
	}

	public void setVehicleIcon(String vehicleIcon) {
		this.vehicleIcon = vehicleIcon;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleModels() {
		return vehicleModels;
	}

	public void setVehicleModels(String vehicleModels) {
		this.vehicleModels = vehicleModels;
	}

	public String getCarFrame() {
		return carFrame;
	}

	public void setCarFrame(String carFrame) {
		this.carFrame = carFrame;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverPhone() {
		return driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Long getEnabledFlag() {
		return enabledFlag;
	}

	public void setEnabledFlag(Long enabledFlag) {
		this.enabledFlag = enabledFlag;
	}

	public String getIsLife() {
		return isLife;
	}

	public void setIsLife(String isLife) {
		this.isLife = isLife;
	}

	public String getBindUserId() {
		return bindUserId;
	}

	public void setBindUserId(String bindUserId) {
		this.bindUserId = bindUserId;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getOverSpeed() {
		return overSpeed;
	}

	public void setOverSpeed(String overSpeed) {
		this.overSpeed = overSpeed;
	}

	public Date getInstallTime() {
		return installTime;
	}

	public void setInstallTime(Date installTime) {
		this.installTime = installTime;
	}

	public String getInstallAddress() {
		return installAddress;
	}

	public void setInstallAddress(String installAddress) {
		this.installAddress = installAddress;
	}

	public String getInstallCompany() {
		return installCompany;
	}

	public void setInstallCompany(String installCompany) {
		this.installCompany = installCompany;
	}

	public String getInstallPosition() {
		return installPosition;
	}

	public void setInstallPosition(String installPosition) {
		this.installPosition = installPosition;
	}

	public String getInstallPersonnel() {
		return installPersonnel;
	}

	public void setInstallPersonnel(String installPersonnel) {
		this.installPersonnel = installPersonnel;
	}

	public String getInstallImage() {
		return installImage;
	}

	public void setInstallImage(String installImage) {
		this.installImage = installImage;
	}

	public String getIccid() {
		return iccid;
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getOrgIdForAttention() {
		return orgIdForAttention;
	}

	public void setOrgIdForAttention(String orgIdForAttention) {
		this.orgIdForAttention = orgIdForAttention;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
