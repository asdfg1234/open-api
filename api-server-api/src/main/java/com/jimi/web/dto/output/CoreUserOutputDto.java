/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月26日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.dto.output;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.commons.DatetimeUtil;
import com.jimi.dto.base.OutputDto;

/**
 * @FileName CoreUserOutputDto.java
 * @Description: 
 *
 * @Date 2017年4月26日 下午5:02:56
 * @author li.shangzhi
 * @version 1.0
 */
public class CoreUserOutputDto extends OutputDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	private Integer id;
	/**
	 * idCard
	 */
	private String idCard;
	/**
	 * 帐号类型(唯一索引):0超级管理员，1运营商，2店员，3终端用户 5手机注册 6PC注册 7平台内部添加 ,8.一级代理商 9.普通用户
	 * 10.普通代理商 11.销售 12.体验账号
	 */
	private Integer type;
	/**
	 * 所属运营商ID(随销售而改变,最终变成庞物商点的OrgId)
	 */
	private Integer orgId;
	/**
	 * 所属运营商父节点编码
	 */
	private String orgCode;
	/**
	 * 登录帐号(唯一索引)
	 */
	private String account;
	/**
	 * MD5密码
	 */
	private String password;
	/**
	 * 显示名称
	 */
	private String name;
	/**
	 * 是否启用(1:启用,0:不启用)
	 */
	private Integer displayFlag;
	/**
	 * 所在地
	 */
	private String address;
	/**
	 * 生日
	 */
	private Date birth;
	/**
	 * 生日（开始时间，用于搜索）
	 */
	private Date startBirth;
	/**
	 * 生日（结束时间，用于搜索）
	 */
	private Date endBirth;
	/**
	 * 状态：0注册，1激活，2删除
	 */
	private Integer status;
	/**
	 * 公司名
	 */
	private String companyName;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 手机类型：0Android,1Apple
	 */
	private Integer plat;
	/**
	 * 语言(zh,en)
	 */
	private String language;
	/**
	 * 性别:0男，1女
	 */
	private Integer sex;
	/**
	 * 头像ID
	 */
	private String headPicId;
	/**
	 * 创建人
	 */
	private Integer createdBy;
	/**
	 * 标志:1可用，0不可用
	 */
	private Integer enabledFlag;
	/**
	 * 创建时间
	 */
	private Date creationDate;
	/**
	 * 创建时间（开始时间，用于搜索）
	 */
	private Date startCreationDate;
	/**
	 * 创建时间（结束时间，用于搜索）
	 */
	private Date endCreationDate;
	/**
	 * 修改人
	 */
	private Integer updatedBy;
	/**
	 * 修改时间
	 */
	private Date updationDate;
	/**
	 * 修改时间（开始时间，用于搜索）
	 */
	private Date startUpdationDate;
	/**
	 * 修改时间（结束时间，用于搜索）
	 */
	private Date endUpdationDate;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 用户时区
	 */
	private String timeZones;
	/**
	 * 手机国家代码
	 */
	private String cycode;

	public CoreUserOutputDto() {
	}

	public CoreUserOutputDto(Integer id) {
		this.id = id;
	}

	public void setId(Integer id) {
		id = id == null ? 0 : id;
		this.id = id;
	}

	public Integer getId() {
		return this.id == null ? 0 : this.id;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getIdCard() {
		return this.idCard == null ? null : this.idCard.trim();
	}

	public void setType(Integer type) {
		type = type == null ? 0 : type;
		this.type = type;
	}

	public Integer getType() {
		return this.type == null ? 0 : this.type;
	}

	public void setOrgId(Integer orgId) {
		orgId = orgId == null ? 0 : orgId;
		this.orgId = orgId;
	}

	public Integer getOrgId() {
		return this.orgId == null ? 0 : this.orgId;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgCode() {
		return this.orgCode == null ? null : this.orgCode.trim();
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAccount() {
		return this.account == null ? null : this.account.trim();
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return this.password == null ? null : this.password.trim();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name == null ? null : this.name.trim();
	}

	public void setDisplayFlag(Integer displayFlag) {
		displayFlag = displayFlag == null ? 0 : displayFlag;
		this.displayFlag = displayFlag;
	}

	public Integer getDisplayFlag() {
		return this.displayFlag == null ? 0 : this.displayFlag;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return this.address == null ? null : this.address.trim();
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public Date getBirth() {
		return this.birth;
	}

	public void setStartBirth(Date startBirth) {
		this.startBirth = startBirth;
	}

	public Date getStartBirth() {
		return this.startBirth;
	}

	public void setEndBirth(Date endBirth) {
		this.endBirth = endBirth;
	}

	public Date getEndBirth() {
		return this.endBirth;
	}

	public String getStringBirth() {
		if (this.birth == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.birth, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setStatus(Integer status) {
		status = status == null ? 0 : status;
		this.status = status;
	}

	public Integer getStatus() {
		return this.status == null ? 0 : this.status;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return this.companyName == null ? null : this.companyName.trim();
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return this.email == null ? null : this.email.trim();
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return this.phone == null ? null : this.phone.trim();
	}

	public void setPlat(Integer plat) {
		plat = plat == null ? 0 : plat;
		this.plat = plat;
	}

	public Integer getPlat() {
		return this.plat == null ? 0 : this.plat;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguage() {
		return this.language == null ? null : this.language.trim();
	}

	public void setSex(Integer sex) {
		sex = sex == null ? 0 : sex;
		this.sex = sex;
	}

	public Integer getSex() {
		return this.sex == null ? 0 : this.sex;
	}

	public void setHeadPicId(String headPicId) {
		this.headPicId = headPicId;
	}

	public String getHeadPicId() {
		return this.headPicId == null ? null : this.headPicId.trim();
	}

	public void setCreatedBy(Integer createdBy) {
		createdBy = createdBy == null ? 0 : createdBy;
		this.createdBy = createdBy;
	}

	public Integer getCreatedBy() {
		return this.createdBy == null ? 0 : this.createdBy;
	}

	public void setEnabledFlag(Integer enabledFlag) {
		enabledFlag = enabledFlag == null ? 0 : enabledFlag;
		this.enabledFlag = enabledFlag;
	}

	public Integer getEnabledFlag() {
		return this.enabledFlag == null ? 0 : this.enabledFlag;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setStartCreationDate(Date startCreationDate) {
		this.startCreationDate = startCreationDate;
	}

	public Date getStartCreationDate() {
		return this.startCreationDate;
	}

	public void setEndCreationDate(Date endCreationDate) {
		this.endCreationDate = endCreationDate;
	}

	public Date getEndCreationDate() {
		return this.endCreationDate;
	}

	public String getStringCreationDate() {
		if (this.creationDate == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.creationDate, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setUpdatedBy(Integer updatedBy) {
		updatedBy = updatedBy == null ? 0 : updatedBy;
		this.updatedBy = updatedBy;
	}

	public Integer getUpdatedBy() {
		return this.updatedBy == null ? 0 : this.updatedBy;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

	public Date getUpdationDate() {
		return this.updationDate;
	}

	public void setStartUpdationDate(Date startUpdationDate) {
		this.startUpdationDate = startUpdationDate;
	}

	public Date getStartUpdationDate() {
		return this.startUpdationDate;
	}

	public void setEndUpdationDate(Date endUpdationDate) {
		this.endUpdationDate = endUpdationDate;
	}

	public Date getEndUpdationDate() {
		return this.endUpdationDate;
	}

	public String getStringUpdationDate() {
		if (this.updationDate == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.updationDate, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return this.remark == null ? null : this.remark.trim();
	}

	public void setTimeZones(String timeZones) {
		this.timeZones = timeZones;
	}

	public String getTimeZones() {
		return this.timeZones == null ? null : this.timeZones.trim();
	}

	public void setCycode(String cycode) {
		this.cycode = cycode;
	}

	public String getCycode() {
		return this.cycode == null ? null : this.cycode.trim();
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
