/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月11日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.api;

import com.jimi.app.dto.output.ApiAppInfoOutputDto;
import com.jimi.exception.SysApiException;
import com.jimi.web.dto.output.AccessTokenOutputDto;

/**
 * @FileName IOAuthApiService.java
 * @Description:
 *
 * @Date 2017年4月11日 上午11:40:45
 * @author li.shangzhi
 * @version 1.0
 */
public interface IOAuthApiService {

	/**
	 * @Title: getAppkeyAndSecret
	 * @Description: 获取用户的AppKey和AppSecret
	 * @param userId
	 * @param userPwd
	 * @param projectCode
	 * @author li.shangzhi
	 * @date 2017年4月12日 下午3:21:47
	 */
	ApiAppInfoOutputDto getAppkeyAndSecret(String userId, String userPwd, String projectCode, String appDesc);

	/**
	 * @Title: createAccessToken
	 * @Description: 生成AccessToken
	 * @param appKey
	 * @param userId
	 * @param account
	 * @param expiresIn
	 * @author li.shangzhi
	 * @date 2017年4月12日 下午6:28:43
	 */
	AccessTokenOutputDto createAccessToken(String appKey, String userId, String account, Integer expiresIn) throws SysApiException;
	
	/**
	 * @Title: createAccessToken
	 * @Description: 刷新AccessToken
	 * @param accessToken
	 * @param refreshToken
	 * @param expiresIn
	 * @author li.shangzhi
	 * @date 2017年4月12日 下午6:28:43
	 */
	AccessTokenOutputDto refreshAccessToken(String accessToken, String refreshToken, Integer expiresIn) throws SysApiException;

}
