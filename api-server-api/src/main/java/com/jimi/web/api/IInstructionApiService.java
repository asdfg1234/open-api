/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.api;

import java.util.List;

import com.jimi.dto.base.ApiResult;
import com.jimi.web.dto.input.SendInstructionInputDto;
import com.jimi.web.dto.output.CommandLogsOutputDto;
import com.jimi.web.dto.output.InstructionDetailsOutputDto;

/**
 * @FileName IInstructionApiService.java
 * @Description:
 *
 * @Date 2017年7月25日 下午2:08:56
 * @author li.shangzhi
 * @version 1.0
 */
public interface IInstructionApiService {

	/**
	 * @Title: getInstructionByMcType
	 * @Description: 根据设备机型查询支持的指令列表
	 * @param mcType
	 *            机型
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月24日 下午6:18:17
	 */
	List<InstructionDetailsOutputDto> getInstructionByMcType(String mcType);

	/**
	 * @Title: sendInstruction
	 * @Description: 向设备发送指令
	 * @param inputDto
	 * @author li.shangzhi
	 * @date 2017年7月25日 下午3:37:44
	 */
	ApiResult sendInstruction(SendInstructionInputDto inputDto);

	/**
	 * @Title: getCommandLogsByImei
	 * @Description: 根据imei查询下发的指令结果
	 * @param imei
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月25日 上午11:44:33
	 */
	List<CommandLogsOutputDto> getCommandLogsByImei(String imei);
}
