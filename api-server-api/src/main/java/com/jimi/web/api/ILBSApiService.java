/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月17日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.api;

import com.jimi.exception.SysApiException;
import com.jimi.web.dto.output.LbsPointOutputDto;

/**
 * @FileName ILBSApiService.java
 * @Description:
 *
 * @Date 2017年4月17日 上午11:43:08
 * @author li.shangzhi
 * @version 1.0
 */
public interface ILBSApiService {

	/**
	 * @Title: getAddress
	 * @Description: 获取设备位置<br>
	 *               一个设备一天10次，1000设备，一天可以有1W次请求。<br>
	 *               单个设备IMEI，10秒钟只能请求一次
	 * @param imei
	 * @param lbs
	 * @param wifi
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月19日 上午10:08:12
	 */
	LbsPointOutputDto getAddress(String imei, String lbs, String wifi) throws SysApiException;
}
