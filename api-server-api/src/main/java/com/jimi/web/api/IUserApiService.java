/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.api;

import java.util.List;

import com.jimi.web.dto.input.UserInfoInputDto;
import com.jimi.web.dto.output.CoreUserOutputDto;
import com.jimi.web.dto.output.DeviceInfoOutputDto;
import com.jimi.web.dto.output.DeviceLocationInfoOutputDto;

/**
 * @FileName IUserApiService.java
 * @Description:
 *
 * @Date 2017年4月13日 下午1:46:11
 * @author li.shangzhi
 * @version 1.0
 */
public interface IUserApiService {
	
	/**
	 * @Title: getAccount 
	 * @Description: 查询账号信息
	 * @param userId
	 * @param account
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月26日 下午6:19:34
	 */
	CoreUserOutputDto getAccount(String account);
	
	/**
	 * @Title: checkUserRange 
	 * @Description: 检查该账号是否所属userId，或其子账号
	 * @param userId
	 * @param account
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月26日 下午6:19:34
	 */
	CoreUserOutputDto checkUserRange(String userId, String account);

	/**
	 * @Title: getAccounts 
	 * @Description: 获取指定账号下所有子账号信息
	 * @param account
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月13日 下午8:10:45
	 */
	List<CoreUserOutputDto> getAccounts(String userId);

	/**
	 * @Title: getDevices 
	 * @Description: 查询该账户下所有IMEI信息
	 * @param account
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月13日 下午8:10:18
	 */
	List<DeviceInfoOutputDto> getDevices(String userId);

	/**
	 * @Title: getAllLocations 
	 * @Description: 查询账号下所有设备的当前位置和状态信息
	 * @param account
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月13日 下午5:31:29
	 */
	List<DeviceLocationInfoOutputDto> getAllLocations(String userId, String mapType);

	/**
	 * @Title: saveUser 
	 * @Description: 保存账号
	 * @param userInfoInputDto
	 * @author li.shangzhi
	 * @date 2017年6月12日 下午4:15:02
	 */
	String saveUser(UserInfoInputDto userInfoInputDto);
}
