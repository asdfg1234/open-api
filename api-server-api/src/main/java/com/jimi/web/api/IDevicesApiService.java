/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月14日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.api;

import java.util.List;

import com.jimi.dto.base.ApiResult;
import com.jimi.web.dto.input.ElectricFenceInputDto;
import com.jimi.web.dto.output.DeviceInfoOutputDto;
import com.jimi.web.dto.output.DeviceLocationInfoOutputDto;
import com.jimi.web.dto.output.GpsPointOutputDto;

/**
 * @FileName IDeviceApiService.java
 * @Description:
 *
 * @Date 2017年4月14日 上午9:41:07
 * @author li.shangzhi
 * @version 1.0
 */
public interface IDevicesApiService {

	/**
	 * @Title: getLocations 
	 * @Description: 查询多个imei的位置信息
	 * @param imeis 多个imei号，用逗号分隔
	 * @param mapType
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年7月19日 下午4:49:55
	 */
	List<DeviceLocationInfoOutputDto> getLocations(String imeis, String mapType);

	List<GpsPointOutputDto> getTracks(String imei, String beginTime, String endTime, String mapType);

	/**
	 * @Title: bindUser 
	 * @Description: 绑定设备
	 * @param imei 设备imei
	 * @param bindUserId 要绑定的userId
	 * @param bindAccount 要绑定的账号
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年5月19日 下午11:10:37
	 */
	boolean bindUser(String imei, String bindUserId, String bindAccount);
	
	boolean unBindUser(String imei, String userId);

	String updateDeviceInfo(String imei, String deviceName, String vehicleName, String vehicleIcon, String vehicleNumber,
			String vehicleModels, String driverName, String driverPhone);

	/**
	 * @Title: CheckDeviceRange 
	 * @Description: 检查imei是否为userId的设备或下级设备
	 * @param userId
	 * @param imei
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月21日 上午11:43:20
	 */
	DeviceInfoOutputDto checkDeviceRange(String userId, String imei);
	
	/**
	 * @Title: createElectricFence 
	 * @Description: 创建电子围栏
	 * @param electricFenceVo
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月19日 下午5:05:07
	 */
	ApiResult createElectricFence(ElectricFenceInputDto electricFenceInputDto);
	
	/**
	 * @Title: deleteElectricFence 
	 * @Description: 删除电子围栏
	 * @param imei
	 * @param instructNo
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月21日 上午11:47:59
	 */
	ApiResult deleteElectricFence(String imei, String mcType, String instructNo);
	
	/**
	 * @Title: moveDevice 
	 * @Description: 转移/销售设备
	 * @param srcUserId 原用户Id
	 * @param destUserId 目标用户Id
	 * @param imeis 设备imei，用逗号分隔，最多支持500个
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年7月17日 下午4:53:22
	 */
	String moveDevice(String srcUserId, String destUserId, String imeis);
}
