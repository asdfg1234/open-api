/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.gateway.service;

/**
 * @FileName IRefreshRouteService.java
 * @Description: 
 *
 * @Date 2017年4月25日 下午8:11:13
 * @author li.shangzhi
 * @version 1.0
 */
public interface IRefreshRouteService {

	/**
	 * @Title: refreshRoute
	 * @Description: 通过zuul事件机制属性路由信息
	 * @author li.shangzhi
	 * @date 2017年4月5日 下午2:34:47
	 */
	public void refreshRoute();
	
}
