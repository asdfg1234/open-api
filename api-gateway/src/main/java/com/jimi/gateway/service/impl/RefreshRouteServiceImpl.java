/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月5日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.gateway.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.RoutesRefreshedEvent;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.jimi.gateway.service.IRefreshRouteService;

/**
 * @FileName RefreshRouteService.java
 * @Description: 提供路由信息刷新功能
 *
 * @Date 2017年4月5日 下午2:26:42
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class RefreshRouteServiceImpl implements IRefreshRouteService {

	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	private RouteLocator routeLocator;

	/**
	 * @Title: refreshRoute
	 * @Description: 通过zuul事件机制属性路由信息
	 * @author li.shangzhi
	 * @date 2017年4月5日 下午2:34:47
	 */
	@Override
	public void refreshRoute() {
		RoutesRefreshedEvent routesRefreshedEvent = new RoutesRefreshedEvent(routeLocator);
		publisher.publishEvent(routesRefreshedEvent);
	}
}
