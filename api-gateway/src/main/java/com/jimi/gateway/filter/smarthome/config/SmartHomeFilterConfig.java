/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月5日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.gateway.filter.smarthome.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jimi.gateway.filter.smarthome.pre.ShAuthFilter;
import com.jimi.gateway.filter.smarthome.pre.ShParamFilter;
import com.jimi.gateway.filter.smarthome.route.H5StaticResourceFilter;
import com.jimi.gateway.filter.smarthome.route.ShRouteMappingFilter;

/**
 * @FileName SmartHomeFilterConfig.java
 * @Description: SmartHome项目网关过滤器链配置
 *
 * @Date 2017年4月5日 下午2:17:05
 * @author li.shangzhi
 * @version 1.0
 */
@Configuration
public class SmartHomeFilterConfig {
	
	@Autowired
	private ZuulProperties zuulProperties;

	@Bean
	public ShParamFilter shParamFilter() {
		return new ShParamFilter();
	}
	
	@Bean
	public ShAuthFilter shAuthFilter() {
		return new ShAuthFilter();
	}
	
	@Bean
	public ShRouteMappingFilter shRouteMappingFilter() {
		return new ShRouteMappingFilter();
	}
	
	@Bean
	public H5StaticResourceFilter h5StaticResourceFilter() {
		return new H5StaticResourceFilter(zuulProperties);
	}
	
}