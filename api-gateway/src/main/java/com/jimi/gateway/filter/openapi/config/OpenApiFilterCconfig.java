/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.gateway.filter.openapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jimi.gateway.filter.openapi.post.AccessingLogFilter;
import com.jimi.gateway.filter.openapi.pre.AuthentificationFilter;
import com.jimi.gateway.filter.openapi.pre.RequestParamFilter;
import com.jimi.gateway.filter.openapi.route.RouteLimitFilter;
import com.jimi.gateway.filter.openapi.route.RouteMappingFilter;

/**
 * @FileName OpenApiFilterCconfig.java
 * @Description: Open Api网关过滤器链配置
 *
 * @Date 2017年7月12日 上午10:46:54
 * @author li.shangzhi
 * @version 1.0
 */
@Configuration
public class OpenApiFilterCconfig {

	@Bean
	public RequestParamFilter requestParamFilter() {
		return new RequestParamFilter();
	}
	
	@Bean
	public AuthentificationFilter authentificationFilter() {
		return new AuthentificationFilter();
	}
	
	@Bean
	public RouteLimitFilter routeLimitFilter() {
		return new RouteLimitFilter();
	}
	
	@Bean
	public RouteMappingFilter routeMappingFilter() {
		return new RouteMappingFilter();
	}
	
	@Bean
	public AccessingLogFilter accessingLogFilter() {
		return new AccessingLogFilter();
	}
}
