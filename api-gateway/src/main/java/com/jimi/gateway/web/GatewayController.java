/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月11日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.gateway.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.web.ZuulHandlerMapping;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jimi.app.api.IApiOpenFunctionsApi;
import com.jimi.app.dto.input.ApiOpenFunctionsInputDto;
import com.jimi.app.dto.output.ApiOpenFunctionsOutputDto;
import com.jimi.framework.context.SpringContextHolder;
import com.jimi.gateway.service.IRefreshRouteService;

/**
 * @FileName DemoController.java
 * @Description:
 *
 * @Date 2017年4月11日 下午8:54:00
 * @author li.shangzhi
 * @version 1.0
 */
@RestController
public class GatewayController {

	@Autowired
	IRefreshRouteService refreshRouteService;

	@Resource
	private IApiOpenFunctionsApi openFunctionsApi;

	@RequestMapping("/refreshRoute")
	public String refreshRoute() {
		refreshRouteService.refreshRoute();
		return "refreshRoute";
	}

	@Autowired
	ZuulHandlerMapping zuulHandlerMapping;

	@RequestMapping("/watchNowRoute")
	public String watchNowRoute() {
		// 可以用debug模式看里面具体是什么
		Map<String, Object> handlerMap = zuulHandlerMapping.getHandlerMap();
		System.out.println(handlerMap);
		return "watchNowRoute";
	}

}
