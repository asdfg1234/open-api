/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月6日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.gateway.utils;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jimi.exception.IErrorCode;
import com.jimi.utils.MessageSourceUtil;

/**
 * @FileName ResultModel.java
 * @Description:
 *
 * @Date 2017年4月6日 下午2:32:03
 * @author li.shangzhi
 * @version 1.0
 */
public class ResultModel {
	
	@JsonProperty("code")
	private Integer code = null;

	@JsonProperty("message")
	private String message = null;

	@JsonProperty("result")
	private Object result = null;

	public ResultModel() {
		this.code = 0;
		this.message = "success";
	}

	public ResultModel(Integer code) {
		if (code == -1) {
			this.code = -1;
			this.message = "系统繁忙";
		}
	}

	public ResultModel(Integer code, String msg) {
		this.code = code;
		this.message = msg;
	}

	public ResultModel(IErrorCode errorCode) {
		this.code = errorCode.getCode();
		String msg = MessageSourceUtil.getMessage(errorCode.getErrorKey());
		if (StringUtils.isNotBlank(msg)) {
			this.message = msg;
		} else {
			this.message = errorCode.getErrorMsg();
		}
	}

	/**
	 * Get code
	 * 
	 * @return code
	 **/
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Get message
	 * 
	 * @return message
	 **/
	public String getMessage() {
		return message;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public ResultModel result(Object result) {
		this.result = result;
		return this;
	}

	/**
	 * Get message
	 * 
	 * @return message
	 **/
	public Object getResult() {
		return result;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ResultModel result = (ResultModel) o;
		return Objects.equals(this.code, result.code) && Objects.equals(this.message, result.message)
				&& Objects.equals(this.result, result.result);
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, message, result);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Error {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("    result: ").append(toIndentedString(result)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
