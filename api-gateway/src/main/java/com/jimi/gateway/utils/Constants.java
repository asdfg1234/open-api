/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月5日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.gateway.utils;

/**
 * @FileName Constants.java
 * @Description:
 *
 * @Date 2017年4月5日 下午5:05:05
 * @author li.shangzhi
 * @version 1.0
 */
public class Constants {

	public static final String SIGN_METHOD_MD5 = "md5";

	public static final String SIGN_METHOD_HMAC = "hmac";

	public static final String CHARSET_UTF8 = "UTF-8";

	/*
	 * zuul过滤器常量 
	 * pre：可以在请求被路由之前调用 
	 * route：在路由请求时候被调用 
	 * post：在routing和error过滤器之后被调用
	 * error：处理请求时发生错误时被调用
	 */
	public static final String PRE_FILTER = "pre";
	public static final String ROUTE_FILTER = "route";
	public static final String ERROR_FILTER = "error";
	public static final String POST_FILTER = "post";
	
	/* 
	 * open api通用参数
	 */
	public static final String METHOD = "method";
	public static final String TIMESTAMP = "timestamp";
	public static final String APP_KEY = "app_key";
	public static final String SIGN = "sign";
	public static final String SIGN_METHOD = "sign_method";
	public static final String V = "v";
	public static final String FORMAT = "format";
	public static final String[] PARAMS = { METHOD, APP_KEY, TIMESTAMP, FORMAT, V, SIGN_METHOD, SIGN };
	
	public static final String JSON = "json";
	public static final String VERSION_0_9 = "0.9";
	public static final String VERSION_1_0 = "1.0";
	
	/**
	 * 开放方法对象常量，存到RequestContext中
	 */
	public static final String OPEN_FUNCTION = "openFunction";
	/**
	 * 用户的APP对象常量，存到RequestContext中
	 */
	public static final String APP_INFO = "appInfo";
	/**
	 * 用户信息对象常量，存到RequestContext中
	 */
	public static final String USE_ACCOUNT = "useAccount";
	
	/**
	 * 是否继续下一个过滤器
	 */
	public static final String IS_NEXT_FILTER = "isNextFilter";
	
	/**
	 * open api的路由Id，与t_api_gateway_route_info表对应
	 */
	public static final String OPEN_API_NAMESPACE = "open.aichezaixian.com";
	/**
	 * smarthome的路由Id，与t_api_gateway_route_info表对应
	 */
	public static final String SMARTHOME_NAMESPACE = "smarthome.jimicloud.com";
	/**
	 *smarthome H5资源的路由Id，与t_api_gateway_route_info表对应
	 */
	public static final String H5_STATIC_RESOURCE_NAMESPACE = "smarthome.h5.static.resource";
}
