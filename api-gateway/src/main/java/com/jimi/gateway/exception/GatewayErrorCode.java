/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.gateway.exception;

import com.jimi.exception.IErrorCode;

/**
 * @FileName GatewayErrorCode.java
 * @Description: gateway错误码
 *
 * @Date 2017年4月25日 下午7:32:20
 * @author li.shangzhi
 * @version 1.0
 */
public enum GatewayErrorCode implements IErrorCode {
	
	SUCCESS(0, "success", "成功"),
	
	PARAM_METHOD_ERROR(1001, "gateway.param.method.invalid", "缺少方法名参数或不存在的方法名"),
	PARAM_APPKEY_ERROR(1001, "gateway.param.appkey.invalid", "缺少AppKey参数或无效的AppKey"),
	PARAM_TIMESTAMP_ERROR(1001, "gateway.param.timestamp.invalid", "缺少时间戳参数或非法的时间戳"),
	PARAM_FORMAT_ERROR(1001, "gateway.param.format.invalid", "缺少数据格式参数或数据格式错误"),
	PARAM_VERSION_ERROR(1001, "gateway.param.version.invalid", "缺少版本参数或无效的版本"),
	PARAM_SIGNMETHOD_ERROR(1001, "gateway.param.signmethod.invalid", "缺少签名方法参数或非法的签名方法"),
	PARAM_SIGN_ERROR(1001, "gateway.param.sign.invalid", "缺少签名参数或签名无效"),
	
	REQ_RATE_TOO_HIGH_S_ERROR(1006, "req_rate_too_high_s_error", "非法访问,每秒请求频率过高!"),
	REQ_RATE_TOO_HIGH_D_ERROR(1006, "req_rate_too_high_d_error", "非法访问,当天请求频率过高!");

	private final int number;

	private final String errorKey;

	private final String errorMsg;
	
	private GatewayErrorCode(int number, String errorKey, String errorMsg) {
		this.number = number;
		this.errorKey = errorKey;
		this.errorMsg = errorMsg;
	}
	
	@Override
	public int getCode() {
		return number;
	}

	@Override
	public String getErrorKey() {
		return errorKey;
	}

	@Override
	public String getErrorMsg() {
		return errorMsg;
	}

}
