/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月5日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.gateway;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;

/**
 * @FileName GatewayApplication.java
 * @Description: api-gateway启动入口
 *
 * @Date 2017年4月5日 上午11:42:32
 * @author li.shangzhi
 * @version 1.0
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.jimi")
@EnableZuulProxy
public class GatewayApplication implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(GatewayApplication.class);
	
	private static final String EXIT_CODE = "exitcode";
	
	@Value("${language}")
	private String language;

	@Override
	public void run(String... args) throws Exception {
		if (args.length > 0 && EXIT_CODE.equals(args[0])) {
			throw new ExitException();
		}
		
		if("zh".equals(language)) {
			// 设置默认语言环境  
	        Locale.setDefault(Locale.CHINA);
		} else {
			// 设置英文语言环境  
	        Locale.setDefault(Locale.US);
		}
	}
	
	public static void main(String[] args) {
		new SpringApplication(GatewayApplication.class).run(args);
		logger.info("====================Jimi Api GateWay启动成功!====================");
	}
	
	@Bean
	public MessageSource messageSource() {
	    final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	    messageSource.setBasename("i18n/messages");
	    messageSource.setFallbackToSystemLocale(false);
	    messageSource.setCacheSeconds(0);
	    messageSource.setDefaultEncoding("UTF-8");
	    return messageSource;
	}
	
//	@Bean
//	public EmbeddedServletContainerCustomizer containerCustomizer() {
//	    return new EmbeddedServletContainerCustomizer() {
//	        @Override
//	        public void customize(ConfigurableEmbeddedServletContainer container) {
//	            ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/errorHtml");
//	            container.addErrorPages(error404Page);
//	        }
//	    };
//	}
	
	class ExitException extends RuntimeException implements ExitCodeGenerator {
		private static final long serialVersionUID = 1L;

		@Override
		public int getExitCode() {
			return 10;
		}
	}
}
