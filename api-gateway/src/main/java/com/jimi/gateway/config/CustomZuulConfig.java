/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月5日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jimi.gateway.filter.error.ErrorFilter;
import com.jimi.gateway.route.CustomRouteLocator;

/**
 * @FileName CustomZuulConfig.java
 * @Description: 装载自定义zuul配置
 *
 * @Date 2017年4月5日 下午2:17:05
 * @author li.shangzhi
 * @version 1.0
 */
@Configuration
public class CustomZuulConfig {

	@Autowired
	private ZuulProperties zuulProperties;

	@Autowired
	ServerProperties server;

	@Bean
	public CustomRouteLocator routeLocator() {
		CustomRouteLocator routeLocator = new CustomRouteLocator(this.server.getServletPrefix(), this.zuulProperties);
		return routeLocator;
	}
	
	@Bean
	public ErrorFilter errorFilter() {
		return new ErrorFilter();
	}
}
