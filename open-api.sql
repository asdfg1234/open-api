/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50171
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50171
File Encoding         : 65001

Date: 2017-04-28 16:23:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_api_app_accessing_log
-- ----------------------------
DROP TABLE IF EXISTS `t_api_app_accessing_log`;
CREATE TABLE `t_api_app_accessing_log` (
  `id` varchar(32) NOT NULL,
  `remote_ip` varchar(50) NOT NULL COMMENT '访问者IP',
  `app_key` varchar(32) NOT NULL COMMENT '访问者app_key',
  `user_id` int(11) NOT NULL COMMENT '主用户id',
  `account` varchar(50) NOT NULL COMMENT '主账号',
  `use_account` varchar(50) NOT NULL COMMENT '当前访问的账号',
  `route_id` varchar(32) NOT NULL COMMENT '环境标识',
  `method` varchar(100) NOT NULL COMMENT '访问的方法',
  `path` varchar(250) NOT NULL COMMENT 'url路径',
  `status` tinyint(4) NOT NULL COMMENT '访问结果',
  `error_desc` varchar(100) DEFAULT NULL,
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='api接口访问日志';

-- ----------------------------
-- Records of t_api_app_accessing_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_api_app_funs
-- ----------------------------
DROP TABLE IF EXISTS `t_api_app_funs`;
CREATE TABLE `t_api_app_funs` (
  `app_key` varchar(32) NOT NULL COMMENT 'app_key',
  `fun_id` varchar(32) NOT NULL COMMENT '方法ID',
  `fun_name` varchar(50) NOT NULL COMMENT '方法描述',
  `req_per_second` int(11) NOT NULL COMMENT '每秒最高访问量',
  `req_per_day` int(11) NOT NULL COMMENT '每天最高访问量',
  PRIMARY KEY (`app_key`,`fun_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='app与function关系表';

-- ----------------------------
-- Records of t_api_app_funs
-- ----------------------------
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', '0648bffb07bf4842bf458183fdc877ca', '查询账号下的所有子账户信息', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', '1f2453344146422c8529433305d9e590', '获取access_token', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', '22f855c8773f4fc9b7fa630bd013ab95', '为设备IMEI删除电子围栏', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', '2305185cb29b428f97268fa97cac6f23', '查询账户下所有设备最新位置信息', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', '230b41f784c44cdbb4d8f4e5b2a5cd05', '根据IMEI获取轨迹数据', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', '77e9fb07a0cd4195b1fd60e0da3c7cd1', '为设备IMEI修改车辆信息', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', '840ca52c710b4dc3a2bdc72a374ac19a', '查询账户下所有设备信息', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', 'a923e5ddcd28464bb2d4a414f577a9d0', '为设备IMEI解除绑定用户', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', 'b66804283ce6462c859d24f06a46b3b2', 'wifi、基站定位解析', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', 'b8080696d0274c8f99e548e96fef3c22', '根据IMEI获取最新定位数据', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', 'cad897b56abb4cc1874ee2bbfa0ee1cf', '获取appKey和appSecret', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', 'ccf43f26ecf94addadfab7a73600f4b9', '为设备IMEI绑定用户', '10', '1000');
INSERT INTO `t_api_app_funs` VALUES ('8FB345B8693CCD00FD85C7E607C02D2A', 'fdd45b0c1fd84a77ab4e5811465ee732', '为设备IMEI创建电子围栏', '10', '1000');

-- ----------------------------
-- Table structure for t_api_app_info
-- ----------------------------
DROP TABLE IF EXISTS `t_api_app_info`;
CREATE TABLE `t_api_app_info` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `account` varchar(50) NOT NULL COMMENT '用户账号',
  `project_code` varchar(20) NOT NULL COMMENT '项目标识',
  `app_key` varchar(32) NOT NULL COMMENT 'proejct_code+user_id生成',
  `app_secret` varchar(32) NOT NULL COMMENT 'app密码随机生成',
  `app_desc` varchar(50) NOT NULL COMMENT 'app描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `last_udpate_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`user_id`,`project_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='APP信息表';

-- ----------------------------
-- Records of t_api_app_info
-- ----------------------------
INSERT INTO `t_api_app_info` VALUES ('75354', 'jimitest', 'TUQIANG', '8FB345B8693CCD00FD85C7E607C02D2A', '10211836c1b04007af49e79f4a10edc1', '几米测试环境', '2017-04-27 09:36:29', '2017-04-27 09:36:09');

-- ----------------------------
-- Table structure for t_api_gateway_route_info
-- ----------------------------
DROP TABLE IF EXISTS `t_api_gateway_route_info`;
CREATE TABLE `t_api_gateway_route_info` (
  `id` varchar(32) NOT NULL COMMENT '代理ID',
  `path` varchar(45) NOT NULL COMMENT '路由路径',
  `service_id` varchar(45) DEFAULT NULL COMMENT '服务ID，注册中心使用',
  `url` varchar(45) NOT NULL COMMENT '完整的url路径',
  `retryable` tinyint(1) NOT NULL COMMENT '是否重试，service-id下有用',
  `enabled` tinyint(1) NOT NULL COMMENT '是否启用，1-启用 0-不启用',
  `strip_prefix` tinyint(1) NOT NULL,
  `api_name` varchar(50) NOT NULL COMMENT '路由描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='路由表';

-- ----------------------------
-- Records of t_api_gateway_route_info
-- ----------------------------
INSERT INTO `t_api_gateway_route_info` VALUES ('open.aichezaixian.com', '/route/rest/**', null, 'http://open.aichezaixian.com/route/rest', '0', '1', '1', '几米开放API正式环境');

-- ----------------------------
-- Table structure for t_api_open_functions
-- ----------------------------
DROP TABLE IF EXISTS `t_api_open_functions`;
CREATE TABLE `t_api_open_functions` (
  `id` varchar(32) NOT NULL,
  `route_id` varchar(32) NOT NULL COMMENT '路由表Id',
  `method` varchar(100) NOT NULL COMMENT '方法名标识',
  `url` varchar(250) NOT NULL COMMENT '方法完整请求路径',
  `data_filter` char(1) DEFAULT NULL COMMENT '需要数据权限',
  `method_desc` varchar(250) NOT NULL COMMENT '方法描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='开放api方法表';

-- ----------------------------
-- Records of t_api_open_functions
-- ----------------------------
INSERT INTO `t_api_open_functions` VALUES ('0648bffb07bf4842bf458183fdc877ca', 'open.aichezaixian.com', 'jimi.user.child.list', 'http://127.0.0.1:8092/v1/user/getAccounts', null, '查询账号下的所有子账户信息');
INSERT INTO `t_api_open_functions` VALUES ('1f2453344146422c8529433305d9e590', 'open.aichezaixian.com', 'jimi.oauth.token.get', 'http://127.0.0.1:8092/v1/oauth/getAccessToken', null, '获取access_token');
INSERT INTO `t_api_open_functions` VALUES ('22f855c8773f4fc9b7fa630bd013ab95', 'open.aichezaixian.com', 'jimi.open.device.fence.delete', 'http://127.0.0.1:8092/v1/devices/deleteElectricFence', null, '为设备IMEI删除电子围栏');
INSERT INTO `t_api_open_functions` VALUES ('2305185cb29b428f97268fa97cac6f23', 'open.aichezaixian.com', 'jimi.user.device.location.list', 'http://127.0.0.1:8092/v1/user/getAllLocations', null, '查询账户下所有设备最新位置信息');
INSERT INTO `t_api_open_functions` VALUES ('230b41f784c44cdbb4d8f4e5b2a5cd05', 'open.aichezaixian.com', 'jimi.device.track.list', 'http://127.0.0.1:8092/v1/devices/getTracks', null, '根据IMEI获取轨迹数据');
INSERT INTO `t_api_open_functions` VALUES ('77e9fb07a0cd4195b1fd60e0da3c7cd1', 'open.aichezaixian.com', 'jimi.open.device.update', 'http://127.0.0.1:8092/v1/devices/updateDeviceInfo', null, '为设备IMEI修改车辆信息');
INSERT INTO `t_api_open_functions` VALUES ('840ca52c710b4dc3a2bdc72a374ac19a', 'open.aichezaixian.com', 'jimi.user.device.list', 'http://127.0.0.1:8092/v1/user/getDevices', null, '查询账户下所有设备信息');
INSERT INTO `t_api_open_functions` VALUES ('a923e5ddcd28464bb2d4a414f577a9d0', 'open.aichezaixian.com', 'jimi.open.device.unbind', 'http://127.0.0.1:8092/v1/devices/unBindUser', null, '为设备IMEI解除绑定用户');
INSERT INTO `t_api_open_functions` VALUES ('b66804283ce6462c859d24f06a46b3b2', 'open.aichezaixian.com', 'jimi.lbs.address.get', 'http://127.0.0.1:8092/v1/lbs/getAddress', null, 'wifi、基站定位解析');
INSERT INTO `t_api_open_functions` VALUES ('b8080696d0274c8f99e548e96fef3c22', 'open.aichezaixian.com', 'jimi.device.location.get', 'http://127.0.0.1:8092/v1/devices/getLocations', null, '根据IMEI获取最新定位数据');
INSERT INTO `t_api_open_functions` VALUES ('cad897b56abb4cc1874ee2bbfa0ee1cf', 'open.aichezaixian.com', 'jimi.oauth.appkey.create', 'http://127.0.0.1:8092/v1/oauth/getAppkeyAndSecret', null, '获取appKey和appSecret');
INSERT INTO `t_api_open_functions` VALUES ('ccf43f26ecf94addadfab7a73600f4b9', 'open.aichezaixian.com', 'jimi.open.device.bind', 'http://127.0.0.1:8092/v1/devices/bindUser', null, '为设备IMEI绑定用户');
INSERT INTO `t_api_open_functions` VALUES ('fdd45b0c1fd84a77ab4e5811465ee732', 'open.aichezaixian.com', 'jimi.open.device.fence.create', 'http://127.0.0.1:8092/v1/devices/createElectricFence', null, '为设备IMEI创建电子围栏');
