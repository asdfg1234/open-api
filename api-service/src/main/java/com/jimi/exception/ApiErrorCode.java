/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月2日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.exception;

/**
 * @FileName ApiErrorCode.java
 * @Description: 
 *
 * @Date 2017年5月2日 上午11:19:23
 * @author li.shangzhi
 * @version 1.0
 */
public enum ApiErrorCode implements IErrorCode {

	PARAM_MAPTYPE_ERROR(1001, "api.mapType.invalid", "地图类型参数不正确"),
	PARAM_DATE_INTERVAL_ERROR(1001, "param_date_interval_error", "时间跨度超过指定范围"),
	PARAM_BEGIN_DATE_ERROR(1001, "param_begin_date_error", "时间条件错误，开始时间大于结束时间");
	
	private final int number;

	private final String errorKey;

	private final String errorMsg;
	
	private ApiErrorCode(int number, String errorKey, String errorMsg) {
		this.number = number;
		this.errorKey = errorKey;
		this.errorMsg = errorMsg;
	}
	
	@Override
	public int getCode() {
		return number;
	}

	@Override
	public String getErrorKey() {
		return errorKey;
	}

	@Override
	public String getErrorMsg() {
		return errorMsg;
	}

}
