/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月24日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.instruct;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.jimi.framework.utils.ResultModel;

/**
 * @FileName InstructionApi.java
 * @Description:
 *
 * @Date 2017年7月24日 下午2:37:14
 * @author li.shangzhi
 * @version 1.0
 */
@Api(value = "Instructions")
public interface InstructionApi {

	@ApiOperation(value = "获取设备指令列表", notes = "获取设备所属机型支持的指令列表", response = ResultModel.class, httpMethod = "GET", tags = { "指令接口" })
	public ResponseEntity<ResultModel> getInstructionList(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			HttpServletRequest request);
	
	@ApiOperation(value = "发送指令消息", notes = "发送指令消息(在线设备同步下发、离线设备异步存储后等待上线后下发)", response = ResultModel.class, httpMethod = "POST", tags = { "指令接口" })
	public ResponseEntity<ResultModel> sendInstruction(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			@ApiParam(name = "inst_param_json", value = "指令的josn对象", required = true) String instParamJson,
			HttpServletRequest request);
	
	@ApiOperation(value = "获取指令结果列表", notes = "查询指定设备指令发送结果列表", response = ResultModel.class, httpMethod = "GET", tags = { "指令接口" })
	public ResponseEntity<ResultModel> getInstructionResult(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			HttpServletRequest request);
}
