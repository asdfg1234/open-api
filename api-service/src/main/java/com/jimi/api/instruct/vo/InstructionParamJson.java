/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月28日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.instruct.vo;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @FileName InstructionParamJson.java
 * @Description:
 *
 * @Date 2017年7月28日 下午1:46:25
 * @author li.shangzhi
 * @version 1.0
 */
public class InstructionParamJson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 指令ID
	 */
	@JSONField(name = "inst_id")
	private String instId;
	/**
	 * 指令模板
	 */
	@JSONField(name = "inst_template")
	private String instTemplate;
	/**
	 * 指令参数
	 */
	@JSONField(name = "params")
	private String[] params;
	/**
	 * 是否覆盖已存在的离线指令
	 */
	@JSONField(name = "is_cover")
	private boolean isCover;

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getInstTemplate() {
		return instTemplate;
	}

	public void setInstTemplate(String instTemplate) {
		this.instTemplate = instTemplate;
	}

	public String[] getParams() {
		return params;
	}

	public void setParams(String[] params) {
		this.params = params;
	}

	public boolean isCover() {
		return isCover;
	}

	public void setCover(boolean isCover) {
		this.isCover = isCover;
	}

}
