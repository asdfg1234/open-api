/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月27日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.instruct.vo;

import java.io.Serializable;

/**
 * @FileName InstructionDetailsVo.java
 * @Description:
 *
 * @Date 2017年7月27日 上午10:22:18
 * @author li.shangzhi
 * @version 1.0
 */
public class InstructionDetailsVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private Integer id;
	/**
	 * 名称
	 */
	private String orderName;
	/**
	 * 内容
	 */
	private String orderContent;
	/**
	 * 说明
	 */
	private String orderExplain;
	/**
	 * 提示
	 */
	private String orderMsg;
	/**
	 * 是否支持离线指令 0:是否 1:是
	 */
	private String isOffLine;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderContent() {
		return orderContent;
	}

	public void setOrderContent(String orderContent) {
		this.orderContent = orderContent;
	}

	public String getOrderExplain() {
		return orderExplain;
	}

	public void setOrderExplain(String orderExplain) {
		this.orderExplain = orderExplain;
	}

	public String getOrderMsg() {
		return orderMsg;
	}

	public void setOrderMsg(String orderMsg) {
		this.orderMsg = orderMsg;
	}

	public String getIsOffLine() {
		return isOffLine;
	}

	public void setIsOffLine(String isOffLine) {
		this.isOffLine = isOffLine;
	}

}
