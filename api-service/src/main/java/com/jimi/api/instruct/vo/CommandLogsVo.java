/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月27日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.instruct.vo;

import java.io.Serializable;
import java.util.Date;

import com.jimi.commons.DatetimeUtil;

/**
 * @FileName CommandLogsVo.java
 * @Description:
 *
 * @Date 2017年7月27日 上午10:38:52
 * @author li.shangzhi
 * @version 1.0
 */
public class CommandLogsVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * codeId
	 */
	private String codeId;
	/**
	 * 指令编码
	 */
	private String code;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 指令状态0：执行失败,1：执行成功,3 : 待发送,4:已取消
	 */
	private String isExecute;
	/**
	 * 发送时间
	 */
	private Date sendTime;
	/**
	 * 发送者
	 */
	private String sender;
	/**
	 * 接收：imei
	 */
	private String receiveDevice;
	/**
	 * 0在线 1离线
	 */
	private String isOffLine;
	/**
	 * 指令名称
	 */
	private String idsource;

	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIsExecute() {
		return isExecute;
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	public String getSendTime() {
		if (this.sendTime == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.sendTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiveDevice() {
		return receiveDevice;
	}

	public void setReceiveDevice(String receiveDevice) {
		this.receiveDevice = receiveDevice;
	}

	public String getIsOffLine() {
		return isOffLine;
	}

	public void setIsOffLine(String isOffLine) {
		this.isOffLine = isOffLine;
	}

	public String getIdsource() {
		return idsource;
	}

	public void setIdsource(String idsource) {
		this.idsource = idsource;
	}

}
