/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.devices;

import javax.servlet.http.HttpServletRequest;

import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotBlank;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

import com.jimi.framework.utils.ResultModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @FileName DevicesApi.java
 * @Description:
 *
 * @Date 2017年4月12日 上午10:23:51
 * @author li.shangzhi
 * @version 1.0
 */
@Api(value = "Devices")
public interface DevicesApi {

	@ApiOperation(value = "根据IMEI获取最新定位数据", notes = "获取单个或多个设备最新的位置信息", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> getLocations(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imeis", value = "设备imei号列表(多个用“,”分割)", required = true) String imeis,
			@ApiParam(name = "map_type", value = "地图类型(BAIDU/GOOGLE)", defaultValue = "BAIDU", required = false) String mapType,
			HttpServletRequest request);

	@ApiOperation(value = "根据IMEI获取轨迹数据", notes = "单个设备获取三个月内，时间跨度不超过2天的轨迹数据", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> getTracks(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			@ApiParam(name = "begin_time", value = "开始时间，格式为yyyy-MM-dd HH:mm:ss", required = true) String beginTime,
			@ApiParam(name = "end_time", value = "结束时间，格式为yyyy-MM-dd HH:mm:ss", required = true) String endTime,
			@ApiParam(name = "map_type", value = "地图类型(BAIDU/GOOGLE)", defaultValue = "BAIDU", required = false) String mapType,
			HttpServletRequest request);

	@ApiOperation(value = "为设备IMEI绑定用户", notes = "提供通过设备imei号绑定用户", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> bindUser(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			@ApiParam(name = "user_id", value = "要绑定的用户账号", required = true) String userId, HttpServletRequest request);
	
	@ApiOperation(value = "为设备IMEI解除绑定用户", notes = "提供通过设备imei号绑定用户", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> unBindUser(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			@ApiParam(name = "user_id", value = "绑定的用户账号", required = true) String userId, HttpServletRequest request);

	@ApiOperation(value = "为设备IMEI修改车辆信息", notes = "提供通过设备imei号更改车辆信息", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> updateDeviceInfo(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			@ApiParam(name = "device_name", value = "设备名称", required = false) String deviceName,
			@ApiParam(name = "vehicle_name", value = "车辆名称", required = false) String vehicleName,
			@ApiParam(name = "vehicle_icon", value = "车辆图标", required = false) String vehicleIcon,
			@ApiParam(name = "vehicle_number", value = "车牌号", required = false) String vehicleNumber,
			@ApiParam(name = "vehicle_models", value = "车辆品牌", required = false) String vehicleModels,
			@ApiParam(name = "driver_name", value = "司机名称", required = false) String driverName,
			@ApiParam(name = "driver_phone", value = "司机电话", required = false) String driverPhone, HttpServletRequest request);
	
	@ApiOperation(value = "创建电子围栏", notes = "创建电子围栏", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> createElectricFence(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			@ApiParam(name = "fence_name", value = "围栏名称", required = true) String fenceName,
			@ApiParam(name = "alarm_type", value = "告警类型(in/out/in,out)", defaultValue = "in,out", required = true) String alarmType,
			@ApiParam(name = "report_mode", value = "报警上报方式，0：仅GPRS，1：SMS+GPRS", defaultValue = "1", required = true) String reportMode,
			@ApiParam(name = "alarm_switch", value = "围栏报警开关(ON开启/OFF关闭)", defaultValue = "OFF", required = true) String alarmSwitch,
			@ApiParam(name = "lng", value = "经度", required = true) String lng,
			@ApiParam(name = "lat", value = "纬度", required = true) String lat,
			@ApiParam(name = "radius", value = "围栏半径(1～9999；单位：百米)", required = true) String radius,
			@ApiParam(name = "zoom_level", value = "缩放级别(3-19)", required = true) String zoomLevel,
			@ApiParam(name = "map_type", value = "地图类型(BAIDU/GOOGLE)", defaultValue = "BAIDU", required = false) String mapType,
			HttpServletRequest request);
	
	@ApiOperation(value = "删除电子围栏", notes = "删除电子围栏", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> deleteElectricFence(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			@ApiParam(name = "instruct_no", value = "电子围栏指令序号", required = true) String instructNo,
			HttpServletRequest request);
	
	@ApiOperation(value = "获取设备定位地图", notes = "获取设备定位地图", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> getDeviceLocationMap(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@NotBlank(message = "api.accessToken.invalid") @RequestParam(value = "access_token", required = true) String accessToken,
			@NotBlank(message = "api.imei.invalid") @RequestParam(value = "imei", required = true) String imei,
			@RequestParam(value = "map_type",  defaultValue = "", required = false) String mapType,
			HttpServletRequest request);

	@ApiOperation(value = "转移/销售设备", notes = "转移/销售设备", response = ResultModel.class, tags = { "设备接口" })
	public ResponseEntity<ResultModel> moveDevice(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@NotBlank(message = "api.accessToken.invalid") @Length(min = 10, max = 32, message = "api.accessToken.invalid") @RequestParam(value = "access_token", required = true) String accessToken,
			@ApiParam(name = "src_account", value = "原账号", required = true) String srcAccount,
			@ApiParam(name = "imeis", value = "设备imei号列表(多个用“,”分割)", required = true) String imeis,
			@ApiParam(name = "dest_account", value = "目标账号", required = true) String destAccount,
			HttpServletRequest request);
}
