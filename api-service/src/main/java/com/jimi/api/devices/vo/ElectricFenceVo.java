/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月17日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.api.devices.vo;

import java.io.Serializable;

/**
 * @FileName DeviceVo.java
 * @Description: 
 *
 * @Date 2017年4月17日 下午6:02:58
 * @author li.shangzhi
 * @version 1.0
 */
public class ElectricFenceVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String imei;
	
	private String mcType; // 设备型号
	
	private String fenceName; // 围栏名
	
	private String alarmType; // 进出报警类型
	
	private String reportMode;// 报警上报方式，0：仅GPRS，1：SMS+GPRS，默认为：1
	
	private String alarmSwitch;// 报警开关(ON开启/OFF关闭)
	
	private String lng; // 经度
	
	private String lat; // 维度
	
	private String radius;// 半径
	
	private String zoomLevel;// 缩放级别
	
	private String mapType;// 地图类型

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getMcType() {
		return mcType;
	}

	public void setMcType(String mcType) {
		this.mcType = mcType;
	}

	public String getFenceName() {
		return fenceName;
	}

	public void setFenceName(String fenceName) {
		this.fenceName = fenceName;
	}

	public String getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(String alarmType) {
		this.alarmType = alarmType;
	}

	public String getReportMode() {
		return reportMode;
	}

	public void setReportMode(String reportMode) {
		this.reportMode = reportMode;
	}
	
	public String getAlarmSwitch() {
		return alarmSwitch;
	}

	public void setAlarmSwitch(String alarmSwitch) {
		this.alarmSwitch = alarmSwitch;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getZoomLevel() {
		return zoomLevel;
	}

	public void setZoomLevel(String zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

	public String getMapType() {
		return mapType;
	}

	public void setMapType(String mapType) {
		this.mapType = mapType;
	}
	
}
