/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月14日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.api.devices.vo;

import java.util.Date;

import com.jimi.commons.DatetimeUtil;

/**
 * @FileName GpsPointVo.java
 * @Description: 
 *
 * @Date 2017年4月14日 下午5:12:13
 * @author li.shangzhi
 * @version 1.0
 */
public class GpsPointVo {

	/**
	 * 纬度
	 */
	private double lat;
	/**
	 * 经度
	 */
	private double lng;
	/**
	 * GPS时间
	 */
	private Date gpsTime;
	/**
	 * 方位
	 */
	private int direction;
	/**
	 * GPS速度
	 */
	private double gpsSpeed;
	
	//定位类型 1是卫星定位 2是基站定位 3是wifi定位
	private int posType;
	
	public int getPosType(){
		return posType;
	}
	
	public void setPosType(int posType){
		this.posType = posType;
	}

	public String getGpsTime() {
		if (this.gpsTime == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.gpsTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setGpsTime(Date gpsTime) {
		this.gpsTime = gpsTime;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public double getGpsSpeed() {
		return gpsSpeed;
	}

	public void setGpsSpeed(double gpsSpeed) {
		this.gpsSpeed = gpsSpeed;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
}
