/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.api.lbs.controller;

import javax.servlet.http.HttpServletRequest;

import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotBlank;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jimi.api.lbs.LBSApi;
import com.jimi.api.lbs.vo.LbsPointVo;
import com.jimi.exception.ErrorCode;
import com.jimi.exception.SysApiException;
import com.jimi.framework.base.BaseController;
import com.jimi.framework.bean.BeanUtil;
import com.jimi.framework.interceptor.loginsecurity.LoginSecurity;
import com.jimi.framework.utils.DESEncrypt;
import com.jimi.framework.utils.ResultModel;
import com.jimi.web.api.IDevicesApiService;
import com.jimi.web.api.ILBSApiService;
import com.jimi.web.dto.output.DeviceInfoOutputDto;
import com.jimi.web.dto.output.LbsPointOutputDto;

/**
 * @FileName LBSApiController.java
 * @Description: 
 *
 * @Date 2017年4月12日 下午2:25:24
 * @author li.shangzhi
 * @version 1.0
 */
@RestController
@RequestMapping(value = "/lbs")
public class LBSApiController extends BaseController implements LBSApi {

	@Autowired
	private ILBSApiService lbsApiService;
	
	@Autowired
	private IDevicesApiService devicesApiService;
	
	@ResponseBody
	@LoginSecurity
	@RequestMapping(value = "/getAddress", params = { "method=jimi.lbs.address.get" }, produces = { "application/json" }, method = {
			RequestMethod.GET, RequestMethod.POST })
	@Override
	public ResponseEntity<ResultModel> getAddress(
//			@NotBlank(message = "api.method.invalid") @Length(min = 10, max = 20, message = "api.method.invalid") @RequestParam(value = "method", required = true) String method,
			@NotBlank(message = "api.appkey.invalid") @Length(min = 10, max = 32, message = "api.appkey.invalid") @RequestParam(value = "app_key", required = true) String appKey,
//			@NotBlank(message = "api.timestamp.invalid") @DateRange(format = "yyyy-MM-dd HH:mm:ss", message = "api.timestamp.invalid") @RequestParam(value = "timestamp", required = true) String timestamp,
//			@NotBlank(message = "api.format.invalid") @MemberOf(value = { "json", "xml" }, message = "api.format.invalid") @RequestParam(value = "format", required = true) String format,
//			@NotBlank(message = "api.version.invalid") @MemberOf(value = { "1.0" }, message = "api.version.invalid") @RequestParam(value = "v", required = true) String v,
//			@NotBlank(message = "api.signmethod.invalid") @MemberOf(value = { "md5" }, message = "api.signmethod.invalid") @RequestParam(value = "sign_method", required = true) String signMethod,
//			@NotBlank(message = "api.sign.invalid") @Length(min = 10, max = 32, message = "api.sign.invalid") @RequestParam(value = "sign", required = true) String sign,
			@NotBlank(message = "api.accessToken.invalid") @Length(min = 10, max = 32, message = "api.accessToken.invalid") @RequestParam(value = "access_token", required = true) String accessToken,
			@NotBlank(message = "api.imei.invalid") @Length(min = 15, max = 16, message = "api.imei.invalid") @RequestParam(value = "imei", required = true) String imei,
			@RequestParam(value = "lbs", required = false) String lbs,
			@RequestParam(value = "wifi", required = false) String wifi,
			HttpServletRequest request) {
		if(StringUtils.isBlank(lbs) && StringUtils.isBlank(wifi)) {
			return new ResponseEntity<>(new ResultModel(ErrorCode.PARAM_ERROR), HttpStatus.OK);
		}
		String decrypt = DESEncrypt.decrypt(appKey);
		String[] strs = decrypt.split(",");
		String projectCode = strs[0];
		String userId = strs[1];
		DeviceInfoOutputDto deviceInfoOutputDto = devicesApiService.checkDeviceRange(userId, imei);
		if(null == deviceInfoOutputDto) {
			return new ResponseEntity<ResultModel>(new ResultModel(ErrorCode.INVALID_DEVICE_ERROR), HttpStatus.OK);
		}
		try{
			lbs = StringUtils.isBlank(lbs) ? "" : lbs;
			wifi = StringUtils.isBlank(wifi) ? "" : wifi;
			LbsPointOutputDto lbsOutput = lbsApiService.getAddress(imei, lbs, wifi);
			
			ResultModel resultModel = new ResultModel();
			resultModel.setResult(BeanUtil.convertBean(lbsOutput, LbsPointVo.class));
			return new ResponseEntity<ResultModel>(resultModel, HttpStatus.OK);
		}catch(SysApiException apiEx) {
			logger.error(apiEx.getMessage(), apiEx);
			return new ResponseEntity<>(new ResultModel(apiEx.getErrorCode()), HttpStatus.OK);
		}
	}

}
