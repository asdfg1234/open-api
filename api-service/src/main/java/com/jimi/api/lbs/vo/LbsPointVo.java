/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月26日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.api.lbs.vo;

import java.io.Serializable;

/**
 * @FileName LbsPointVo.java
 * @Description: 
 *
 * @Date 2017年4月26日 下午5:51:59
 * @author li.shangzhi
 * @version 1.0
 */
public class LbsPointVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 纬度
	 */
	private double lat;
	/**
	 * 经度
	 */
	private double lng;

	/**
	 * 精准度，值越大越好
	 */
	private int accuracy;

	/**
	 * POI地址
	 */
//	private String addr;

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public int getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(int accuracy) {
		this.accuracy = accuracy;
	}

//	public String getAddr() {
//		return addr;
//	}

//	public void setAddr(String addr) {
//		this.addr = addr;
//	}
}
