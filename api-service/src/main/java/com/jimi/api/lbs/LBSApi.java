/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.lbs;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.jimi.framework.utils.ResultModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @FileName LBSApi.java
 * @Description:
 *
 * @Date 2017年4月12日 上午10:23:51
 * @author li.shangzhi
 * @version 1.0
 */
@Api(value = "lbs/wifi")
public interface LBSApi {

	@ApiOperation(value = "wifi/基站定位解析接口", notes = "按用户名下总设备数（按每10次/天/设备，包含下级所有子账户）总数配置限额", response = ResultModel.class, tags = { "wifi/基站接口" })
	public ResponseEntity<ResultModel> getAddress(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "imei", value = "设备imei号", required = true) String imei,
			@ApiParam(name = "lbs", value = "LBS信息组(mcc,mnc,lac,cell,rssi)", required = false) String lbs,
			@ApiParam(name = "wifi", value = "MAC信息组(mac,rssi)", required = false) String wifi,
			HttpServletRequest request);
}
