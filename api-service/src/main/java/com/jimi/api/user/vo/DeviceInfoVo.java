/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.user.vo;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.jimi.commons.DatetimeUtil;

/**
 * @FileName DeviceInfo.java
 * @Description:
 *
 * @Date 2017年4月13日 下午6:02:20
 * @author li.shangzhi
 * @version 1.0
 */
public class DeviceInfoVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 设备IMEI号（16位）
	 */
	private String imei;
	/**
	 * 设备名称
	 */
	private String deviceName;
	/**
	 * 设备类型
	 */
	private String mcType;
	/**
	 * 机型使用范围(aotomobile:汽车 electromobile:电动车 personal:个人 pet:宠物 plane:飞机
	 * others:其他)
	 */
	@JSONField(name="useScope")
	private String mcTypeUseScope;
	/**
	 * SIM卡号
	 */
	private String sim;
	/**
	 * 到期时间
	 */
	private Date expiration;
	/**
	 * 激活时间
	 */
	private Date activationTime;
	/**
	 * 备注
	 */
	private String reMark;
	/**
	 * 车辆名称
	 */
	private String vehicleName;
	/**
	 * 车辆图标
	 */
	private String vehicleIcon;
	/**
	 * 车牌号
	 */
	private String vehicleNumber;
	/**
	 * 车辆品牌
	 */
	private String vehicleModels;
	/**
	 * 车架号
	 */
	private String carFrame;
	/**
	 * 司机名称
	 */
	private String driverName;
	/**
	 * 司机电话
	 */
	private String driverPhone;
	/**
	 * 是否启用（0：不启用；1：启用）
	 */
	private Long enabledFlag;
	/**
	 * 电机发动机号
	 */
	private String engineNumber;
	
	public DeviceInfoVo() {
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getMcType() {
		return mcType;
	}

	public void setMcType(String mcType) {
		this.mcType = mcType;
	}

	public String getMcTypeUseScope() {
		return mcTypeUseScope;
	}

	public void setMcTypeUseScope(String mcTypeUseScope) {
		this.mcTypeUseScope = mcTypeUseScope;
	}

	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}

	public String getExpiration() {
		if (this.expiration == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.expiration, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public String getActivationTime() {
		if (this.activationTime == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.activationTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setActivationTime(Date activationTime) {
		this.activationTime = activationTime;
	}

	public String getReMark() {
		return reMark;
	}

	public void setReMark(String reMark) {
		this.reMark = reMark;
	}

	public String getVehicleName() {
		return vehicleName;
	}

	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}

	public String getVehicleIcon() {
		return vehicleIcon;
	}

	public void setVehicleIcon(String vehicleIcon) {
		this.vehicleIcon = vehicleIcon;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleModels() {
		return vehicleModels;
	}

	public void setVehicleModels(String vehicleModels) {
		this.vehicleModels = vehicleModels;
	}

	public String getCarFrame() {
		return carFrame;
	}

	public void setCarFrame(String carFrame) {
		this.carFrame = carFrame;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverPhone() {
		return driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}

	public Long getEnabledFlag() {
		return enabledFlag;
	}

	public void setEnabledFlag(Long enabledFlag) {
		this.enabledFlag = enabledFlag;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

}