/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.user;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.jimi.framework.utils.ResultModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @FileName DeviceApi.java
 * @Description:
 *
 * @Date 2017年4月12日 上午10:23:51
 * @author li.shangzhi
 * @version 1.0
 */
@Api(value = "User")
public interface UserApi {

	@ApiOperation(value = "查询账号下的所有子账户信息", notes = "根据账户，获取该账户下的所有子账户信息", response = ResultModel.class, tags = { "用户接口" })
	public ResponseEntity<ResultModel> getAccounts(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "target", value = "要监控的账户(获取token时使用的经销商账户或者其子账户)", required = true) String target, HttpServletRequest request);

	@ApiOperation(value = "查询账户下所有设备信息", notes = "根据账户，获取该账户下所有IMEI信息", response = ResultModel.class, tags = { "用户接口" })
	public ResponseEntity<ResultModel> getDevices(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "target", value = "要监控的账户(获取token时使用的经销商账户或者其子账户)", required = true) String target, HttpServletRequest request);

	@ApiOperation(value = "查询账户下所有设备最新位置信息", notes = "根据账户，获取该账户下所有IMEI的最新定位数据", response = ResultModel.class, tags = { "用户接口" })
	public ResponseEntity<ResultModel> getAllLocations(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "target", value = "要监控的账户(获取token时使用的经销商账户或者其子账户)", required = true) String target, 
			@ApiParam(name = "map_type", value = "地图类型(BAIDU/GOOGLE)", defaultValue = "BAIDU", required = false) String map_type, HttpServletRequest request);
	
	@ApiOperation(value = "创建平台账号", notes = "给指定账号创建子账号", response = ResultModel.class, tags = { "用户接口" })
	public ResponseEntity<ResultModel> createUsers(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "top_account", value = "上级账号", required = true) String topAccount, 
			@ApiParam(name = "nick_name", value = "客户名称", required = true) String nickName, 
			@ApiParam(name = "account_type", value = "账号类型(8:代理商,9:普通用户,11:销售)", required = true) String accountType, 
			@ApiParam(name = "account", value = "登录账号", required = true) String account, 
			@ApiParam(name = "password", value = "密码(md5加密后的值)", required = true) String password, 
			@ApiParam(name = "phone", value = "电话(建议填写、用于找回密码)", required = false) String phone, 
			@ApiParam(name = "email", value = "邮箱", required = false) String email, 
			@ApiParam(name = "contact", value = "联系人", required = false) String contact, 
			@ApiParam(name = "company_name", value = "公司名", required = false) String companyName, HttpServletRequest request);
}
