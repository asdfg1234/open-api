/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.api.user.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.commons.DatetimeUtil;

/**
 * @FileName CoreUserVo.java
 * @Description: 
 *
 * @Date 2017年4月13日 下午8:09:07
 * @author li.shangzhi
 * @version 1.0
 */
public class CoreUserVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 登录帐号(唯一索引)
	 */
	private String account;
	/**
	 * 显示名称
	 */
	private String name;
	/**
	 * 帐号类型(唯一索引):0超级管理员，1运营商，2店员，3终端用户 5手机注册 6PC注册 7平台内部添加 ,8.一级代理商 9.普通用户
	 * 10.普通代理商 11.销售 12.体验账号
	 */
	private Integer type;
	/**
	 * 是否启用(1:启用,0:不启用)
	 */
	private Integer displayFlag;
	/**
	 * 所在地
	 */
	private String address;
	/**
	 * 生日
	 */
	private Date birth;
	/**
	 * 公司名
	 */
	private String companyName;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 语言(zh,en)
	 */
	private String language;
	/**
	 * 性别:0男，1女
	 */
	private Integer sex;
	/**
	 * 标志:1可用，0不可用
	 */
	private Integer enabledFlag;
	/**
	 * 备注
	 */
	private String remark;

	public CoreUserVo() {
	}

	public void setType(Integer type) {
		type = type == null ? 0 : type;
		this.type = type;
	}

	public Integer getType() {
		return this.type == null ? 0 : this.type;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAccount() {
		return this.account == null ? null : this.account.trim();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name == null ? null : this.name.trim();
	}

	public void setDisplayFlag(Integer displayFlag) {
		displayFlag = displayFlag == null ? 0 : displayFlag;
		this.displayFlag = displayFlag;
	}

	public Integer getDisplayFlag() {
		return this.displayFlag == null ? 0 : this.displayFlag;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return this.address == null ? null : this.address.trim();
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public String getBirth() {
		if (this.birth == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.birth, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return this.companyName == null ? null : this.companyName.trim();
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return this.email == null ? null : this.email.trim();
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return this.phone == null ? null : this.phone.trim();
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguage() {
		return this.language == null ? null : this.language.trim();
	}

	public void setSex(Integer sex) {
		sex = sex == null ? 0 : sex;
		this.sex = sex;
	}

	public Integer getSex() {
		return this.sex == null ? 0 : this.sex;
	}

	public void setEnabledFlag(Integer enabledFlag) {
		enabledFlag = enabledFlag == null ? 0 : enabledFlag;
		this.enabledFlag = enabledFlag;
	}

	public Integer getEnabledFlag() {
		return this.enabledFlag == null ? 0 : this.enabledFlag;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return this.remark == null ? null : this.remark.trim();
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
