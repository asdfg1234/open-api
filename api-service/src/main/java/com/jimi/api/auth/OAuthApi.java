/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.api.auth;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.jimi.framework.utils.ResultModel;

/**
 * @FileName OAuthApi.java
 * @Description:
 *
 * @Date 2017年4月10日 下午3:59:42
 * @author li.shangzhi
 * @version 1.0
 */
@Api(value = "OAuth")
public interface OAuthApi {

	@ApiOperation(value = "获取Appkey和AppSecret", notes = "获取Appkey和AppSecret", response = ResultModel.class, tags = { "认证授权" })
	public ResponseEntity<ResultModel> getAppkeyAndSecret(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
//			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "user_id", value = "被创建人账号", required = true) String userId,
			@ApiParam(name = "user_pwd", value = "被创建人账号的密码(md5值)", required = true) String userPwd,
			@ApiParam(name = "project_code", value = "项目编码(TUQIANG)", required = true, defaultValue = "TUQIANG") String projectCode,
			@ApiParam(name = "app_desc", value = "被创建人描述", required = true) String appDesc,
			HttpServletRequest request);

	@ApiOperation(value = "获取AccessToken", notes = "获取AccessToken", response = ResultModel.class, tags = { "认证授权" })
	public ResponseEntity<ResultModel> getAccessToken(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "user_id", value = "用户Id", required = true) String userId,
			@ApiParam(name = "user_pwd_md5", value = "用户ID密码的MD5值", required = true) String userPwdmd5,
			@ApiParam(name = "expires_in", value = "token有效期(60-7200)秒", required = true, defaultValue = "60") int expiresIn,
			HttpServletRequest request);
	
	@ApiOperation(value = "刷新AccessToken", notes = "刷新AccessToken", response = ResultModel.class, tags = { "认证授权" })
	public ResponseEntity<ResultModel> refreshAccessToken(
//			@ApiParam(name = "method", value = "请求方法", required = true) String method,
			@ApiParam(name = "app_key", value = "分配给应用的AppKey", required = true) String appKey,
//			@ApiParam(name = "timestamp", value = "时间戳，格式为yyyy-MM-dd HH:mm:ss", required = true) String timestamp,
//			@ApiParam(name = "format", value = "响应格式(json)", required = true, defaultValue = "json") String format,
//			@ApiParam(name = "v", value = "API协议版本", required = true, defaultValue = "1.0") String v,
//			@ApiParam(name = "sign_method", value = "签名方法(md5)", required = true, defaultValue = "md5") String signMethod,
//			@ApiParam(name = "sign", value = "签名", required = true) String sign,
			@ApiParam(name = "access_token", value = "授权的令牌", required = true) String accessToken,
			@ApiParam(name = "refresh_token", value = "授权刷新的令牌", required = true) String refreshToken,
			@ApiParam(name = "expires_in", value = "token有效期(60-7200)秒", required = true, defaultValue = "60") int expiresIn,
			HttpServletRequest request);
}
