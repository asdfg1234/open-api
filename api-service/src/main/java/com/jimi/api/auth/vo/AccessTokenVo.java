/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月27日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.api.auth.vo;

import java.io.Serializable;

/**
 * @FileName AccessTokenVo.java
 * @Description: 
 *
 * @Date 2017年4月27日 上午10:15:51
 * @author li.shangzhi
 * @version 1.0
 */
public class AccessTokenVo implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 总账号的appkey
	 */
	private String appKey;

	/**
	 * 申请的账号
	 */
	private String account;

	/**
	 * 申请到的accessToken
	 */
	private String accessToken;
	
	/**
	 * 申请到的refreshToken
	 */
	private String refreshToken;

	/**
	 * token有效期
	 */
	private int expiresIn;
	
	private String time;

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
}
