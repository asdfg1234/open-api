/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.configuration.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-04-10T09:50:15.354Z")
/**
 * @FileName SwaggerDocumentationConfig.java
 * @Description: Swagger2配置类
 *
 * @Date 2017年4月10日 下午2:03:33
 * @author li.shangzhi
 * @version 1.0
 */
@Configuration
public class SwaggerDocumentationConfig {

	public ApiInfo apiInfo() {
		return new ApiInfoBuilder()
			.title("jimi-open-api")
			.description("客户端RESTful web api，提供h5、app、android的接口")
			.license("")
			.licenseUrl("")
			.termsOfServiceUrl("")
			.version("1.0.0")
			.contact(new Contact("", "", ""))
			.build();
	}

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
	        .select()
	            .apis(RequestHandlerSelectors.basePackage("com.jimi.api"))
	            .build()
	        .directModelSubstitute(org.joda.time.LocalDate.class, java.sql.Date.class)
	        .directModelSubstitute(org.joda.time.DateTime.class, java.util.Date.class)
	        .apiInfo(apiInfo());
	}
}
