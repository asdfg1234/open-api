/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.base;

import java.util.Enumeration;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.Modifier;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/**
 * @FileName BaseController.java
 * @Description:
 *
 * @Date 2017年4月10日 下午2:44:39
 * @author li.shangzhi
 * @version 1.0
 */
public class BaseController {

	protected final Logger logger = LoggerFactory.getLogger(super.getClass().getName());

	public String getAccessToken(HttpServletRequest request) {
		return request.getParameter("access_token");
	}

	/**
	 * 请求参数通过javassist反射绑定Bean
	 * 
	 * @param bean
	 * @param request
	 * @return
	 */
	public Object getParamBean(Class bean, HttpServletRequest request) {
		Object obj = null;
		try {
			obj = bean.newInstance();
			// 获得当前类名
			String clazzName = Thread.currentThread().getStackTrace()[2].getClassName();
			// 获得当前方法名
			String strMethod = Thread.currentThread().getStackTrace()[2].getMethodName();
			// 使用javassist的反射方法的参数名
			Class<?> clazz = Class.forName(clazzName);
			ClassPool pool = ClassPool.getDefault();
			CtClass ctClass = pool.get(clazz.getName());
			CtMethod ctMethod = ctClass.getDeclaredMethod(strMethod);
			MethodInfo methodInfo = ctMethod.getMethodInfo();
			CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
			LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
			BeanWrapper beanWrapper = new BeanWrapperImpl(obj);
			Enumeration<String> e = request.getParameterNames();
			String parameterName, parameterValue;
			while (e.hasMoreElements()) {
				parameterName = e.nextElement();
				parameterValue = request.getParameter(parameterName);
				if (attr != null) {
					int len = ctMethod.getParameterTypes().length;
					int pos = Modifier.isStatic(ctMethod.getModifiers()) ? 0 : 1;
					for (int i = 0; i < len; i++) {
						String paramName = attr.variableName(i + pos);
						if (paramName.equals(parameterName)) {
							beanWrapper.setPropertyValue(parameterName, parameterValue);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("接收参数时发生异常", e);
		}
		return obj;
	}
}
