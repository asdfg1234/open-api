/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.framework.oval;

import net.sf.oval.guard.GuardInterceptor;

/**
 * @FileName MyGuardInterceptor.java
 * @Description: oval校验拦截器
 *
 * @Date 2017年4月10日 下午2:54:07
 * @author li.shangzhi
 * @version 1.0
 */
public class MyGuardInterceptor extends GuardInterceptor {

	public MyGuardInterceptor(){
        super(new MyGuard());
    }
}
