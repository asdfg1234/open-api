/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月14日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.framework.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * @FileName ValidatorUtils.java
 * @Description: 
 *
 * @Date 2017年4月14日 下午4:21:55
 * @author li.shangzhi
 * @version 1.0
 */
public class ValidatorUtils {

	/**
	 * 电话正则式
	 */
	private static final Pattern phoneRule = Pattern.compile("0\\d{2,3}-\\d{7,8}");
	/**
	 * 手机正则式 13,14,15,18段
	 */
	private static final Pattern mobileRule = Pattern.compile("^[1][3,4,5,8][0-9]{9}$");

	/**
	 * IMEI规则
	 */
	private static final Pattern imeiRule = Pattern.compile("\\d{15,16}");

	/**
	 * 
	 * @param phonenumber
	 * @return
	 * @author chengxuwei 2015年1月17日
	 */
	public static boolean isPhone(String no) {
		boolean match = false;
		if (StringUtils.isNotBlank(no)) {
			Matcher m = phoneRule.matcher(no);
			match = m.matches();
		}
		return match;
	}

	/**
	 * 手机格式验证
	 * 
	 * @param mobiles
	 * @return
	 * @author chengxuwei 2015年1月17日
	 */
	public static boolean isMobile(String no) {
		boolean match = false;
		if (StringUtils.isNotBlank(no)) {
			Matcher m = mobileRule.matcher(no);
			match = m.matches();
		}
		return match;
	}

	/**
	 * IMEI格式验证
	 * 
	 * @param imei
	 * @return
	 * @author chengxuwei 2015年1月17日
	 */
	public static boolean isImei(String imei) {
		boolean match = false;
		if (StringUtils.isNotBlank(imei)) {
			Matcher m = imeiRule.matcher(imei);
			match = m.matches();
		}
		return match;
	}
}
