package com.jimi.framework.utils;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jimi.exception.IErrorCode;
import com.jimi.utils.MessageSourceUtil;

import io.swagger.annotations.ApiModelProperty;

/**
 * Error
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-12-22T15:18:40.440Z")
public class ResultModel {

	@JsonProperty("code")
	private Integer code = null;

	@JsonProperty("message")
	private String message = null;

	@JsonProperty("result")
	private Object result = null;

	public ResultModel() {
		this.code = 0;
		this.message = "success";
	}

	public ResultModel(Integer code) {
		if (code == -1) {
			this.code = -1;
			this.message = "系统繁忙";
		}
	}

	public ResultModel(Integer code, String msg) {
		this.code = code;
		this.message = msg;
	}

	public ResultModel(IErrorCode errorCode) {
		this.code = errorCode.getCode();
		String msg = MessageSourceUtil.getMessage(errorCode.getErrorKey());
		if (StringUtils.isNotBlank(msg)) {
			this.message = msg;
		} else {
			this.message = errorCode.getErrorMsg();
		}
	}

	/**
	 * Get code
	 * 
	 * @return code
	 **/
	@ApiModelProperty(value = "")
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public ResultModel message(String message) {
		this.message = message;
		return this;
	}

	/**
	 * Get message
	 * 
	 * @return message
	 **/
	@ApiModelProperty(value = "")
	public String getMessage() {
		return message;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public ResultModel result(Object result) {
		this.result = result;
		return this;
	}

	/**
	 * Get message
	 * 
	 * @return message
	 **/
	@ApiModelProperty(value = "")
	public Object getResult() {
		return result;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ResultModel result = (ResultModel) o;
		return Objects.equals(this.code, result.code) && Objects.equals(this.message, result.message)
				&& Objects.equals(this.result, result.result);
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, message, result);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Error {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("    result: ").append(toIndentedString(result)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
