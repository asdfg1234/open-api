/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年6月15日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.utils;

import java.io.StringWriter;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jimi.commons.DatetimeUtil;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

/**
 * @FileName FreeMarkerUtil.java
 * @Description:
 *
 * @Date 2017年6月15日 上午10:58:37
 * @author li.shangzhi
 * @version 1.0
 */
public class FreeMarkerUtil {

	private static final Logger logger = LoggerFactory.getLogger(FreeMarkerUtil.class);

	@SuppressWarnings("rawtypes")
	public static String getContent(String ftlPath, Map data) {
		StringWriter writer = new StringWriter();
		try {
			Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
			cfg.setTemplateLoader(new ClassTemplateLoader(FreeMarkerUtil.class, "/pagebuild"));
			cfg.setObjectWrapper(new DefaultObjectWrapper(cfg.getVersion()));
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
			cfg.setDefaultEncoding("UTF-8");
			Template template = cfg.getTemplate(ftlPath);
			template.process(data, writer);
		} catch (Exception e) {
			logger.error("[FreeMarkerUtil]模板生成时发生异常,ftlPath={}", ftlPath, e);
			return "";
		}
		return writer.toString();
	}
}
