/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.jimi.framework.cache.redis.RedisUtil;
import com.jimi.framework.cache.redis.RedisUtil.ModuleType;
import com.jimi.web.dto.output.AccessTokenOutputDto;

/**
 * @FileName SessionUtil.java
 * @Description: 会话工具类
 *
 * @Date 2017年4月10日 下午8:35:29
 * @author li.shangzhi
 * @version 1.0
 */
public class SessionUtil {

	private static final Logger logger = LoggerFactory.getLogger(SessionUtil.class);
	/**
	 * 验证sessionId
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static String getLoginaccount(HttpServletRequest request) {
		try {
			String token = request.getParameter("access_token");

			if (StringUtils.isNotBlank(token)) {

				AccessTokenOutputDto outputDto = JSONObject.parseObject(RedisUtil.getString(ModuleType.OAUTH, token), AccessTokenOutputDto.class);

				if (outputDto != null) {
					return "success";
				}
				logger.info("在redis中找不到access_token，token值为：{}", token);
				return "fail";
			} else {
				logger.info("请求参数access_token不存在");
				return null;
			}

		} catch (Exception e) {
			logger.error("验证access_token有效时发生异常。", e);
			return null;
		}
	}
}
