/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.interceptor.loginsecurity;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @FileName LoginSecurity.java
 * @Description: 登录校验注解
 *
 * @Date 2017年4月10日 下午8:28:23
 * @author li.shangzhi
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@Inherited
public @interface LoginSecurity {

	/**
	 * 登录成功后回到原地址
	 */
	public final static String BACK = "BACK";

	/**
	 * 登录成功后不回到原地址
	 */
	public final static String DEFAULT = "DEFAULT";

	/**
	 * json 格式
	 * 
	 * @return
	 */
	String json() default "{\"code\": 1001, \"message\": \"未通过身份验证\", \"result\": null}";

	/**
	 * 登录成功后指定跳转地址
	 * 
	 */
	String redirect() default BACK;
}