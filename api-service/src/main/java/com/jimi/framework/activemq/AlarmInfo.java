/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月9日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.activemq;

/**
 * @FileName AlarmInfo.java
 * @Description:
 *
 * @Date 2017年5月9日 下午5:26:00
 * @author li.shangzhi
 * @version 1.0
 */
public class AlarmInfo {

	/**
	 * 消息类型，alarm：告警，后续扩展
	 */
	private String msgType;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 设备imei
	 */
	private String imei;

	/**
	 * 设备名称
	 */
	private String deviceName;

	/**
	 * 告警类型
	 */
	private String alarmType;

	/**
	 * 告警名称
	 */
	private String alarmName;

	/**
	 * 纬度
	 */
	private double lng;

	/**
	 * 经度
	 */
	private double lat;

	/**
	 * 告警时间
	 */
	private String alarmTime;

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(String alarmType) {
		this.alarmType = alarmType;
	}

	public String getAlarmName() {
		return alarmName;
	}

	public void setAlarmName(String alarmName) {
		this.alarmName = alarmName;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public String getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(String alarmTime) {
		this.alarmTime = alarmTime;
	}

}
