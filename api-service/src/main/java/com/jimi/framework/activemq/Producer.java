/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月2日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.activemq;

//import javax.jms.Queue;

//import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.jms.core.JmsMessagingTemplate;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Service;

/**
 * @FileName Producer.java
 * @Description:
 *
 * @Date 2017年5月2日 下午3:54:55
 * @author li.shangzhi
 * @version 1.0
 */
//@Service("producer")
//@EnableScheduling
public class Producer {

//	@Autowired
	// 也可以注入JmsTemplate，JmsMessagingTemplate对JmsTemplate进行了封装
//	private JmsMessagingTemplate jmsTemplate;
	
//	@Autowired
//	private Queue queue;

	// 发送消息，destination是发送到的队列，message是待发送的消息
//	@Scheduled(fixedDelay=3000)//每3s执行1次
	public void sendMessage() {
//		jmsTemplate.convertAndSend(queue, "测试mq");
	}
}
