/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月2日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.activemq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.jimi.app.api.IApiAppPushSetApi;
import com.jimi.app.dto.output.ApiAppPushSetOutputDto;
import com.jimi.web.util.http.Result;
import com.jimi.web.util.http.SendRequest;

/**
 * @FileName OpenApiConsumer.java
 * @Description:
 *
 * @Date 2017年5月2日 下午3:55:53
 * @author li.shangzhi
 * @version 1.0
 */
@Component
public class OpenApiConsumer {

	private static final Logger logger = LoggerFactory.getLogger(OpenApiConsumer.class);

	@Autowired
	private IApiAppPushSetApi apiAppPushSetApi;

	// 使用JmsListener配置消费者监听的队列，其中text是接收到的消息,创建5-10个消费者处理
	@JmsListener(destination = "queue.TUQIANG.pushAlarm", concurrency = "5-10")
	public void receivePushAlarm(byte[] text) {
		// 向第三方系统推送告警
		AlarmInfo alarmInfo = JSONObject.parseObject(text, AlarmInfo.class);
		ApiAppPushSetOutputDto appInfo = apiAppPushSetApi.getById(alarmInfo.getUserId());
		if (appInfo.getEnabled()) {
			String url = appInfo.getPushUrl();
			Map<String, Object> alarm = new HashMap<>();
			alarm.put("imei", alarmInfo.getImei());
			alarm.put("deviceName", alarmInfo.getDeviceName());
			alarm.put("alarmType", getAlarmType(alarmInfo.getAlarmType()));
			alarm.put("alarmName", alarmInfo.getAlarmName());
			alarm.put("lat", alarmInfo.getLat());
			alarm.put("lng", alarmInfo.getLng());
			alarm.put("alarmTime", alarmInfo.getAlarmTime());

			Map<String, String> params = new HashMap<>();
			params.put("msgType", "jimi.push.device.alarm");
			params.put("data", JSONObject.toJSONString(alarm));
			try {
				Result result = SendRequest.sendPost(url, null, params, "utf-8");
				logger.info("向{}推送告警信息成功，消息内容：{}", appInfo.getAccount(), params);
			} catch (IOException | InterruptedException | ExecutionException e) {
				logger.error("向{}推送告警信息失败，消息内容：{}", appInfo.getAccount(), params, e);
			}
		}
	}

	private String getAlarmType(String alarmType) {
		switch (alarmType) {
		case "ACC_OFF":
			// ACC关闭
			alarmType = "1001";
			break;
		case "ACC_ON":
			// ACC开启
			alarmType = "1002";
			break;
		case "offline":
			// 离线告警
			alarmType = "1003";
			break;
		case "stayAlert":
			// 停留告警
			alarmType = "1004";
			break;
		case "lingerAlert":
			// 逗留告警
			alarmType = "1005";
			break;
		case "in":
			// 进入围栏
			alarmType = "1006";
			break;
		case "out":
			// 离开围栏
			alarmType = "1007";
			break;
		case "stayTimeIn":
			// 长时间不进
			alarmType = "1008";
			break;
		case "stayTimeOut":
			// 长时间不出
			alarmType = "1009";
			break;
		default:
			break;
		}
		return alarmType;
	}
}