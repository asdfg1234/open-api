<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<title>操控台 - Tracker</title>
<style>
body {
	margin: 0;
	padding: 0;
	border: 0;
}

.map-popover {
	position: absolute;
	top: 100px;
	left: 100px;
	z-index: 9;
	width: 270px;
	font-size: 12px;
	border: 1px solid #aaa;
	border-radius: 4px;
	box-shadow: 3px 3px 3px rgba(0, 0, 0, .2);
	background-color: #fff;
	font-family: "Helvetica Neue", Helvetica, arial, microsoft yahei,
		"\5B8B\4F53";
}

.map-popover:before, .map-popover:after {
	content: "";
	position: absolute;
	left: 50%;
	bottom: -20px;
	z-index: 2;
	width: 0;
	height: 0;
	margin-left: -5px;
	border-width: 10px 8px;
	border-style: solid;
	border-color: #f9f9f9 transparent transparent #f9f9f9;
}

.map-popover:after {
	border-width: 11px 9px;
	bottom: -23px;
	z-index: 1;
	margin-left: -6px;
	border-color: rgba(0, 0, 0, .2) transparent transparent
		rgba(0, 0, 0, .2);
}

.popover-header {
	border-bottom: 1px solid #ddd;
	background-color: #f9f9f9;
	border-radius: 4px 4px 0 0;
	box-shadow: inset 0 1px 0 #fff;
}

.popover-header, .popover-body, .popover-footer {
	padding: 4px 15px;
}

.popover-header h5 {
	margin: 0;
}

.popover-body table {
	table-layout: fixed;
}

.popover-body table tr th {
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

.popover-body table th, .popover-body table td {
	padding: 2px;
	vertical-align: top;
}

.popover-footer {
	border-top: 1px solid #ddd;
	background-color: #f9f9f9;
	border-radius: 0 0 4px 4px;
	box-shadow: inset 0 1px 0 #fff;
}

.popover-footer a {
	display: inline-block;
	margin-right: 8px;
	max-width: 100px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	color: #0087d6;
	cursor: pointer;
}

.map-popover table th {
	padding-right: 5px;
	text-align: right;
	font-weight: normal;
}
</style>
</head>

<body>
	<div>
		<div id="allMap" style="width: 100%; height: 800px;"></div>
	</div>
	<input type="hidden" id="imei" value="${data.imei}"></input>
	<input type="hidden" id="deviceName" value="${data.deviceName}"></input>
	<input type="hidden" id="icons" value="${data.icon}"></input>
	<input type="hidden" id="status" value="${data.status}"></input>
	<input type="hidden" id="lat" value="${data.lat?string('#.###############')}"></input>
	<input type="hidden" id="lng" value="${data.lng?string('#.###############')}"></input>
	<input type="hidden" id="expireFlag" value="${data.expireFlag}"></input>
	<input type="hidden" id="activationFlag" value="${data.activationFlag}"></input>
	<input type="hidden" id="posType" value="${data.posType}"></input>
	<input type="hidden" id="locDesc" value="${data.locDesc}"></input>
	<input type="hidden" id="gpsTime" value="${data.gpsTime?string("yyyy-MM-dd HH:mm:ss")}"></input>
	<input type="hidden" id="hbTime" value="${data.hbTime?string("yyyy-MM-dd HH:mm:ss")}"></input>
	<input type="hidden" id="speed" value="${data.speed}"></input>
	<input type="hidden" id="accStatus" value="${data.accStatus}"></input>
	<input type="hidden" id="electQuantity" value="${data.electQuantity}"></input>
	<input type="hidden" id="powerValue" value="${data.powerValue}"></input>

	<script type="text/javascript"
		src="http://cdn.bootcss.com/jquery/1.8.3/jquery.min.js"></script>
	<script>
    /*!art-template - Template Engine | http://aui.github.com/artTemplate/*/
    !function(){function a(a){return a.replace(t,"").replace(u,",").replace(v,"").replace(w,"").replace(x,"").split(y)}function b(a){return"'"+a.replace(/('|\\)/g,"\\$1").replace(/\r/g,"\\r").replace(/\n/g,"\\n")+"'"}function c(c,d){function e(a){return m+=a.split(/\n/).length-1,k&&(a=a.replace(/\s+/g," ").replace(/<!--[\w\W]*?-->/g,"")),a&&(a=s[1]+b(a)+s[2]+"\n"),a}function f(b){var c=m;if(j?b=j(b,d):g&&(b=b.replace(/\n/g,function(){return m++,"$line="+m+";"})),0===b.indexOf("=")){var e=l&&!/^=[=#]/.test(b);if(b=b.replace(/^=[=#]?|[\s;]*$/g,""),e){var f=b.replace(/\s*\([^\)]+\)/,"");n[f]||/^(include|print)$/.test(f)||(b="$escape("+b+")")}else b="$string("+b+")";b=s[1]+b+s[2]}return g&&(b="$line="+c+";"+b),r(a(b),function(a){if(a&&!p[a]){var b;b="print"===a?u:"include"===a?v:n[a]?"$utils."+a:o[a]?"$helpers."+a:"$data."+a,w+=a+"="+b+",",p[a]=!0}}),b+"\n"}var g=d.debug,h=d.openTag,i=d.closeTag,j=d.parser,k=d.compress,l=d.escape,m=1,p={$data:1,$filename:1,$utils:1,$helpers:1,$out:1,$line:1},q="".trim,s=q?["$out='';","$out+=",";","$out"]:["$out=[];","$out.push(",");","$out.join('')"],t=q?"$out+=text;return $out;":"$out.push(text);",u="function(){var text=''.concat.apply('',arguments);"+t+"}",v="function(filename,data){data=data||$data;var text=$utils.$include(filename,data,$filename);"+t+"}",w="'use strict';var $utils=this,$helpers=$utils.$helpers,"+(g?"$line=0,":""),x=s[0],y="return new String("+s[3]+");";r(c.split(h),function(a){a=a.split(i);var b=a[0],c=a[1];1===a.length?x+=e(b):(x+=f(b),c&&(x+=e(c)))});var z=w+x+y;g&&(z="try{"+z+"}catch(e){throw {filename:$filename,name:'Render Error',message:e.message,line:$line,source:"+b(c)+".split(/\\n/)[$line-1].replace(/^\\s+/,'')};}");try{var A=new Function("$data","$filename",z);return A.prototype=n,A}catch(B){throw B.temp="function anonymous($data,$filename) {"+z+"}",B}}var d=function(a,b){return"string"==typeof b?q(b,{filename:a}):g(a,b)};d.version="3.0.0",d.config=function(a,b){e[a]=b};var e=d.defaults={openTag:"<%",closeTag:"%>",escape:!0,cache:!0,compress:!1,parser:null},f=d.cache={};d.render=function(a,b){return q(a,b)};var g=d.renderFile=function(a,b){var c=d.get(a)||p({filename:a,name:"Render Error",message:"Template not found"});return b?c(b):c};d.get=function(a){var b;if(f[a])b=f[a];else if("object"==typeof document){var c=document.getElementById(a);if(c){var d=(c.value||c.innerHTML).replace(/^\s*|\s*$/g,"");b=q(d,{filename:a})}}return b};var h=function(a,b){return"string"!=typeof a&&(b=typeof a,"number"===b?a+="":a="function"===b?h(a.call(a)):""),a},i={"<":"&#60;",">":"&#62;",'"':"&#34;","'":"&#39;","&":"&#38;"},j=function(a){return i[a]},k=function(a){return h(a).replace(/&(?![\w#]+;)|[<>"']/g,j)},l=Array.isArray||function(a){return"[object Array]"==={}.toString.call(a)},m=function(a,b){var c,d;if(l(a))for(c=0,d=a.length;d>c;c++)b.call(a,a[c],c,a);else for(c in a)b.call(a,a[c],c)},n=d.utils={$helpers:{},$include:g,$string:h,$escape:k,$each:m};d.helper=function(a,b){o[a]=b};var o=d.helpers=n.$helpers;d.onerror=function(a){var b="Template Error\n\n";for(var c in a)b+="<"+c+">\n"+a[c]+"\n\n";"object"==typeof console&&console.error(b)};var p=function(a){return d.onerror(a),function(){return"{Template Error}"}},q=d.compile=function(a,b){function d(c){try{return new i(c,h)+""}catch(d){return b.debug?p(d)():(b.debug=!0,q(a,b)(c))}}b=b||{};for(var g in e)void 0===b[g]&&(b[g]=e[g]);var h=b.filename;try{var i=c(a,b)}catch(j){return j.filename=h||"anonymous",j.name="Syntax Error",p(j)}return d.prototype=i.prototype,d.toString=function(){return i.toString()},h&&b.cache&&(f[h]=d),d},r=n.$each,s="break,case,catch,continue,debugger,default,delete,do,else,false,finally,for,function,if,in,instanceof,new,null,return,switch,this,throw,true,try,typeof,var,void,while,with,abstract,boolean,byte,char,class,const,double,enum,export,extends,final,float,goto,implements,import,int,interface,long,native,package,private,protected,public,short,static,super,synchronized,throws,transient,volatile,arguments,let,yield,undefined",t=/\/\*[\w\W]*?\*\/|\/\/[^\n]*\n|\/\/[^\n]*$|"(?:[^"\\]|\\[\w\W])*"|'(?:[^'\\]|\\[\w\W])*'|\s*\.\s*[$\w\.]+/g,u=/[^\w$]+/g,v=new RegExp(["\\b"+s.replace(/,/g,"\\b|\\b")+"\\b"].join("|"),"g"),w=/^\d[^,]*|,\d[^,]*/g,x=/^,+|,+$/g,y=/^$|,+/;e.openTag="{{",e.closeTag="}}";var z=function(a,b){var c=b.split(":"),d=c.shift(),e=c.join(":")||"";return e&&(e=", "+e),"$helpers."+d+"("+a+e+")"};e.parser=function(a){a=a.replace(/^\s/,"");var b=a.split(" "),c=b.shift(),e=b.join(" ");switch(c){case"if":a="if("+e+"){";break;case"else":b="if"===b.shift()?" if("+b.join(" ")+")":"",a="}else"+b+"{";break;case"/if":a="}";break;case"each":var f=b[0]||"$data",g=b[1]||"as",h=b[2]||"$value",i=b[3]||"$index",j=h+","+i;"as"!==g&&(f="[]"),a="$each("+f+",function("+j+"){";break;case"/each":a="});";break;case"echo":a="print("+e+");";break;case"print":case"include":a=c+"("+b.join(",")+");";break;default:if(/^\s*\|\s*[\w\$]/.test(e)){var k=!0;0===a.indexOf("#")&&(a=a.substr(1),k=!1);for(var l=0,m=a.split("|"),n=m.length,o=m[l++];n>l;l++)o=z(o,m[l]);a=(k?"=":"=#")+o}else a=d.helpers[c]?"=#"+c+"("+b.join(",")+");":"="+a}return a},"function"==typeof define?define(function(){return d}):"undefined"!=typeof exports?module.exports=d:this.template=d}();
    </script>
	<!-- map -->
	<script type="text/javascript"
		src="http://api.map.baidu.com/api?v=2.0&ak=6AsXWbkycdKilWXGGt8ZzZea"></script>
	<!-- 显示车辆信息气泡 -->
	<script>
    var BMapLib=window.BMapLib=BMapLib||{};var INFOBOX_AT_TOP=1,INFOBOX_AT_RIGHT=2,INFOBOX_AT_BOTTOM=3,INFOBOX_AT_LEFT=4;(function(){var b,a=b=a||{version:"1.5.0"};a.guid="$BAIDU$";(function(){window[a.guid]=window[a.guid]||{};a.lang=a.lang||{};a.lang.isString=function(d){return"[object String]"==Object.prototype.toString.call(d)};a.lang.isFunction=function(d){return"[object Function]"==Object.prototype.toString.call(d)};a.lang.Event=function(d,e){this.type=d;this.returnValue=true;this.target=e||null;this.currentTarget=null};a.object=a.object||{};a.extend=a.object.extend=function(f,d){for(var e in d){if(d.hasOwnProperty(e)){f[e]=d[e]}}return f};a.event=a.event||{};a.event._listeners=a.event._listeners||[];a.dom=a.dom||{};a.dom._g=function(d){if(a.lang.isString(d)){return document.getElementById(d)}return d};a._g=a.dom._g;a.event.on=function(e,h,j){h=h.replace(/^on/i,"");e=a.dom._g(e);var i=function(l){j.call(e,l)},d=a.event._listeners,g=a.event._eventFilter,k,f=h;h=h.toLowerCase();if(g&&g[h]){k=g[h](e,h,i);f=k.type;i=k.listener}if(e.addEventListener){e.addEventListener(f,i,false)}else{if(e.attachEvent){e.attachEvent("on"+f,i)}}d[d.length]=[e,h,j,i,f];return e};a.on=a.event.on;a.event.un=function(f,i,e){f=a.dom._g(f);i=i.replace(/^on/i,"").toLowerCase();var l=a.event._listeners,g=l.length,h=!e,k,j,d;while(g--){k=l[g];if(k[1]===i&&k[0]===f&&(h||k[2]===e)){j=k[4];d=k[3];if(f.removeEventListener){f.removeEventListener(j,d,false)}else{if(f.detachEvent){f.detachEvent("on"+j,d)}}l.splice(g,1)}}return f};a.un=a.event.un;a.dom.g=function(d){if("string"==typeof d||d instanceof String){return document.getElementById(d)}else{if(d&&d.nodeName&&(d.nodeType==1||d.nodeType==9)){return d}}return null};a.g=a.G=a.dom.g;a.dom._styleFixer=a.dom._styleFixer||{};a.dom._styleFilter=a.dom._styleFilter||[];a.dom._styleFilter.filter=function(e,h,j){for(var d=0,g=a.dom._styleFilter,f;f=g[d];d++){if(f=f[j]){h=f(e,h)}}return h};a.string=a.string||{};a.string.toCamelCase=function(d){if(d.indexOf("-")<0&&d.indexOf("_")<0){return d}return d.replace(/[-_][^-_]/g,function(e){return e.charAt(1).toUpperCase()})};a.dom.setStyle=function(f,e,g){var h=a.dom,d;f=h.g(f);e=a.string.toCamelCase(e);if(d=h._styleFilter){g=d.filter(e,g,"set")}d=h._styleFixer[e];(d&&d.set)?d.set(f,g):(f.style[d||e]=g);return f};a.setStyle=a.dom.setStyle;a.dom.setStyles=function(e,f){e=a.dom.g(e);for(var d in f){a.dom.setStyle(e,d,f[d])}return e};a.setStyles=a.dom.setStyles;a.browser=a.browser||{};a.browser.ie=a.ie=/msie (\d+\.\d+)/i.test(navigator.userAgent)?(document.documentMode||+RegExp["\x241"]):undefined;a.dom._NAME_ATTRS=(function(){var d={cellpadding:"cellPadding",cellspacing:"cellSpacing",colspan:"colSpan",rowspan:"rowSpan",valign:"vAlign",usemap:"useMap",frameborder:"frameBorder"};if(a.browser.ie<8){d["for"]="htmlFor";d["class"]="className"}else{d.htmlFor="for";d.className="class"}return d})();a.dom.setAttr=function(e,d,f){e=a.dom.g(e);if("style"==d){e.style.cssText=f}else{d=a.dom._NAME_ATTRS[d]||d;e.setAttribute(d,f)}return e};a.setAttr=a.dom.setAttr;a.dom.setAttrs=function(f,d){f=a.dom.g(f);for(var e in d){a.dom.setAttr(f,e,d[e])}return f};a.setAttrs=a.dom.setAttrs;a.dom.create=function(f,d){var g=document.createElement(f),e=d||{};return a.dom.setAttrs(g,e)};b.undope=true})();var c=BMapLib.InfoBox=function(f,e,d){this._content=e||"";this._isOpen=false;this._map=f;this._opts=d=d||{};this._opts.offset=d.offset||new BMap.Size(0,0);this._opts.boxClass=d.boxClass||"infoBox";this._opts.boxStyle=d.boxStyle||{};this._opts.closeIconMargin=d.closeIconMargin||"2px";this._opts.closeIconUrl=d.closeIconUrl||"close.png";this._opts.enableAutoPan=d.enableAutoPan?true:false;this._opts.align=d.align||INFOBOX_AT_TOP};c.prototype=new BMap.Overlay();c.prototype.initialize=function(e){var d=this;var g=this._div=a.dom.create("div",{"class":this._opts.boxClass});a.dom.setStyles(g,this._opts.boxStyle);g.style.position="absolute";this._setContent(this._content);var f=e.getPanes().floatPane;f.style.width="auto";f.appendChild(g);this._getInfoBoxSize();a.event.on(g,"onmousedown",function(h){d._stopBubble(h)});a.event.on(g,"onmouseover",function(h){d._stopBubble(h)});a.event.on(g,"click",function(h){d._stopBubble(h)});a.event.on(g,"dblclick",function(h){d._stopBubble(h)});return g};c.prototype.draw=function(){this._isOpen&&this._adjustPosition(this._point)};c.prototype.open=function(d){var e=this,f;if(!this._isOpen){this._map.addOverlay(this);this._isOpen=true;setTimeout(function(){e._dispatchEvent(e,"open",{point:e._point})},10)}if(d instanceof BMap.Point){f=d;this._removeMarkerEvt()}else{if(d instanceof BMap.Marker){if(this._marker){this._removeMarkerEvt()}f=d.getPosition();this._marker=d;!this._markerDragend&&this._marker.addEventListener("dragend",this._markerDragend=function(g){e._point=g.point;e._adjustPosition(e._point);e._panBox();e.show()});!this._markerDragging&&this._marker.addEventListener("dragging",this._markerDragging=function(){e.hide();e._point=e._marker.getPosition();e._adjustPosition(e._point)})}}this.show();this._point=f;this._panBox();this._adjustPosition(this._point)};c.prototype.close=function(){if(this._isOpen){this._map.removeOverlay(this);this._remove();this._isOpen=false;this._dispatchEvent(this,"close",{point:this._point})}};c.prototype.enableAutoPan=function(){this._opts.enableAutoPan=true};c.prototype.disableAutoPan=function(){this._opts.enableAutoPan=false};c.prototype.setContent=function(d){this._setContent(d);this._getInfoBoxSize();this._adjustPosition(this._point)};c.prototype.setPosition=function(d){this._point=d;this._adjustPosition(d);this._removeMarkerEvt()};c.prototype.getPosition=function(){return this._point};c.prototype.getOffset=function(){return this._opts.offset},c.prototype._remove=function(){var d=this;if(this.domElement&&this.domElement.parentNode){a.event.un(this._div.firstChild,"click",d._closeHandler());this.domElement.parentNode.removeChild(this.domElement)}this.domElement=null;this._isOpen=false;this.dispatchEvent("onremove")},a.object.extend(c.prototype,{_getCloseIcon:function(){var d="<img src='"+this._opts.closeIconUrl+"' align='right' style='position:absolute;right:0px;cursor:pointer;margin:"+this._opts.closeIconMargin+"'/>";return d},_setContent:function(e){if(!this._div){return}var d=this._getCloseIcon();if(typeof e.nodeType==="undefined"){this._div.innerHTML=d+e}else{this._div.innerHTML=d;this._div.appendChild(e)}this._content=e;this._addEventToClose()},_adjustPosition:function(f){var d=this._getPointPosition(f);var e=this._marker&&this._marker.getIcon();switch(this._opts.align){case INFOBOX_AT_TOP:if(this._marker){this._div.style.bottom=-(d.y-this._opts.offset.height-e.anchor.height+e.infoWindowAnchor.height)-this._marker.getOffset().height+2+"px"}else{this._div.style.bottom=-(d.y-this._opts.offset.height)+"px"}break;case INFOBOX_AT_BOTTOM:if(this._marker){this._div.style.top=d.y+this._opts.offset.height-e.anchor.height+e.infoWindowAnchor.height+this._marker.getOffset().height+"px"}else{this._div.style.top=d.y+this._opts.offset.height+"px"}break}if(this._marker){this._div.style.left=d.x-e.anchor.width+this._marker.getOffset().width+e.infoWindowAnchor.width-this._boxWidth/2+"px"}else{this._div.style.left=d.x-this._boxWidth/2+"px"}},_getPointPosition:function(d){this._pointPosition=this._map.pointToOverlayPixel(d);return this._pointPosition},_getInfoBoxSize:function(){this._boxWidth=parseInt(this._div.offsetWidth,10);this._boxHeight=parseInt(this._div.offsetHeight,10)},_addEventToClose:function(){var d=this;a.event.on(this._div.firstChild,"click",d._closeHandler());this._hasBindEventClose=true},_closeHandler:function(){var d=this;return function(f){d.close()}},_stopBubble:function(d){if(d&&d.stopPropagation){d.stopPropagation()}else{window.event.cancelBubble=true}},_panBox:function(){if(!this._opts.enableAutoPan){return}var i=parseInt(this._map.getContainer().offsetHeight,10),o=parseInt(this._map.getContainer().offsetWidth,10),j=this._boxHeight,d=this._boxWidth;if(j>=i||d>=o){return}if(!this._map.getBounds().containsPoint(this._point)){this._map.setCenter(this._point)}var e=this._map.pointToPixel(this._point),p,m,l,g=d/2-e.x,n=d/2+e.x-o;if(this._marker){var k=this._marker.getIcon()}switch(this._opts.align){case INFOBOX_AT_TOP:var f=this._marker?k.anchor.height+this._marker.getOffset().height-k.infoWindowAnchor.height:0;p=j-e.y+this._opts.offset.height+f+2;break;case INFOBOX_AT_BOTTOM:var f=this._marker?-k.anchor.height+k.infoWindowAnchor.height+this._marker.getOffset().height+this._opts.offset.height:0;m=j+e.y-i+f+4;break}panX=g>0?g:(n>0?-n:0);l=p>0?p:(m>0?-m:0);this._map.panBy(panX,l)},_removeMarkerEvt:function(){this._markerDragend&&this._marker.removeEventListener("dragend",this._markerDragend);this._markerDragging&&this._marker.removeEventListener("dragging",this._markerDragging);this._markerDragend=this._markerDragging=null},_dispatchEvent:function(d,e,g){e.indexOf("on")!=0&&(e="on"+e);var f=new a.lang.Event(e);if(!!g){for(var h in g){f[h]=g[h]}}d.dispatchEvent(f)}})})();
    </script>
	<script id="InfoWindowTpl" type="text/html">
    <div class="map-popover" style="{{if map_type == 'baiduMap'}}top:60px; left: 105px;{{else}}top:70px; left: 116px;{{/if}} width:300px">
      <div class="popover-header">
      <b>设备信息</b>
      </div>
      <div class="popover-body">
        <table style="table-layout: fixed;width: 100%; height:168px;">
          <tbody>
			<tr>
			  <th width="70">IMEI：</th>
              <td name='imei' title="{{imei}}">{{imei}}{{if status == "1"}}<span style="color: #1ed431;">(在线)</span>{{else}}<span style="color: #666;">(离线)</span>{{/if}}</td>
            </tr>
			<tr>
              <th title="设备名称">设备名称：</th>
              <td title="{{deviceName}}" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{deviceName}}</td>
            </tr>
			<tr>
              <th title="电话">电话：</th>
              <td title="{{phone}}">{{phone}}</td>
            </tr>
			{{if electricity!=null && electricity!=""}}
				<tr>
					<th title="电量">电量：</td>
					<td title="{{electricity}}%">{{electricity}}%</td>
				</tr>
			{{/if}}
			{{if icon !="cow" && icon !="per" && icon !="plane"}}
				{{if (positionType!=null) && (positionType !="") && (positionType!="BEACON")}}
					<tr>
              			<th title="ACC">ACC：</th>
             	 		<td title="{{if acc=="1"&&(status=="1")}}开启{{else}}关闭{{/if}}">{{if acc=="1"&&(status=="1")}}开启{{else}}关闭{{/if}}</td>
            		</tr>
				{{/if}}
				{{if positionType!=null && positionType !="" && positionType!="BEACON"}}
						<tr>
							<th title="速度">速度：</th>
								{{if positionType!=null && positionType !="" && positionType=="GPS"}}
									{{if status=="0"}}
										<td title="暂无速度">暂无速度</td>
									{{/if}}
									{{if status=="1"}}
										{{if speed<0}}
											<td title="暂无速度">暂无速度</td>
										{{else}}
											<td title="{{speed}}Km/h">{{speed}}Km/h</td>
										{{/if}}
									{{/if}}
								{{else}}
									<td title="暂无速度">暂无速度</td>
								{{/if}}
								 
						</tr>
			 	 {{/if}}
			{{else}}
				 {{if positionType && positionType!="BEACON"}}
						<tr>
							<th title="速度">速度：</th>
							{{if positionType!=null && positionType !="" && positionType=="GPS"}}
									{{if status=="0"}}
										<td title="暂无速度">暂无速度</td>
									{{else}}
										{{if speed>=0 && speed<=15}}
											<td title="正常">正常</td>
										{{/if}}
										{{if speed>15}}
											<td title="较快">较快</td>
										{{/if}}
										{{if speed<0}}
											<td title="暂无速度">暂无速度</td>
										{{/if}}
									{{/if}}
							{{else}}
								<td title="暂无速度">暂无速度</td>
							{{/if}}
						</tr>
				 {{/if}}
			{{/if}}
            <tr>
              <th title="定位时间">定位时间：</th>
              <td title="{{gpsTime}}{{if positionType!=null && positionType !=""}}{{if positionType=="LBS"}}(基站定位){{/if}}{{if positionType=="WIFI"}}(WIFI定位){{/if}}{{if positionType=="GPS"}}(卫星定位){{/if}}{{if positionType=="BEACON"}}(蓝牙定位){{/if}}{{else}}(卫星定位){{/if}}" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;"style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{gpsTime}}
				{{if positionType!=null && positionType !=""}}
					{{if positionType=="LBS"}}(基站定位){{/if}}
					{{if positionType=="WIFI"}}(WIFI定位){{/if}}
					{{if positionType=="GPS"}}(卫星定位){{/if}}
					{{if positionType=="BEACON"}}(蓝牙定位){{/if}}
				{{else}}
					(卫星定位)
				{{/if}}
			  </td>
            </tr>
            <tr>
              <th title="通讯时间">通讯时间：</th>
              <td title="{{hbTime}}">{{hbTime}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
</script>
	<script>
    $("#allMap").css("height",$(window).height());
    
    var allMap = new BMap.Map("allMap",{
		enableMapClick : false
	}); // 创建Map实例
    allMap.centerAndZoom(new BMap.Point(116.404, 39.915), 11); // 初始化地图,设置中心点坐标和地图级别
    allMap.addControl(new BMap.MapTypeControl()); //添加地图类型控件
    allMap.setCurrentCity("北京"); // 设置地图显示的城市 此项是必须设置的
    allMap.enableScrollWheelZoom(true); //开启鼠标滚轮缩放
    allMap.addControl(new BMap.NavigationControl()); //设置导航条 （左上角，添加默认缩放平移控件）

    var data = {"gpsTime":$("#gpsTime").val(),"lng":$("#lng").val(),"deviceName":$("#deviceName").val(),"positionType":$("#posType").val(),"lat":$("#lat").val(),"icon":$("#icon").val(),"status":$("#status").val(),"speed":$("#speed").val(),"imei":$("#imei").val(),"acc":$("#accStatus").val(),"hbTime":$("#hbTime").val()};
    
    </script>
	<script>
	$(function(){
		var point = new BMap.Point(data.lng, data.lat);
		var marker = new BMap.Marker(point);
		
		allMap.addOverlay(marker);
		var content = template('InfoWindowTpl', $.extend({
			language : "zh",
			map_type : "baiduMap"
		}, data));
		
		op = $.extend({
			boxStyle : {
				width : "503px",
				height : "0px"
			},
			offset : new BMap.Size(503, 265),
			// 直接隐藏官方关闭按钮
			closeIconMargin : "2px;display:none;",
			closeIconUrl : " ",
			enableAutoPan : true,
			align : 1
		}, {"status":data.status});
		dialog = new BMapLib.InfoBox(allMap, content, op);
		allMap.addOverlay(dialog);
		dialog.open(marker);
		allMap.setViewport([point]);
	})
    </script>
</body>
</html>