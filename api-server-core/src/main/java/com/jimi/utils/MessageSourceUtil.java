/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.utils;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;

import com.jimi.framework.context.SpringContextHolder;

/**
 * @FileName MessageSourceUtils.java
 * @Description: 获取国际化资源工具类
 *
 * @Date 2017年4月25日 下午7:43:46
 * @author li.shangzhi
 * @version 1.0
 */
public class MessageSourceUtil {

	private static final Logger logger = LoggerFactory.getLogger(MessageSourceUtil.class);

	private MessageSourceUtil() {

	}

	/**
	 * @Title: getMessage
	 * @Description: 指定语言，获取国际化资源
	 * @param key
	 *            国际化Key
	 * @param locale
	 *            语言
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月25日 下午7:47:56
	 */
	public static String getMessage(String key, Locale locale) {
		try {
			MessageSource messageSource = SpringContextHolder.getBean(MessageSource.class);
			return messageSource.getMessage(key, null, locale);
		} catch (NoSuchMessageException e) {
			logger.error("获取国际化资源异常，国际化KEY：{}，语言：{}", key, locale.toString(), e);
			return null;
		}
	}

	/**
	 * @Title: getMessage
	 * @Description: 默认当前语言，获取国际化资源
	 * @param key
	 *            国际化Key
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月25日 下午7:57:03
	 */
	public static String getMessage(String key) {
		try {
			MessageSource messageSource = SpringContextHolder.getBean(MessageSource.class);
			return messageSource.getMessage(key, null, LocaleContextHolder.getLocale());
		} catch (NoSuchMessageException e) {
			logger.error("获取国际化资源异常，国际化KEY：{}，语言：{}", key, LocaleContextHolder.getLocale().toString(), e);
			return null;
		}
	}

	/**
	 * @Title: getMessage
	 * @Description: 指定语言，获取国际化资源
	 * @param key
	 *            国际化Key
	 * @param locale
	 *            语言
	 * @param params
	 *            参数列表
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月25日 下午7:47:56
	 */
	public static String getMessage(String key, Locale locale, String... params) {
		try {
			MessageSource messageSource = SpringContextHolder.getBean(MessageSource.class);
			return messageSource.getMessage(key, params, locale);
		} catch (NoSuchMessageException e) {
			logger.error("获取国际化资源异常，国际化KEY：{}，语言：{}", key, locale.toString(), e);
			return null;
		}
	}

	/**
	 * @Title: getMessage
	 * @Description: 默认当前语言，获取国际化资源
	 * @param key
	 *            国际化Key
	 * @param params
	 *            参数列表
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月25日 下午7:57:03
	 */
	public static String getMessage(String key, String... params) {
		try {
			MessageSource messageSource = SpringContextHolder.getBean(MessageSource.class);
			return messageSource.getMessage(key, params, LocaleContextHolder.getLocale());
		} catch (NoSuchMessageException e) {
			logger.error("获取国际化资源异常，国际化KEY：{}，语言：{}", key, LocaleContextHolder.getLocale().toString(), e);
			return null;
		}
	}
}
