/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月9日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.framework.base.BaseEntity;

/**
 * @FileName ApiAppPushSetEntity.java
 * @Description:
 *
 * @Date 2017年5月9日 下午6:16:41
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiAppPushSetEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 用户账号
	 */
	private String account;
	/**
	 * 用户描述与appinfo一样
	 */
	private String appDesc;
	/**
	 * 推送的url地址
	 */
	private String pushUrl;
	/**
	 * 是否启用，1-启用 0-不启用
	 */
	private Boolean enabled;

	public ApiAppPushSetEntity() {
	}

	public ApiAppPushSetEntity(Integer userId) {
		this.userId = userId;
	}

	public void setUserId(Integer userId) {
		userId = userId == null ? 0 : userId;
		this.userId = userId;
	}

	public Integer getUserId() {
		return this.userId == null ? 0 : this.userId;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAccount() {
		return this.account == null ? null : this.account.trim();
	}

	public void setAppDesc(String appDesc) {
		this.appDesc = appDesc;
	}

	public String getAppDesc() {
		return this.appDesc == null ? null : this.appDesc.trim();
	}

	public void setPushUrl(String pushUrl) {
		this.pushUrl = pushUrl;
	}

	public String getPushUrl() {
		return this.pushUrl == null ? null : this.pushUrl.trim();
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
