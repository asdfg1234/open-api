/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月28日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.model;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.commons.DatetimeUtil;
import com.jimi.framework.base.BaseEntity;

/**
 * @FileName ApiAppAccessingLogEntity.java
 * @Description:
 *
 * @Date 2017年4月28日 下午2:53:38
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiAppAccessingLogEntity extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private String id;
	/**
	 * 访问者IP
	 */
	private String remoteIp;
	/**
	 * 访问者app_key
	 */
	private String appKey;
	/**
	 * 主用户id
	 */
	private Integer userId;
	/**
	 * 主账号
	 */
	private String account;
	/**
	 * 当前访问的账号
	 */
	private String useAccount;
	/**
	 * 环境标识
	 */
	private String routeId;
	/**
	 * 访问的方法
	 */
	private String method;
	/**
	 * url路径
	 */
	private String path;
	/**
	 * 访问结果
	 */
	private Boolean status;
	/**
	 * errorDesc
	 */
	private String errorDesc;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建时间（开始时间，用于搜索）
	 */
	private Date startCreateTime;
	/**
	 * 创建时间（结束时间，用于搜索）
	 */
	private Date endCreateTime;

	public ApiAppAccessingLogEntity() {
	}

	public ApiAppAccessingLogEntity(String id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return this.id == null ? null : this.id.trim();
	}

	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

	public String getRemoteIp() {
		return this.remoteIp == null ? null : this.remoteIp.trim();
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppKey() {
		return this.appKey == null ? null : this.appKey.trim();
	}

	public void setUserId(Integer userId) {
		userId = userId == null ? 0 : userId;
		this.userId = userId;
	}

	public Integer getUserId() {
		return this.userId == null ? 0 : this.userId;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAccount() {
		return this.account == null ? null : this.account.trim();
	}

	public void setUseAccount(String useAccount) {
		this.useAccount = useAccount;
	}

	public String getUseAccount() {
		return this.useAccount == null ? null : this.useAccount.trim();
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public String getRouteId() {
		return this.routeId == null ? null : this.routeId.trim();
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getMethod() {
		return this.method == null ? null : this.method.trim();
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPath() {
		return this.path == null ? null : this.path.trim();
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public String getErrorDesc() {
		return this.errorDesc == null ? null : this.errorDesc.trim();
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setStartCreateTime(Date startCreateTime) {
		this.startCreateTime = startCreateTime;
	}

	public Date getStartCreateTime() {
		return this.startCreateTime;
	}

	public void setEndCreateTime(Date endCreateTime) {
		this.endCreateTime = endCreateTime;
	}

	public Date getEndCreateTime() {
		return this.endCreateTime;
	}

	public String getStringCreateTime() {
		if (this.createTime == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.createTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
