/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.framework.base.BaseEntity;

/**
 * @FileName ApiAppFunsEntity.java
 * @Description:
 *
 * @Date 2017年4月25日 上午9:26:59
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiAppFunsEntity extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	/**
	 * app_key
	 */
	private String appKey;
	/**
	 * 方法ID
	 */
	private String funId;
	/**
	 * 方法描述
	 */
	private String funName;
	/**
	 * 每秒最高访问量
	 */
	private Integer reqPerSecond;
	/**
	 * 每天最高访问量
	 */
	private Integer reqPerDay;

	public ApiAppFunsEntity() {
	}

	public ApiAppFunsEntity(String appKey, String funId) {
		this.appKey = appKey;
		this.funId = funId;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppKey() {
		return this.appKey == null ? null : this.appKey.trim();
	}

	public void setFunId(String funId) {
		this.funId = funId;
	}

	public String getFunId() {
		return this.funId == null ? null : this.funId.trim();
	}

	public void setFunName(String funName) {
		this.funName = funName;
	}

	public String getFunName() {
		return this.funName == null ? null : this.funName.trim();
	}

	public void setReqPerSecond(Integer reqPerSecond) {
		reqPerSecond = reqPerSecond == null ? 0 : reqPerSecond;
		this.reqPerSecond = reqPerSecond;
	}

	public Integer getReqPerSecond() {
		return this.reqPerSecond == null ? 0 : this.reqPerSecond;
	}

	public void setReqPerDay(Integer reqPerDay) {
		reqPerDay = reqPerDay == null ? 0 : reqPerDay;
		this.reqPerDay = reqPerDay;
	}

	public Integer getReqPerDay() {
		return this.reqPerDay == null ? 0 : this.reqPerDay;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}