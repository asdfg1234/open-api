/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.framework.base.BaseEntity;

/**
 * @FileName ApiGatewayRouteInfoEntity.java
 * @Description:
 *
 * @Date 2017年4月25日 上午9:27:13
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiGatewayRouteInfoEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 代理ID
	 */
	private String id;
	/**
	 * 路由路径
	 */
	private String path;
	/**
	 * 服务ID，注册中心使用
	 */
	private String serviceId;
	/**
	 * 完整的url路径
	 */
	private String url;
	/**
	 * 是否重试，service-id下有用
	 */
	private Boolean retryable;
	/**
	 * 是否启用，1-启用 0-不启用
	 */
	private Boolean enabled;
	/**
	 * stripPrefix
	 */
	private Boolean stripPrefix;
	/**
	 * 路由描述
	 */
	private String apiName;

	public ApiGatewayRouteInfoEntity() {
	}

	public ApiGatewayRouteInfoEntity(String id) {
		this.id = id;
	}

	public void setId(String value) {
		this.id = value;
	}

	public String getId() {
		return this.id == null ? null : this.id.trim();
	}

	public void setPath(String value) {
		this.path = value;
	}

	public String getPath() {
		return this.path == null ? null : this.path.trim();
	}

	public void setServiceId(String value) {
		this.serviceId = value;
	}

	public String getServiceId() {
		return this.serviceId == null ? null : this.serviceId.trim();
	}

	public void setUrl(String value) {
		this.url = value;
	}

	public String getUrl() {
		return this.url == null ? null : this.url.trim();
	}

	public void setRetryable(Boolean value) {
		this.retryable = value;
	}

	public Boolean getRetryable() {
		return this.retryable;
	}

	public void setEnabled(Boolean value) {
		this.enabled = value;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setStripPrefix(Boolean value) {
		this.stripPrefix = value;
	}

	public Boolean getStripPrefix() {
		return this.stripPrefix;
	}

	public void setApiName(String value) {
		this.apiName = value;
	}

	public String getApiName() {
		return this.apiName == null ? null : this.apiName.trim();
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}