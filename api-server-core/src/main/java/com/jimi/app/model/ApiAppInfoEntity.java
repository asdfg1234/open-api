/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.app.model;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.commons.DatetimeUtil;
import com.jimi.framework.base.BaseEntity;

/**
 * @FileName ApiAppInfoEntity.java
 * @Description: 
 *
 * @Date 2017年4月25日 上午9:27:07
 * @author li.shangzhi
 * @version 1.0
 */
public class ApiAppInfoEntity extends BaseEntity{
	
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 用户账号
	 */
	private String account;
	/**
	 * 项目标识
	 */
	private String projectCode;
	/**
	 * proejct_code+user_id生成
	 */
	private String appKey;
	/**
	 * app密码随机生成
	 */
	private String appSecret;
	/**
	 * app描述
	 */
	private String appDesc;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建时间（开始时间，用于搜索）
	 */
	private Date startCreateTime;
	/**
	 * 创建时间（结束时间，用于搜索）
	 */
	private Date endCreateTime;
	/**
	 * 最后更新时间
	 */
	private Date lastUdpateTime;
	/**
	 * 最后更新时间（开始时间，用于搜索）
	 */
	private Date startLastUdpateTime;
	/**
	 * 最后更新时间（结束时间，用于搜索）
	 */
	private Date endLastUdpateTime;
	/**
	 * 是否启用，1-启用 0-不启用
	 */
	private Boolean enabled;

	public ApiAppInfoEntity(){
	}

	public ApiAppInfoEntity(
		Integer userId,
		String projectCode
	){
		this.userId = userId;
		this.projectCode = projectCode;
	}
	public void setUserId(Integer userId) {
		userId = userId == null ? 0 : userId;
		this.userId = userId;
	}
	
	public Integer getUserId() {
		return this.userId == null ? 0 : this.userId;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getAccount() {
		return this.account == null ? null : this.account.trim();
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	
	public String getProjectCode() {
		return this.projectCode == null ? null : this.projectCode.trim();
	}
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
	
	public String getAppKey() {
		return this.appKey == null ? null : this.appKey.trim();
	}
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	
	public String getAppSecret() {
		return this.appSecret == null ? null : this.appSecret.trim();
	}
	public void setAppDesc(String appDesc) {
		this.appDesc = appDesc;
	}
	
	public String getAppDesc() {
		return this.appDesc == null ? null : this.appDesc.trim();
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getCreateTime() {
		return this.createTime;
	}
	public void setStartCreateTime(Date startCreateTime) {
		this.startCreateTime = startCreateTime;
	}
	
	public Date getStartCreateTime() {
		return this.startCreateTime;
	}
	
	public void setEndCreateTime(Date endCreateTime) {
		this.endCreateTime = endCreateTime;
	}
	
	public Date getEndCreateTime() {
		return this.endCreateTime;
	}
	
	public String getStringCreateTime() {
		if(this.createTime == null){
		return null;
		}
		return DatetimeUtil.DateToString(this.createTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}
	public void setLastUdpateTime(Date lastUdpateTime) {
		this.lastUdpateTime = lastUdpateTime;
	}
	
	public Date getLastUdpateTime() {
		return this.lastUdpateTime;
	}
	public void setStartLastUdpateTime(Date startLastUdpateTime) {
		this.startLastUdpateTime = startLastUdpateTime;
	}
	
	public Date getStartLastUdpateTime() {
		return this.startLastUdpateTime;
	}
	
	public void setEndLastUdpateTime(Date endLastUdpateTime) {
		this.endLastUdpateTime = endLastUdpateTime;
	}
	
	public Date getEndLastUdpateTime() {
		return this.endLastUdpateTime;
	}
	
	public String getStringLastUdpateTime() {
		if(this.lastUdpateTime == null){
		return null;
		}
		return DatetimeUtil.DateToString(this.lastUdpateTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setEnabled(Boolean value) {
		this.enabled = value;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}