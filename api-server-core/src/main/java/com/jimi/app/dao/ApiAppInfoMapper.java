/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.app.dao;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import com.jimi.app.model.ApiAppInfoEntity;

/**
 * @FileName ApiAppInfoMapper.java
 * @Description: 
 *
 * @Date 2017年4月25日 上午9:27:52
 * @author li.shangzhi
 * @version 1.0
 */
public interface ApiAppInfoMapper {

	int findPageCount(ApiAppInfoEntity apiAppInfoEntity);

	List<ApiAppInfoEntity> findPage(ApiAppInfoEntity apiAppInfoEntity,RowBounds rowBounds);
	
	List<ApiAppInfoEntity> findList(ApiAppInfoEntity apiAppInfoEntity);
	
	int insert(ApiAppInfoEntity apiAppInfoEntity);
	
	int update(ApiAppInfoEntity apiAppInfoEntity);
	
	int remove(String id);
	
	ApiAppInfoEntity getById(String id);
	
	ApiAppInfoEntity getByAppKey(String appKey);
}
