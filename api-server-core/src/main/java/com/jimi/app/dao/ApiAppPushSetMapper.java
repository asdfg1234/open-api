/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月9日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.app.dao;

import java.util.List;

import com.jimi.app.model.ApiAppPushSetEntity;

/**
 * @FileName ApiAppPushSetMapper.java
 * @Description: 
 *
 * @Date 2017年5月9日 下午6:14:23
 * @author li.shangzhi
 * @version 1.0
 */
public interface ApiAppPushSetMapper {

	List<ApiAppPushSetEntity> findList(ApiAppPushSetEntity apiAppPushSetEntity);
	
	int insert(ApiAppPushSetEntity apiAppPushSetEntity);
	
	int update(ApiAppPushSetEntity apiAppPushSetEntity);
	
	int remove(String id);
	
	ApiAppPushSetEntity getById(String id);
}
