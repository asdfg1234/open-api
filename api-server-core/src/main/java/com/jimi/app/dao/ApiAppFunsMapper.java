/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.app.dao;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import com.jimi.app.model.ApiAppFunsEntity;

/**
 * @FileName ApiAppFunsMapper.java
 * @Description: 
 *
 * @Date 2017年4月25日 上午9:27:35
 * @author li.shangzhi
 * @version 1.0
 */
public interface ApiAppFunsMapper {

	int findPageCount(ApiAppFunsEntity apiAppFunsEntity);

	List<ApiAppFunsEntity> findPage(ApiAppFunsEntity apiAppFunsEntity,RowBounds rowBounds);
	
	List<ApiAppFunsEntity> findList(ApiAppFunsEntity apiAppFunsEntity);
	
	int insert(ApiAppFunsEntity apiAppFunsEntity);
	
	int update(ApiAppFunsEntity apiAppFunsEntity);
	
	int remove(String id);
	
	ApiAppFunsEntity getById(String id);
}
