/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月28日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.dao;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import com.jimi.app.model.ApiAppAccessingLogEntity;

/**
 * @FileName ApiAppAccessingLogMapper.java
 * @Description:
 *
 * @Date 2017年4月28日 下午2:55:14
 * @author li.shangzhi
 * @version 1.0
 */
public interface ApiAppAccessingLogMapper {

	int findPageCount(ApiAppAccessingLogEntity apiAppAccessingLogEntity);

	List<ApiAppAccessingLogEntity> findPage(ApiAppAccessingLogEntity apiAppAccessingLogEntity, RowBounds rowBounds);

	List<ApiAppAccessingLogEntity> findList(ApiAppAccessingLogEntity apiAppAccessingLogEntity);

	int insert(ApiAppAccessingLogEntity apiAppAccessingLogEntity);

	int update(ApiAppAccessingLogEntity apiAppAccessingLogEntity);

	int remove(String id);

	ApiAppAccessingLogEntity getById(String id);
}
