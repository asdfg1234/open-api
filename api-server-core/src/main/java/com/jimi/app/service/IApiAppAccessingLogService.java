/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月28日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import com.jimi.app.model.ApiAppAccessingLogEntity;

/**
 * @FileName IApiAppAccessingLogService.java
 * @Description:
 *
 * @Date 2017年4月28日 下午2:53:13
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiAppAccessingLogService {

	/**
	 * 获取总条数
	 */
	public int findPageCount(ApiAppAccessingLogEntity apiAppAccessingLogEntity);

	/**
	 * 获取分页数据
	 */
	public List<ApiAppAccessingLogEntity> findPage(ApiAppAccessingLogEntity apiAppAccessingLogEntity, RowBounds rowBounds);

	/**
	 * 获取集合
	 */
	public List<ApiAppAccessingLogEntity> findList(ApiAppAccessingLogEntity apiAppAccessingLogEntity);

	/**
	 * 保存单条记录
	 */
	public String insert(ApiAppAccessingLogEntity apiAppAccessingLogEntity);

	/**
	 * 更新记录
	 */
	public int update(ApiAppAccessingLogEntity apiAppAccessingLogEntity);

	/**
	 * 通过id删除记录
	 */
	public int remove(String id);

	/**
	 * 通过id查询数据
	 */
	public ApiAppAccessingLogEntity getById(String id);
}
