/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jimi.app.dao.ApiOpenFunctionsMapper;
import com.jimi.app.model.ApiOpenFunctionsEntity;
import com.jimi.app.service.IApiOpenFunctionsService;
import com.jimi.framework.annotation.cache.CacheCable;
import com.jimi.framework.base.BaseService;
import com.jimi.framework.utils.UUIDGenerator;

/**
 * @FileName ApiOpenFunctionsServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月25日 上午9:24:33
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiOpenFunctionsServiceImpl extends BaseService implements IApiOpenFunctionsService {

	@Resource
	private ApiOpenFunctionsMapper apiOpenFunctionsMapper;

	@Override
	public int findPageCount(ApiOpenFunctionsEntity apiOpenFunctionsEntity) {
		return apiOpenFunctionsMapper.findPageCount(apiOpenFunctionsEntity);
	}

	@Override
	public List<ApiOpenFunctionsEntity> findPage(ApiOpenFunctionsEntity apiOpenFunctionsEntity, RowBounds rowBounds) {
		return apiOpenFunctionsMapper.findPage(apiOpenFunctionsEntity, rowBounds);
	}

	@Override
	public List<ApiOpenFunctionsEntity> findList(ApiOpenFunctionsEntity apiOpenFunctionsEntity) {
		return apiOpenFunctionsMapper.findList(apiOpenFunctionsEntity);
	}

	@Override
	@Transactional
	public String insert(ApiOpenFunctionsEntity apiOpenFunctionsEntity) {
		String strId = UUIDGenerator.get32LowCaseUUID();
		apiOpenFunctionsEntity.setId(strId);
		apiOpenFunctionsMapper.insert(apiOpenFunctionsEntity);
		return strId;
	}

	@Override
	@Transactional
	public int update(ApiOpenFunctionsEntity apiOpenFunctionsEntity) {
		return apiOpenFunctionsMapper.update(apiOpenFunctionsEntity);
	}

	@Override
	@Transactional
	public int remove(String id) {
		return apiOpenFunctionsMapper.remove(id);
	}

	@Override
	public ApiOpenFunctionsEntity getById(String id) {
		return apiOpenFunctionsMapper.getById(id);
	}

	@Override
	@CacheCable(key = "'Method_'+#method", value = { "jimiOpenApi." }, localExpiration = 900, expiration = 1800)
	public ApiOpenFunctionsEntity getByMethod(String method) {
		return apiOpenFunctionsMapper.getByMethod(method);
	}

}
