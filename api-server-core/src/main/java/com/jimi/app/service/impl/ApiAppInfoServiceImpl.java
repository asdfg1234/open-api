/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jimi.app.dao.ApiAppInfoMapper;
import com.jimi.app.model.ApiAppInfoEntity;
import com.jimi.app.service.IApiAppInfoService;
import com.jimi.framework.annotation.cache.CacheCable;
import com.jimi.framework.base.BaseService;

/**
 * @FileName ApiAppInfoServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月25日 上午9:24:15
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiAppInfoServiceImpl extends BaseService implements IApiAppInfoService {

	@Resource
	private ApiAppInfoMapper apiAppInfoMapper;

	@Override
	public int findPageCount(ApiAppInfoEntity apiAppInfoEntity) {
		return apiAppInfoMapper.findPageCount(apiAppInfoEntity);
	}

	@Override
	public List<ApiAppInfoEntity> findPage(ApiAppInfoEntity apiAppInfoEntity, RowBounds rowBounds) {
		return apiAppInfoMapper.findPage(apiAppInfoEntity, rowBounds);
	}

	@Override
	public List<ApiAppInfoEntity> findList(ApiAppInfoEntity apiAppInfoEntity) {
		return apiAppInfoMapper.findList(apiAppInfoEntity);
	}

	@Override
	@Transactional
	public int insert(ApiAppInfoEntity apiAppInfoEntity) {
		apiAppInfoEntity.setCreateTime(new Date());
		return apiAppInfoMapper.insert(apiAppInfoEntity);
	}

	@Override
	@Transactional
	public int update(ApiAppInfoEntity apiAppInfoEntity) {
		return apiAppInfoMapper.update(apiAppInfoEntity);
	}

	@Override
	@Transactional
	public int remove(String id) {
		return apiAppInfoMapper.remove(id);
	}

	@Override
	public ApiAppInfoEntity getById(String id) {
		return apiAppInfoMapper.getById(id);
	}

	@Override
	@CacheCable(key = "'AppKey_'+#appKey", value = { "jimiOpenApi." }, localExpiration = 900, expiration = 1800)
	public ApiAppInfoEntity getByAppKey(String appKey) {
		return apiAppInfoMapper.getByAppKey(appKey);
	}

}
