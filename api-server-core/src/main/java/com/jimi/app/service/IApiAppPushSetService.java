/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月9日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service;

import java.util.List;

import com.jimi.app.model.ApiAppPushSetEntity;

/**
 * @FileName IApiAppPushSetService.java
 * @Description:
 *
 * @Date 2017年5月9日 下午6:07:32
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiAppPushSetService {

	/**
	 * 获取集合
	 */
	public List<ApiAppPushSetEntity> findList(ApiAppPushSetEntity apiAppPushSetEntity);

	/**
	 * 保存单条记录
	 */
	public int insert(ApiAppPushSetEntity apiAppPushSetEntity);

	/**
	 * 更新记录
	 */
	public int update(ApiAppPushSetEntity apiAppPushSetEntity);

	/**
	 * 通过id删除记录
	 */
	public int remove(String id);

	/**
	 * 通过id查询数据
	 */
	public ApiAppPushSetEntity getById(String id);

}
