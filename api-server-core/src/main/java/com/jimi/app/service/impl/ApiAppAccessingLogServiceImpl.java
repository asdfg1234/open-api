/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月28日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jimi.app.dao.ApiAppAccessingLogMapper;
import com.jimi.app.model.ApiAppAccessingLogEntity;
import com.jimi.app.service.IApiAppAccessingLogService;
import com.jimi.framework.base.BaseService;
import com.jimi.framework.utils.UUIDGenerator;

/**
 * @FileName ApiAppAccessingLogServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月28日 下午2:54:44
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiAppAccessingLogServiceImpl extends BaseService implements IApiAppAccessingLogService {

	@Resource
	private ApiAppAccessingLogMapper apiAppAccessingLogMapper;

	@Override
	public int findPageCount(ApiAppAccessingLogEntity apiAppAccessingLogEntity) {
		return apiAppAccessingLogMapper.findPageCount(apiAppAccessingLogEntity);
	}

	@Override
	public List<ApiAppAccessingLogEntity> findPage(ApiAppAccessingLogEntity apiAppAccessingLogEntity, RowBounds rowBounds) {
		return apiAppAccessingLogMapper.findPage(apiAppAccessingLogEntity, rowBounds);
	}

	@Override
	public List<ApiAppAccessingLogEntity> findList(ApiAppAccessingLogEntity apiAppAccessingLogEntity) {
		return apiAppAccessingLogMapper.findList(apiAppAccessingLogEntity);
	}

	@Override
	@Transactional
	public String insert(ApiAppAccessingLogEntity apiAppAccessingLogEntity) {
		String strId = UUIDGenerator.get32LowCaseUUID();
		apiAppAccessingLogEntity.setId(strId);
		apiAppAccessingLogEntity.setCreateTime(new Date());
		apiAppAccessingLogMapper.insert(apiAppAccessingLogEntity);
		return strId;
	}

	@Override
	@Transactional
	public int update(ApiAppAccessingLogEntity apiAppAccessingLogEntity) {
		return apiAppAccessingLogMapper.update(apiAppAccessingLogEntity);
	}

	@Override
	@Transactional
	public int remove(String id) {
		return apiAppAccessingLogMapper.remove(id);
	}

	@Override
	public ApiAppAccessingLogEntity getById(String id) {
		return apiAppAccessingLogMapper.getById(id);
	}

}
