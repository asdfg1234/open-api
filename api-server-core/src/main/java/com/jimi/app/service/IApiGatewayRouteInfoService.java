/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import com.jimi.app.model.ApiGatewayRouteInfoEntity;

/**
 * @FileName IApiGatewayRouteInfoService.java
 * @Description:
 *
 * @Date 2017年4月25日 上午9:23:44
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiGatewayRouteInfoService {

	/**
	 * 获取总条数
	 */
	public int findPageCount(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity);

	/**
	 * 获取分页数据
	 */
	public List<ApiGatewayRouteInfoEntity> findPage(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity, RowBounds rowBounds);

	/**
	 * 获取集合
	 */
	public List<ApiGatewayRouteInfoEntity> findList(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity);

	/**
	 * 保存单条记录
	 */
	public int insert(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity);

	/**
	 * 更新记录
	 */
	public int update(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity);

	/**
	 * 通过id删除记录
	 */
	public int remove(String id);

	/**
	 * 通过id查询数据
	 */
	public ApiGatewayRouteInfoEntity getById(String id);
}
