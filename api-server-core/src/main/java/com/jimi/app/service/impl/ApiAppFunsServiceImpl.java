/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jimi.app.dao.ApiAppFunsMapper;
import com.jimi.app.model.ApiAppFunsEntity;
import com.jimi.app.service.IApiAppFunsService;
import com.jimi.framework.annotation.cache.CacheCable;
import com.jimi.framework.base.BaseService;

/**
 * @FileName ApiAppFunsServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月25日 上午9:24:08
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiAppFunsServiceImpl extends BaseService implements IApiAppFunsService {

	@Resource
	private ApiAppFunsMapper apiAppFunsMapper;

	@Override
	public int findPageCount(ApiAppFunsEntity apiAppFunsEntity) {
		return apiAppFunsMapper.findPageCount(apiAppFunsEntity);
	}

	@Override
	public List<ApiAppFunsEntity> findPage(ApiAppFunsEntity apiAppFunsEntity, RowBounds rowBounds) {
		return apiAppFunsMapper.findPage(apiAppFunsEntity, rowBounds);
	}

	@Override
	public List<ApiAppFunsEntity> findList(ApiAppFunsEntity apiAppFunsEntity) {
		return apiAppFunsMapper.findList(apiAppFunsEntity);
	}

	@Override
	@Transactional
	public int insert(ApiAppFunsEntity apiAppFunsEntity) {
		return apiAppFunsMapper.insert(apiAppFunsEntity);
	}

	@Override
	@Transactional
	public int update(ApiAppFunsEntity apiAppFunsEntity) {
		return apiAppFunsMapper.update(apiAppFunsEntity);
	}

	@Override
	@Transactional
	public int remove(String id) {
		return apiAppFunsMapper.remove(id);
	}

	@Override
	public ApiAppFunsEntity getById(String id) {
		return apiAppFunsMapper.getById(id);
	}

	@Override
	@CacheCable(key = "'AppFun_'+#appKey+'_'+#funId", value = { "jimiOpenApi." }, localExpiration = 900, expiration = 1800)
	public ApiAppFunsEntity getByAppFun(String appKey, String funId) {
		ApiAppFunsEntity apiAppFunsEntity = new ApiAppFunsEntity();
		apiAppFunsEntity.setAppKey(appKey);
		apiAppFunsEntity.setFunId(funId);
		List<ApiAppFunsEntity> dataList = apiAppFunsMapper.findList(apiAppFunsEntity);
		if (null != dataList && dataList.size() > 0) {
			return dataList.get(0);
		}
		return null;
	}

}
