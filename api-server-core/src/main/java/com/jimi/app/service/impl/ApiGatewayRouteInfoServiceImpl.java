/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jimi.app.dao.ApiGatewayRouteInfoMapper;
import com.jimi.app.model.ApiGatewayRouteInfoEntity;
import com.jimi.app.service.IApiGatewayRouteInfoService;
import com.jimi.framework.base.BaseService;

/**
 * @FileName ApiGatewayRouteInfoServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月25日 上午9:24:23
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiGatewayRouteInfoServiceImpl extends BaseService implements IApiGatewayRouteInfoService {

	@Resource
	private ApiGatewayRouteInfoMapper apiGatewayRouteInfoMapper;

	@Override
	public int findPageCount(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity) {
		return apiGatewayRouteInfoMapper.findPageCount(apiGatewayRouteInfoEntity);
	}

	@Override
	public List<ApiGatewayRouteInfoEntity> findPage(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity, RowBounds rowBounds) {
		return apiGatewayRouteInfoMapper.findPage(apiGatewayRouteInfoEntity, rowBounds);
	}

	@Override
	public List<ApiGatewayRouteInfoEntity> findList(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity) {
		return apiGatewayRouteInfoMapper.findList(apiGatewayRouteInfoEntity);
	}

	@Override
	@Transactional
	public int insert(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity) {
		return apiGatewayRouteInfoMapper.insert(apiGatewayRouteInfoEntity);
	}

	@Override
	@Transactional
	public int update(ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity) {
		return apiGatewayRouteInfoMapper.update(apiGatewayRouteInfoEntity);
	}

	@Override
	@Transactional
	public int remove(String id) {
		return apiGatewayRouteInfoMapper.remove(id);
	}

	@Override
	public ApiGatewayRouteInfoEntity getById(String id) {
		return apiGatewayRouteInfoMapper.getById(id);
	}

}
