/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import com.jimi.app.model.ApiOpenFunctionsEntity;

/**
 * @FileName IApiOpenFunctionsService.java
 * @Description:
 *
 * @Date 2017年4月25日 上午9:23:54
 * @author li.shangzhi
 * @version 1.0
 */
public interface IApiOpenFunctionsService {

	/**
	 * 获取总条数
	 */
	public int findPageCount(ApiOpenFunctionsEntity apiOpenFunctionsEntity);

	/**
	 * 获取分页数据
	 */
	public List<ApiOpenFunctionsEntity> findPage(ApiOpenFunctionsEntity apiOpenFunctionsEntity, RowBounds rowBounds);

	/**
	 * 获取集合
	 */
	public List<ApiOpenFunctionsEntity> findList(ApiOpenFunctionsEntity apiOpenFunctionsEntity);

	/**
	 * 保存单条记录
	 */
	public String insert(ApiOpenFunctionsEntity apiOpenFunctionsEntity);

	/**
	 * 更新记录
	 */
	public int update(ApiOpenFunctionsEntity apiOpenFunctionsEntity);

	/**
	 * 通过id删除记录
	 */
	public int remove(String id);

	/**
	 * 通过id查询数据
	 */
	public ApiOpenFunctionsEntity getById(String id);
	
	/**
	 * 通过method查询数据
	 */
	public ApiOpenFunctionsEntity getByMethod(String method);
}
