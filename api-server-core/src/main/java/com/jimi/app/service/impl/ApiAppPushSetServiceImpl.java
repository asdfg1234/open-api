/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月9日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jimi.app.dao.ApiAppPushSetMapper;
import com.jimi.app.model.ApiAppPushSetEntity;
import com.jimi.app.service.IApiAppPushSetService;
import com.jimi.framework.annotation.cache.CacheCable;
import com.jimi.framework.base.BaseService;

/**
 * @FileName ApiAppPushSetServiceImpl.java
 * @Description:
 *
 * @Date 2017年5月9日 下午6:15:44
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiAppPushSetServiceImpl extends BaseService implements IApiAppPushSetService {

	@Resource
	private ApiAppPushSetMapper apiAppPushSetMapper;

	@Override
	public List<ApiAppPushSetEntity> findList(ApiAppPushSetEntity apiAppPushSetEntity) {
		return apiAppPushSetMapper.findList(apiAppPushSetEntity);
	}

	@Override
	@Transactional
	public int insert(ApiAppPushSetEntity apiAppPushSetEntity) {
		return apiAppPushSetMapper.insert(apiAppPushSetEntity);
	}

	@Override
	@Transactional
	public int update(ApiAppPushSetEntity apiAppPushSetEntity) {
		return apiAppPushSetMapper.update(apiAppPushSetEntity);
	}

	@Override
	@Transactional
	public int remove(String id) {
		return apiAppPushSetMapper.remove(id);
	}

	@Override
	@CacheCable(key = "'AlarmPushUserId_'+#id", value = { "jimiOpenApi." }, localExpiration = 1800, expiration = 3600)
	public ApiAppPushSetEntity getById(String id) {
		return apiAppPushSetMapper.getById(id);
	}

}
