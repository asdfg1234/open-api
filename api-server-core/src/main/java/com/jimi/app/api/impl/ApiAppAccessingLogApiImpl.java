/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月28日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import net.sf.oval.constraint.NotBlank;

import com.jimi.app.api.IApiAppAccessingLogApi;
import com.jimi.app.dto.input.ApiAppAccessingLogInputDto;
import com.jimi.app.model.ApiAppAccessingLogEntity;
import com.jimi.app.service.IApiAppAccessingLogService;
import com.jimi.framework.annotation.log.LoggerProfile;
import com.jimi.framework.bean.BeanUtil;

/**
 * @FileName ApiAppAccessingLogApiImpl.java
 * @Description:
 *
 * @Date 2017年4月28日 下午2:51:51
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiAppAccessingLogApiImpl implements IApiAppAccessingLogApi {

	@Resource
	IApiAppAccessingLogService apiAppAccessingLogService;

	@Override
	@LoggerProfile(methodNote = "IApiAppAccessingLogApi.removeById")
	public int removeById(String id) {
		return apiAppAccessingLogService.remove(id);
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppAccessingLogApi.insert")
	public String insert(ApiAppAccessingLogInputDto apiAppAccessingLogDto) {
		return apiAppAccessingLogService.insert(this.dtoToEntity(apiAppAccessingLogDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppAccessingLogApi.update")
	public int update(@NotBlank.List(value = { @NotBlank(target = "id") }) ApiAppAccessingLogInputDto apiAppAccessingLogDto) {
		return apiAppAccessingLogService.update(this.dtoToEntity(apiAppAccessingLogDto));
	}

	private ApiAppAccessingLogEntity dtoToEntity(Object obj) {
		return (ApiAppAccessingLogEntity) BeanUtil.convertBean(obj, ApiAppAccessingLogEntity.class);
	}

}
