/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api.impl;

import java.util.List;

import javax.annotation.Resource;

import net.sf.oval.constraint.NotBlank;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.jimi.app.api.IApiAppInfoApi;
import com.jimi.app.dto.input.ApiAppInfoInputDto;
import com.jimi.app.dto.output.ApiAppInfoOutputDto;
import com.jimi.app.model.ApiAppInfoEntity;
import com.jimi.app.service.IApiAppInfoService;
import com.jimi.dto.base.PageModel;
import com.jimi.framework.annotation.log.LoggerProfile;
import com.jimi.framework.bean.BeanUtil;
import com.jimi.framework.utils.RowBoundUtils;

/**
 * @FileName ApiAppInfoApiImpl.java
 * @Description:
 *
 * @Date 2017年4月25日 下午2:48:27
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiAppInfoApiImpl implements IApiAppInfoApi {

	@Resource
	IApiAppInfoService apiAppInfoService;

	@Override
	@LoggerProfile(methodNote = "IApiAppInfoApi.removeById")
	public int removeById(String id) {
		return apiAppInfoService.remove(id);
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppInfoApi.insert")
	public int insert(@NotBlank.List(value = { @NotBlank(target = "account"), @NotBlank(target = "projectCode"),
			@NotBlank(target = "appKey"), @NotBlank(target = "appSecret"), @NotBlank(target = "appDesc") }) ApiAppInfoInputDto apiAppInfoDto) {
		return apiAppInfoService.insert(this.dtoToEntity(apiAppInfoDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppInfoApi.findPage")
	public PageModel<ApiAppInfoOutputDto> findPage(ApiAppInfoInputDto pageInputDto, PageModel pageModel) {
		ApiAppInfoEntity apiAppInfoEntity = (ApiAppInfoEntity) BeanUtil.convertBean(pageInputDto, ApiAppInfoEntity.class);
		RowBounds rowBounds = RowBoundUtils.rowBounds(pageModel);
		apiAppInfoEntity.setOrderSort(pageModel.getOrderSort());
		apiAppInfoEntity.setOrder(pageModel.getOrder().getValue());

		int totalCount = apiAppInfoService.findPageCount(apiAppInfoEntity);
		List<ApiAppInfoEntity> lstApiAppInfos = apiAppInfoService.findPage(apiAppInfoEntity, rowBounds);

		return new PageModel<ApiAppInfoOutputDto>(pageModel.getPage(), pageModel.getLimit(), totalCount,
				(List<ApiAppInfoOutputDto>) BeanUtil.convertBeanList(lstApiAppInfos, ApiAppInfoOutputDto.class));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppInfoApi.findList")
	public List<ApiAppInfoOutputDto> findList(ApiAppInfoInputDto inputDto) {
		ApiAppInfoEntity apiAppInfoEntity = (ApiAppInfoEntity) BeanUtil.convertBean(inputDto, ApiAppInfoEntity.class);
		List<ApiAppInfoEntity> lstApiAppInfos = apiAppInfoService.findList(apiAppInfoEntity);
		return (List<ApiAppInfoOutputDto>) BeanUtil.convertBeanList(lstApiAppInfos, ApiAppInfoOutputDto.class);
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppInfoApi.update")
	public int update(@NotBlank.List(value = { @NotBlank(target = "id") }) ApiAppInfoInputDto apiAppInfoDto) {
		return apiAppInfoService.update(this.dtoToEntity(apiAppInfoDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppInfoApi.getById")
	public ApiAppInfoOutputDto getById(@NotBlank String id) {
		ApiAppInfoEntity apiAppInfoEntity = apiAppInfoService.getById(id);
		return this.entityToDto(apiAppInfoEntity);
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppInfoApi.getByAppKey")
	public ApiAppInfoOutputDto getByAppKey(String appKey) {
		ApiAppInfoEntity apiAppInfoEntity = apiAppInfoService.getByAppKey(appKey);
		return this.entityToDto(apiAppInfoEntity);
	}

	private ApiAppInfoEntity dtoToEntity(Object obj) {
		return (ApiAppInfoEntity) BeanUtil.convertBean(obj, ApiAppInfoEntity.class);
	}

	private ApiAppInfoOutputDto entityToDto(Object obj) {
		return (ApiAppInfoOutputDto) BeanUtil.convertBean(obj, ApiAppInfoOutputDto.class);
	}

}
