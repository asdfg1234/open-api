/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api.impl;

import java.util.List;

import javax.annotation.Resource;

import net.sf.oval.constraint.NotBlank;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.jimi.app.api.IApiAppFunsApi;
import com.jimi.app.dto.input.ApiAppFunsInputDto;
import com.jimi.app.dto.output.ApiAppFunsOutputDto;
import com.jimi.app.model.ApiAppFunsEntity;
import com.jimi.app.service.IApiAppFunsService;
import com.jimi.dto.base.PageModel;
import com.jimi.framework.annotation.log.LoggerProfile;
import com.jimi.framework.bean.BeanUtil;
import com.jimi.framework.utils.RowBoundUtils;

/**
 * @FileName ApiAppFunsApiImpl.java
 * @Description:
 *
 * @Date 2017年4月25日 下午2:48:17
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiAppFunsApiImpl implements IApiAppFunsApi {

	@Resource
	IApiAppFunsService apiAppFunsService;

	@Override
	@LoggerProfile(methodNote = "IApiAppFunsApi.removeById")
	public int removeById(String id) {
		return apiAppFunsService.remove(id);
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppFunsApi.findPage")
	public PageModel<ApiAppFunsOutputDto> findPage(ApiAppFunsInputDto pageInputDto, PageModel pageModel) {
		ApiAppFunsEntity apiAppFunsEntity = (ApiAppFunsEntity) BeanUtil.convertBean(pageInputDto, ApiAppFunsEntity.class);
		RowBounds rowBounds = RowBoundUtils.rowBounds(pageModel);
		apiAppFunsEntity.setOrderSort(pageModel.getOrderSort());
		apiAppFunsEntity.setOrder(pageModel.getOrder().getValue());

		int totalCount = apiAppFunsService.findPageCount(apiAppFunsEntity);
		List<ApiAppFunsEntity> lstApiAppFunss = apiAppFunsService.findPage(apiAppFunsEntity, rowBounds);

		return new PageModel<ApiAppFunsOutputDto>(pageModel.getPage(), pageModel.getLimit(), totalCount,
				(List<ApiAppFunsOutputDto>) BeanUtil.convertBeanList(lstApiAppFunss, ApiAppFunsOutputDto.class));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppFunsApi.findList")
	public List<ApiAppFunsOutputDto> findList(ApiAppFunsInputDto inputDto) {
		ApiAppFunsEntity apiAppFunsEntity = (ApiAppFunsEntity) BeanUtil.convertBean(inputDto, ApiAppFunsEntity.class);
		List<ApiAppFunsEntity> lstApiAppFunss = apiAppFunsService.findList(apiAppFunsEntity);
		return (List<ApiAppFunsOutputDto>) BeanUtil.convertBeanList(lstApiAppFunss, ApiAppFunsOutputDto.class);
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppFunsApi.insert")
	public int insert(
			@NotBlank.List(value = { @NotBlank(target = "appKey"), @NotBlank(target = "funId"), @NotBlank(target = "funName") }) ApiAppFunsInputDto apiAppFunsDto) {
		return apiAppFunsService.insert(this.dtoToEntity(apiAppFunsDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppFunsApi.update")
	public int update(@NotBlank.List(value = { @NotBlank(target = "id") }) ApiAppFunsInputDto apiAppFunsDto) {
		return apiAppFunsService.update(this.dtoToEntity(apiAppFunsDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppFunsApi.getById")
	public ApiAppFunsOutputDto getById(@NotBlank String id) {
		ApiAppFunsEntity apiAppFunsEntity = apiAppFunsService.getById(id);
		return this.entityToDto(apiAppFunsEntity);
	}
	
	@Override
	@LoggerProfile(methodNote = "IApiAppFunsApi.getByAppFun")
	public ApiAppFunsOutputDto getByAppFun(String appKey, String funId) {
		ApiAppFunsEntity apiAppFunsEntity = apiAppFunsService.getByAppFun(appKey, funId);
		return this.entityToDto(apiAppFunsEntity);
	}

	private ApiAppFunsEntity dtoToEntity(Object obj) {
		return (ApiAppFunsEntity) BeanUtil.convertBean(obj, ApiAppFunsEntity.class);
	}

	private ApiAppFunsOutputDto entityToDto(Object obj) {
		return (ApiAppFunsOutputDto) BeanUtil.convertBean(obj, ApiAppFunsOutputDto.class);
	}

}
