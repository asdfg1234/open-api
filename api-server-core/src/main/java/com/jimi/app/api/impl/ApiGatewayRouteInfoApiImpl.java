/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api.impl;

import java.util.List;

import javax.annotation.Resource;

import net.sf.oval.constraint.NotBlank;

import org.springframework.stereotype.Service;

import com.jimi.app.api.IApiGatewayRouteInfoApi;
import com.jimi.app.dto.input.ApiGatewayRouteInfoInputDto;
import com.jimi.app.dto.output.ApiGatewayRouteInfoOutputDto;
import com.jimi.app.model.ApiGatewayRouteInfoEntity;
import com.jimi.app.service.IApiGatewayRouteInfoService;
import com.jimi.framework.annotation.log.LoggerProfile;
import com.jimi.framework.bean.BeanUtil;

/**
 * @FileName ApiGatewayRouteInfoApiImpl.java
 * @Description:
 *
 * @Date 2017年4月25日 下午2:48:37
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiGatewayRouteInfoApiImpl implements IApiGatewayRouteInfoApi {

	@Resource
	IApiGatewayRouteInfoService apiGatewayRouteInfoService;

	@Override
	@LoggerProfile(methodNote = "IApiGatewayRouteInfoApi.removeById")
	public int removeById(String id) {
		return apiGatewayRouteInfoService.remove(id);
	}

	@Override
	@LoggerProfile(methodNote = "IApiGatewayRouteInfoApi.findList")
	public List<ApiGatewayRouteInfoOutputDto> findList(ApiGatewayRouteInfoInputDto inputDto) {
		ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity = (ApiGatewayRouteInfoEntity) BeanUtil.convertBean(inputDto,
				ApiGatewayRouteInfoEntity.class);
		List<ApiGatewayRouteInfoEntity> lstApiGatewayRouteInfos = apiGatewayRouteInfoService.findList(apiGatewayRouteInfoEntity);
		return (List<ApiGatewayRouteInfoOutputDto>) BeanUtil.convertBeanList(lstApiGatewayRouteInfos, ApiGatewayRouteInfoOutputDto.class);
	}

	@Override
	@LoggerProfile(methodNote = "IApiGatewayRouteInfoApi.insert")
	public int insert(@NotBlank.List(value = { @NotBlank(target = "id"), @NotBlank(target = "path"), @NotBlank(target = "url"),
			@NotBlank(target = "retryable"), @NotBlank(target = "enabled"), @NotBlank(target = "stripPrefix"),
			@NotBlank(target = "apiName") }) ApiGatewayRouteInfoInputDto apiGatewayRouteInfoDto) {
		return apiGatewayRouteInfoService.insert(this.dtoToEntity(apiGatewayRouteInfoDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiGatewayRouteInfoApi.update")
	public int update(@NotBlank.List(value = { @NotBlank(target = "id") }) ApiGatewayRouteInfoInputDto apiGatewayRouteInfoDto) {
		return apiGatewayRouteInfoService.update(this.dtoToEntity(apiGatewayRouteInfoDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiGatewayRouteInfoApi.getById")
	public ApiGatewayRouteInfoOutputDto getById(@NotBlank String id) {
		ApiGatewayRouteInfoEntity apiGatewayRouteInfoEntity = apiGatewayRouteInfoService.getById(id);
		return this.entityToDto(apiGatewayRouteInfoEntity);
	}

	private ApiGatewayRouteInfoEntity dtoToEntity(Object obj) {
		return (ApiGatewayRouteInfoEntity) BeanUtil.convertBean(obj, ApiGatewayRouteInfoEntity.class);
	}

	private ApiGatewayRouteInfoOutputDto entityToDto(Object obj) {
		return (ApiGatewayRouteInfoOutputDto) BeanUtil.convertBean(obj, ApiGatewayRouteInfoOutputDto.class);
	}

}
