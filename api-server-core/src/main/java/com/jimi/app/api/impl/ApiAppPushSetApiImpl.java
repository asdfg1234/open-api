/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年5月9日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api.impl;

import java.util.List;

import javax.annotation.Resource;

import net.sf.oval.constraint.NotBlank;

import org.springframework.stereotype.Service;

import com.jimi.app.api.IApiAppPushSetApi;
import com.jimi.app.dto.input.ApiAppPushSetInputDto;
import com.jimi.app.dto.output.ApiAppPushSetOutputDto;
import com.jimi.app.model.ApiAppPushSetEntity;
import com.jimi.app.service.IApiAppPushSetService;
import com.jimi.framework.annotation.log.LoggerProfile;
import com.jimi.framework.bean.BeanUtil;

/**
 * @FileName ApiAppPushSetApiImpl.java
 * @Description:
 *
 * @Date 2017年5月9日 下午6:06:48
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiAppPushSetApiImpl implements IApiAppPushSetApi {

	@Resource
	IApiAppPushSetService apiAppPushSetService;
	
	@Override
	@LoggerProfile(methodNote = "IApiAppInfoApi.findList")
	public List<ApiAppPushSetOutputDto> findList(ApiAppPushSetInputDto inputDto) {
		ApiAppPushSetEntity apiAppPushSetEntity = (ApiAppPushSetEntity) BeanUtil.convertBean(inputDto, ApiAppPushSetEntity.class);
		List<ApiAppPushSetEntity> lstApiAppPushSets = apiAppPushSetService.findList(apiAppPushSetEntity);
		return (List<ApiAppPushSetOutputDto>) BeanUtil.convertBeanList(lstApiAppPushSets, ApiAppPushSetOutputDto.class);
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppPushSetApi.removeById")
	public int removeById(String id) {
		return apiAppPushSetService.remove(id);
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppPushSetApi.insert")
	public int insert(@NotBlank.List(value = { @NotBlank(target = "account"), @NotBlank(target = "appDesc"),
			@NotBlank(target = "pushUrl") }) ApiAppPushSetInputDto apiAppPushSetDto) {
		return apiAppPushSetService.insert(this.dtoToEntity(apiAppPushSetDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppPushSetApi.update")
	public int update(@NotBlank.List(value = { @NotBlank(target = "id") }) ApiAppPushSetInputDto apiAppPushSetDto) {
		return apiAppPushSetService.update(this.dtoToEntity(apiAppPushSetDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiAppPushSetApi.getById")
	public ApiAppPushSetOutputDto getById(@NotBlank String id) {
		ApiAppPushSetEntity apiAppPushSetEntity = apiAppPushSetService.getById(id);
		return this.entityToDto(apiAppPushSetEntity);
	}

	private ApiAppPushSetEntity dtoToEntity(Object obj) {
		return (ApiAppPushSetEntity) BeanUtil.convertBean(obj, ApiAppPushSetEntity.class);
	}

	private ApiAppPushSetOutputDto entityToDto(Object obj) {
		return (ApiAppPushSetOutputDto) BeanUtil.convertBean(obj, ApiAppPushSetOutputDto.class);
	}

}
