/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.app.api.impl;

import java.util.List;

import javax.annotation.Resource;

import net.sf.oval.constraint.NotBlank;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import com.jimi.app.api.IApiOpenFunctionsApi;
import com.jimi.app.dto.input.ApiOpenFunctionsInputDto;
import com.jimi.app.dto.output.ApiOpenFunctionsOutputDto;
import com.jimi.app.model.ApiOpenFunctionsEntity;
import com.jimi.app.service.IApiOpenFunctionsService;
import com.jimi.dto.base.PageModel;
import com.jimi.framework.annotation.log.LoggerProfile;
import com.jimi.framework.bean.BeanUtil;
import com.jimi.framework.utils.RowBoundUtils;

/**
 * @FileName ApiOpenFunctionsApiImpl.java
 * @Description:
 *
 * @Date 2017年4月25日 下午2:48:46
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class ApiOpenFunctionsApiImpl implements IApiOpenFunctionsApi {

	@Resource
	IApiOpenFunctionsService apiOpenFunctionsService;

	@Override
	@LoggerProfile(methodNote = "IApiOpenFunctionsApi.removeById")
	public int removeById(String id) {
		return apiOpenFunctionsService.remove(id);
	}

	@Override
	@LoggerProfile(methodNote = "IApiOpenFunctionsApi.insert")
	public String insert(@NotBlank.List(value = { @NotBlank(target = "routeId"), @NotBlank(target = "method"), @NotBlank(target = "url"),
			@NotBlank(target = "methodDesc") }) ApiOpenFunctionsInputDto apiOpenFunctionsDto) {
		return apiOpenFunctionsService.insert(this.dtoToEntity(apiOpenFunctionsDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiOpenFunctionsApi.findPage")
	public PageModel<ApiOpenFunctionsOutputDto> findPage(ApiOpenFunctionsInputDto pageInputDto, PageModel pageModel) {
		ApiOpenFunctionsEntity apiOpenFunctionsEntity = (ApiOpenFunctionsEntity) BeanUtil.convertBean(pageInputDto,
				ApiOpenFunctionsEntity.class);
		RowBounds rowBounds = RowBoundUtils.rowBounds(pageModel);
		apiOpenFunctionsEntity.setOrderSort(pageModel.getOrderSort());
		apiOpenFunctionsEntity.setOrder(pageModel.getOrder().getValue());

		int totalCount = apiOpenFunctionsService.findPageCount(apiOpenFunctionsEntity);
		List<ApiOpenFunctionsEntity> lstApiOpenFunctionss = apiOpenFunctionsService.findPage(apiOpenFunctionsEntity, rowBounds);

		return new PageModel<ApiOpenFunctionsOutputDto>(pageModel.getPage(), pageModel.getLimit(), totalCount,
				(List<ApiOpenFunctionsOutputDto>) BeanUtil.convertBeanList(lstApiOpenFunctionss, ApiOpenFunctionsOutputDto.class));
	}

	@Override
	@LoggerProfile(methodNote = "IApiOpenFunctionsApi.findList")
	public List<ApiOpenFunctionsOutputDto> findList(ApiOpenFunctionsInputDto inputDto) {
		ApiOpenFunctionsEntity apiOpenFunctionsEntity = (ApiOpenFunctionsEntity) BeanUtil.convertBean(inputDto,
				ApiOpenFunctionsEntity.class);
		List<ApiOpenFunctionsEntity> lstApiOpenFunctionss = apiOpenFunctionsService.findList(apiOpenFunctionsEntity);
		return (List<ApiOpenFunctionsOutputDto>) BeanUtil.convertBeanList(lstApiOpenFunctionss, ApiOpenFunctionsOutputDto.class);
	}

	@Override
	@LoggerProfile(methodNote = "IApiOpenFunctionsApi.update")
	public int update(@NotBlank.List(value = { @NotBlank(target = "id") }) ApiOpenFunctionsInputDto apiOpenFunctionsDto) {
		return apiOpenFunctionsService.update(this.dtoToEntity(apiOpenFunctionsDto));
	}

	@Override
	@LoggerProfile(methodNote = "IApiOpenFunctionsApi.getById")
	public ApiOpenFunctionsOutputDto getById(@NotBlank String id) {
		ApiOpenFunctionsEntity apiOpenFunctionsEntity = apiOpenFunctionsService.getById(id);
		return this.entityToDto(apiOpenFunctionsEntity);
	}
	
	@Override
	@LoggerProfile(methodNote = "IApiOpenFunctionsApi.getByMethod")
	public ApiOpenFunctionsOutputDto getByMethod(String method) {
		ApiOpenFunctionsEntity apiOpenFunctionsEntity = apiOpenFunctionsService.getByMethod(method);
		return this.entityToDto(apiOpenFunctionsEntity);
	}

	private ApiOpenFunctionsEntity dtoToEntity(Object obj) {
		return (ApiOpenFunctionsEntity) BeanUtil.convertBean(obj, ApiOpenFunctionsEntity.class);
	}

	private ApiOpenFunctionsOutputDto entityToDto(Object obj) {
		return (ApiOpenFunctionsOutputDto) BeanUtil.convertBean(obj, ApiOpenFunctionsOutputDto.class);
	}

}
