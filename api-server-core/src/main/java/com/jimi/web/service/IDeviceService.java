/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.service;

import java.util.List;

import com.jimi.web.model.Device;
import com.jimi.web.model.DeviceMcType;

/**
 * @FileName IDeviceService.java
 * @Description: 
 *
 * @Date 2017年4月13日 下午1:58:30
 * @author li.shangzhi
 * @version 1.0
 */
public interface IDeviceService {

	List<Device> search(Device device);
	
	/**
	 * @Title: getDeviceByUserId 
	 * @Description: 查询用户Id下所有设备
	 * @param userId
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年7月19日 下午4:43:37
	 */
	List<Device> getDeviceByUserId(String userId);
	
	/**
	 * @Title: getOne 
	 * @Description: 根据imei号查询设备信息
	 * @param imei
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月14日 上午10:49:50
	 */
	Device getOne(String imei);
	
	/**
	 * @Title: getDevice 
	 * @Description: 查询多个imei号的设备信息
	 * @param imeis 多个imei号，用逗号分隔
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月14日 上午10:48:54
	 */
	List<Device> getDevice(String imeis);
	
	Integer updateEquipment(Device device);
	
	Integer updateEquipmentDetail(Device device);
	
	/**
	 * @Title: checkDeviceRange 
	 * @Description: 检查imei是否是userId下的
	 * @param userId
	 * @param imei
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月18日 下午7:24:32
	 */
	Device checkDeviceRange(String userId, String imei);
	
	/**
	 * @Title: getDeviceDictByTypy 
	 * @Description: 根据设备型号得到字典信息
	 * @param mcType
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月18日 下午7:25:47
	 */
	DeviceMcType getDeviceDictByTypy(String mcType);
	
	/**
	 * @Title: updateBind 
	 * @Description: 修改t_dev_device_info表信息
	 * @param imei
	 * @param flag '0.未绑定 1.绑定',
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年4月20日 上午11:12:24
	 */
	Integer updateBind(String imei, String flag);
}
