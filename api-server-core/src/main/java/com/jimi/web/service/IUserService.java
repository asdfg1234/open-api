/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.service;

import java.util.List;

import com.jimi.web.model.CoreUser;
import com.jimi.web.model.UserInfo;

/**
 * @FileName ITrackerDbService.java
 * @Description:
 *
 * @Date 2017年4月12日 下午4:23:57
 * @author li.shangzhi
 * @version 1.0
 */
public interface IUserService {

	/**
	 * @Title: getUserInfo
	 * @Description:获取指定账号信息，查询的是core_user表
	 * @param account
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月13日 下午8:06:12
	 */
	CoreUser getUserInfo(String account);

	/**
	 * @Title: getAllChildsUser
	 * @Description: 获取指定账号下所有子账号信息
	 * @param account
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月13日 下午8:05:38
	 */
	List<CoreUser> getAllChildsUser(String account);

	/**
	 * @Title: checkUserRange
	 * @Description: 检查账号是否是userid子账号
	 * @param userId
	 * @param account
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月17日 下午5:08:33
	 */
	CoreUser checkUserRange(String userId, String account);

	/**
	 * @Title: getDefaultUserGroupId
	 * @Description: 查询默认的用户分组ID
	 * @param userId
	 * @return
	 * @author li.shangzhi
	 * @date 2017年5月19日 下午11:05:51
	 */
	String getDefaultUserGroupId(String userId);

	/**
	 * @Title: getUserInfo
	 * @Description: 根据用户ID查询用户信息，查询的是user_info表
	 * @param userId
	 * @return
	 * @author li.shangzhi
	 * @date 2017年6月12日 下午4:28:13
	 */
	UserInfo getUserInfoByUserId(String userId);

}
