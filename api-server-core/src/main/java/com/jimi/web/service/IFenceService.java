/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月19日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.service;

import java.util.List;

import com.jimi.web.model.ElectricFence;

/**
 * @FileName IFenceService.java
 * @Description: 
 *
 * @Date 2017年4月19日 下午1:43:13
 * @author li.shangzhi
 * @version 1.0
 */
public interface IFenceService {

	List<ElectricFence> search(ElectricFence electricFence);
	
	ElectricFence getElectricFenceById(String id);
	
	List<ElectricFence> getElectricFenceByImei(String imei);
	
	Integer insert(ElectricFence electricFence);
	
	Integer update(ElectricFence electricFence);
	
	Integer delete(String... ids);
	
	ElectricFence getElectronicFenceByNo(String imei, String instructNo);
}
