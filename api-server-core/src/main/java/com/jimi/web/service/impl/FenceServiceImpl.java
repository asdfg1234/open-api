/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月19日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jimi.framework.base.BaseService;
import com.jimi.web.dao.ElectricFenceMapper;
import com.jimi.web.model.ElectricFence;
import com.jimi.web.service.IFenceService;

/**
 * @FileName FenceServiceImpl.java
 * @Description: 
 *
 * @Date 2017年4月19日 下午2:46:23
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class FenceServiceImpl extends BaseService implements IFenceService {

	@Resource
	private ElectricFenceMapper electricFenceMapper;
	
	@Override
	public List<ElectricFence> search(ElectricFence electricFence) {
		return electricFenceMapper.search(electricFence);
	}

	@Override
	public ElectricFence getElectricFenceById(String id) {
		return electricFenceMapper.getElectricFenceById(id);
	}

	@Override
	public List<ElectricFence> getElectricFenceByImei(String imei) {
		return electricFenceMapper.getElectricFenceByImei(imei);
	}

	@Override
	public Integer insert(ElectricFence electricFence) {
		return electricFenceMapper.insert(electricFence);
	}

	@Override
	public Integer update(ElectricFence electricFence) {
		return electricFenceMapper.update(electricFence);
	}

	@Override
	public Integer delete(String... ids) {
		return electricFenceMapper.delete(ids);
	}

	@Override
	public ElectricFence getElectronicFenceByNo(String imei, String instructNo) {
		return electricFenceMapper.getElectronicFenceByNo(imei, instructNo);
	}

}
