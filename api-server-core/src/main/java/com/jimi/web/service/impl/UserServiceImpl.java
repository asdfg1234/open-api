/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.jimi.framework.base.BaseService;
import com.jimi.web.dao.CoreUserMapper;
import com.jimi.web.dao.UserInfoMapper;
import com.jimi.web.model.CoreUser;
import com.jimi.web.model.UserInfo;
import com.jimi.web.service.IUserService;

/**
 * @FileName TrackerServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月12日 下午4:29:02
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class UserServiceImpl extends BaseService implements IUserService {
	
	@Resource
	private CoreUserMapper coreUserMapper;
	
	@Resource
	private UserInfoMapper userInfoMapper;
	
	@Resource
	private RedisTemplate<String, String> redisTemplate;

	@Override
	public CoreUser getUserInfo(String account) {
		if (StringUtils.isBlank(account)) {
			return null;
		}
		CoreUser coreUserEntity = new CoreUser();
		coreUserEntity.setAccount(account);
		coreUserEntity.setDisplayFlag(1); // 启用
		List<CoreUser> resultList = coreUserMapper.findList(coreUserEntity);
		if (null != resultList && resultList.size() > 0) {
			return resultList.get(0);
		}
		return null;
	}

	@Override
	public List<CoreUser> getAllChildsUser(String account) {
		if (StringUtils.isBlank(account)) {
			return null;
		}
		return coreUserMapper.getAllChildsUser(account);
	}

	@Override
	public CoreUser checkUserRange(String userId, String account) {
		return coreUserMapper.checkUserRange(userId, account);
	}

	@Override
	public String getDefaultUserGroupId(String userId) {
		return coreUserMapper.getDefaultUserGroupId(userId);
	}

	@Override
	public UserInfo getUserInfoByUserId(String userId) {
		UserInfo userInfo = new UserInfo();
		userInfo.setUserId(userId);
		return userInfoMapper.getOneUserInfo(userInfo);
	}

}
