/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.jimi.framework.annotation.cache.CacheCable;
import com.jimi.framework.base.BaseService;
import com.jimi.web.dao.DeviceMapper;
import com.jimi.web.model.Device;
import com.jimi.web.model.DeviceMcType;
import com.jimi.web.service.IDeviceService;

/**
 * @FileName DeviceServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月13日 下午2:05:28
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class DeviceServiceImpl extends BaseService implements IDeviceService {

	@Resource
	private DeviceMapper deviceMapper;

	@Override
	public List<Device> search(Device device) {
		return deviceMapper.search(device);
	}
	
	@Override
	@CacheCable(key = "'getDeviceByUserId_'+#userId", value = { "jimiOpenApi." }, localExpiration = 900, expiration = 1800)
	public List<Device> getDeviceByUserId(String userId) {
		Device d = new Device();
		d.setUserId(userId);
		return deviceMapper.search(d);
	}

	@Override
	public Device getOne(String imei) {
		return deviceMapper.getOne(imei);
	}

	@Override
	@CacheCable(key = "'getDevice_'+#imeis", value = { "jimiOpenApi." }, localExpiration = 900, expiration = 1800)
	public List<Device> getDevice(String imeis) {
		String[] imeisArray = imeis.split(",");
		Set<String> imeisSet = new HashSet<>();
		for (String imei : imeisArray) {
			imeisSet.add(imei);
		}
		return deviceMapper.getDeviceList(imeisSet);
	}
	
	@Override
	public Integer updateEquipment(Device device) {
		return deviceMapper.updateEquipment(device);
	}
	
	@Override
	public Integer updateEquipmentDetail(Device device) {
		return deviceMapper.updateEquipmentDetail(device);
	}

	@Override
	public Device checkDeviceRange(String userId, String imei) {
		return deviceMapper.checkDeviceRange(userId, imei);
	}

	@Override
	public DeviceMcType getDeviceDictByTypy(String mcType) {
		DeviceMcType deviceMcType = deviceMapper.getDeviceDictByType(mcType);
		if (StringUtils.isBlank(deviceMcType.getExt1())) {
			deviceMcType.setExt1("0");// 设置默认不支持离线指令
		}
		if (StringUtils.isBlank(deviceMcType.getExt5())) {
			deviceMcType.setExt1("1");// 设置默认为单围栏
		}
		if (StringUtils.isNotBlank(deviceMcType.getExt5()) && "N".equals(deviceMcType.getExt5())
				&& StringUtils.isBlank(deviceMcType.getExt6())) {
			deviceMcType.setExt6("5");// 如果是多围栏，设置默认支持围栏数为5个
		}
		return deviceMcType;
	}

	@Override
	public Integer updateBind(String imei, String flag) {
		return deviceMapper.updateBind(imei, flag);
	}
}
