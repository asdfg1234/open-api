/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jimi.framework.base.BaseService;
import com.jimi.web.dao.InstructionsMapper;
import com.jimi.web.model.CommandLogsEntity;
import com.jimi.web.model.InstructionDetailsEntity;
import com.jimi.web.service.IInstructionService;

/**
 * @FileName InstructionServiceImpl.java
 * @Description:
 *
 * @Date 2017年7月25日 上午11:50:56
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class InstructionServiceImpl extends BaseService implements IInstructionService {

	@Resource
	private InstructionsMapper instructionsMapper;
	
	@Override
	public List<InstructionDetailsEntity> getInstructionByMcType(String mcType) {
		return instructionsMapper.getInstructionByMcType(mcType);
	}
	
	@Override
	public InstructionDetailsEntity getInstruction(String mcType, String instId, String instTemplate) {
		return instructionsMapper.getInstruction(mcType, instId, instTemplate);
	}

	@Override
	public int insertCommandLog(CommandLogsEntity commandLogsEntity) {
		commandLogsEntity.setSendTime(new Date());
		commandLogsEntity.setCreationDate(new Date());
		commandLogsEntity.setUpdationDate(new Date());
		return instructionsMapper.insertCommandLog(commandLogsEntity);
	}

	@Override
	public List<CommandLogsEntity> getCommandLogsByImei(String imei) {
		return instructionsMapper.getCommandLogsByImei(imei);
	}

	@Override
	public List<CommandLogsEntity> getNotExecutOfflineCommandLogs(String imei) {
		return instructionsMapper.getNotExecutOfflineCommandLogs(imei);
	}

	@Override
	public int deleteOffLineCmdByImei(String... imeis) {
		return instructionsMapper.deleteOffLineCmdByImei(imeis);
	}

}
