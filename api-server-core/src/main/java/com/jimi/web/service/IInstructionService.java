/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.service;

import java.util.List;

import com.jimi.web.model.CommandLogsEntity;
import com.jimi.web.model.InstructionDetailsEntity;

/**
 * @FileName IInstructionService.java
 * @Description:
 *
 * @Date 2017年7月25日 上午9:41:51
 * @author li.shangzhi
 * @version 1.0
 */
public interface IInstructionService {

	/**
	 * @Title: getInstructionByMcType
	 * @Description: 根据设备机型查询支持的指令列表
	 * @param mcType
	 *            机型
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月24日 下午6:18:17
	 */
	List<InstructionDetailsEntity> getInstructionByMcType(String mcType);

	/**
	 * @Title: getInstruction
	 * @Description: 指定机型、指令ID、指令模板查询指令数据
	 * @param mcType
	 *            机型
	 * @param instId
	 *            指令ID
	 * @param instTemplate
	 *            指令模板
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月25日 下午3:31:33
	 */
	InstructionDetailsEntity getInstruction(String mcType, String instId, String instTemplate);

	/**
	 * @Title: insertCommandLog
	 * @Description: 插入指令操作日志
	 * @param commandLogsEntity
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月25日 上午11:43:10
	 */
	int insertCommandLog(CommandLogsEntity commandLogsEntity);

	/**
	 * @Title: getCommandLogsByImei
	 * @Description: 根据imei查询下发的指令结果
	 * @param imei
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月25日 上午11:44:33
	 */
	List<CommandLogsEntity> getCommandLogsByImei(String imei);
	
	/**
	 * @Title: getNotExecutOfflineCommandLogs
	 * @Description: 查询未执行的离线指令记录
	 * @param imei
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月25日 下午7:31:19
	 */
	List<CommandLogsEntity> getNotExecutOfflineCommandLogs(String imei);
	
	/**
	 * @Title: deleteOffLineCmdByImei 
	 * @Description: 删除设备离线指令
	 * @param imeis
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年7月26日 下午2:39:10
	 */
	int deleteOffLineCmdByImei(String... imeis);
}
