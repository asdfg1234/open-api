package com.jimi.web.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.jimi.web.model.ElectricFence;

/**
 * 电子围栏Mapper
 * @author jeek
 *
 */
public interface ElectricFenceMapper {
	
	List<ElectricFence> search(ElectricFence electricFence);
	
	ElectricFence getElectricFenceById(String id);
	
	List<ElectricFence> getElectricFenceByImei(String imei);
	
	Integer insert(ElectricFence electricFence);
	
	Integer update(ElectricFence electricFence);
	
	int delete(String... ids);
	
	ElectricFence getElectronicFenceByNo(@Param("imei") String imei, @Param("instructNo") String instructNo);
}
