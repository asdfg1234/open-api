package com.jimi.web.dao;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.jimi.web.model.Device;
import com.jimi.web.model.DeviceMcType;

public interface DeviceMapper {
	
	List<Device> search(Device device);
	
	Device getOne(String imei);
	
	List<Device> getDeviceList(@Param("imeis")Set<String> imeis);
	
	Integer updateEquipment(Device device);
	
	Integer updateEquipmentDetail(Device device);
	
	Device checkDeviceRange(@Param("userId")String userId, @Param("imei")String imei);
	
	DeviceMcType getDeviceDictByType(String mcType);
	
	Integer updateBind(@Param("imei")String imei, @Param("flag")String flag);
}
