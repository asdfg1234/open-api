/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.jimi.web.model.CoreUser;

/**
 * @FileName CoreUserMapper.java
 * @Description: 
 *
 * @Date 2017年4月13日 上午9:56:39
 * @author li.shangzhi
 * @version 1.0
 */
public interface CoreUserMapper {

	int findPageCount(CoreUser coreUserEntity);

	List<CoreUser> findPage(CoreUser coreUserEntity,RowBounds rowBounds);
	
	List<CoreUser> findList(CoreUser coreUserEntity);
	
	int insert(CoreUser coreUserEntity);
	
	int update(CoreUser coreUserEntity);
	
	int remove(String id);
	
	CoreUser getById(String id);
	
	List<CoreUser> getAllChildsUser(String userId);
	
	CoreUser checkUserRange(@Param("userId")String userId, @Param("account")String account);

	String getDefaultUserGroupId(String userId);
}
