package com.jimi.web.dao;

import com.jimi.web.model.UserInfo;

public interface UserInfoMapper{
	
	public int existsAccount(String account);

	public UserInfo getOneUserInfo(UserInfo userInfo);

	public void updateRelation(UserInfo upUserRelation);

	public void insert(UserInfo userInfo);
	
}
