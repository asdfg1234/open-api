/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月18日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.model;

import java.io.Serializable;

/**
 * @FileName DeviceMcType.java
 * @Description: 
 *
 * @Date 2017年4月18日 下午7:21:42
 * @author li.shangzhi
 * @version 1.0
 */
public class DeviceMcType implements Serializable{

	private static final long serialVersionUID = 1L;

	private String id;
	
	private String key;
	
	private String title;
	
	private String ext1;//是否支持离线指令(0:否  1:是)
	
	private String ext2;//机型使用范围
	
	private String ext3;//是否终生(0 终身  1 不终身)
	
	private String ext4;//是否支持ACC(0:否   1:是)
	
	private String isSupportFence;//是否支持电子围栏（0：否；1：是）
	
	private String ext5;//机型所支持的围栏指令类型(1:单围栏 N:多围栏)
	
	private String ext6;//机型所支持的围栏个数(只适用于围栏指令类型是多围栏是)

	private String isSupportFMMode;//是否支持调频模式(0:否  1:是)
	
    private String pathQueryRange;//轨迹查询范围(上限) 单位:天
    
    private String trackPlayType;//轨迹播放的类型 0：轨迹播放，1：打点播放，2：轨迹兼打点播放

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}

	public String getExt3() {
		return ext3;
	}

	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}

	public String getExt4() {
		return ext4;
	}

	public void setExt4(String ext4) {
		this.ext4 = ext4;
	}

	public String getIsSupportFence() {
		return isSupportFence;
	}

	public void setIsSupportFence(String isSupportFence) {
		this.isSupportFence = isSupportFence;
	}

	public String getExt5() {
		return ext5;
	}

	public void setExt5(String ext5) {
		this.ext5 = ext5;
	}

	public String getExt6() {
		return ext6;
	}

	public void setExt6(String ext6) {
		this.ext6 = ext6;
	}

	public String getIsSupportFMMode() {
		return isSupportFMMode;
	}

	public void setIsSupportFMMode(String isSupportFMMode) {
		this.isSupportFMMode = isSupportFMMode;
	}

	public String getPathQueryRange() {
		return pathQueryRange;
	}

	public void setPathQueryRange(String pathQueryRange) {
		this.pathQueryRange = pathQueryRange;
	}

	public String getTrackPlayType() {
		return trackPlayType;
	}

	public void setTrackPlayType(String trackPlayType) {
		this.trackPlayType = trackPlayType;
	}

}
