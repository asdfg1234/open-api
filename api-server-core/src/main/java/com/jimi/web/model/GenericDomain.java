/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.model;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang3.StringUtils;

/**
 * @FileName GenericDomain.java
 * @Description:
 *
 * @Date 2017年4月13日 下午1:59:53
 * @author li.shangzhi
 * @version 1.0
 */
public class GenericDomain implements Serializable {
	private static final long serialVersionUID = 1L;
	private transient int _currentPage = 1; // Mysql分页每几页,从1开始
	private transient int _pageSize = 10; // Mysql分页大小,

	protected String id;

	/**
	 * 创建人
	 */
	protected String createdBy;

	/**
	 * 创建日期
	 */
	protected Timestamp creationDate;

	/**
	 * 修改人
	 */
	protected String updatedBy;

	/**
	 * 修改日期
	 */
	protected Timestamp updationDate;

	/**
	 * 是否可用
	 */
	protected Long enabledFlag = 1L;

	/**
	 * 历史表ID
	 */
	protected String hisId;

	/**
	 * 历史创建人
	 */
	protected String hisCreatedBy;

	/**
	 * 历史创建日期
	 */
	protected Timestamp hisCreationDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int get_currentPage() {
		return _currentPage;
	}

	public void set_currentPage(int _currentPage) {
		this._currentPage = _currentPage;
	}

	public int get_pageSize() {
		return _pageSize;
	}

	public void set_pageSize(int _pageSize) {
		this._pageSize = _pageSize;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	// @Transient
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationDate() {
		return updationDate;
	}

	public void setUpdationDate(Timestamp updationDate) {
		this.updationDate = updationDate;
	}

	public Long getEnabledFlag() {
		return enabledFlag;
	}

	public void setEnabledFlag(Long enabledFlag) {
		this.enabledFlag = enabledFlag;
	}

	public String getHisId() {
		return hisId;
	}

	public void setHisId(String hisId) {
		this.hisId = hisId;
	}

	public String getHisCreatedBy() {
		return hisCreatedBy;
	}

	public void setHisCreatedBy(String hisCreatedBy) {
		this.hisCreatedBy = hisCreatedBy;
	}

	public Timestamp getHisCreationDate() {
		return hisCreationDate;
	}

	public void setHisCreationDate(Timestamp hisCreationDate) {
		this.hisCreationDate = hisCreationDate;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();

		String tempId = StringUtils.strip(getHisId(), getId());
		String tempIdName = StringUtils.isBlank(getHisId()) ? "id" : "hisId";

		buffer.append(getClass().getName()).append("@").append(Integer.toHexString(hashCode())).append(" [");
		buffer.append(tempIdName + "='").append(tempId).append("'");
		buffer.append("]");

		return buffer.toString();
	}

	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}

		if (this == o) {
			return true;
		}

		if (!(o instanceof GenericDomain)) {
			return false;
		}

		GenericDomain other = (GenericDomain) o;
		if (StringUtils.isBlank(getHisId())) {
			if (getId() != null && other.getId() != null) {
				return getId().equals(other.getId());
			} else {
				return false;
			}
		} else {
			return getHisId().equals(other.getHisId());
		}
	}

	public int hashCode() {
		int result = 17;
		String tempId = StringUtils.strip(getHisId(), getId());
		if (tempId != null && tempId.length() > 0) {
			int tHashCode = tempId.hashCode();
			result = 37 * result + (int) (tHashCode ^ (tHashCode >>> 32));
		}

		return result;
	}
}