/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月17日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.web.model.lbs;

/**
 * @FileName LbsPointResult.java
 * @Description: 
 *
 * @Date 2017年4月17日 下午1:51:34
 * @author li.shangzhi
 * @version 1.0
 */
public class LbsPointResult {

	private String code;

	private LbsPoint data;

	private String msg;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LbsPoint getData() {
		return data;
	}

	public void setData(LbsPoint data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
