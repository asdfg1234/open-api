package com.jimi.web.model;

import java.io.Serializable;

/**
 * 电子围栏实体类，对应表electric_fence
 * 
 * @author jeek
 *
 */
public class ElectricFence implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String instructNo; // 指令序号
	
	private String imei;
	
	private String name; // 围栏名
	
	private String alarmType; // 进出报警类型
	
	private String radius;// 半径
	
	private String reportMode;// 上报方式
	
	private String mapType;// 地图类型
	
	private String zoomLevel;// 缩放级别
	
	private String coordinate1;// 坐标1
	
	private String coordinate2;// 坐标2
	
	private String gpsCoordinate;// GPS坐标
	
	private int instructCode;// 指令返回code

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInstructNo() {
		return instructNo;
	}

	public void setInstructNo(String instructNo) {
		this.instructNo = instructNo;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(String alarmType) {
		this.alarmType = alarmType;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getReportMode() {
		return reportMode;
	}

	public void setReportMode(String reportMode) {
		this.reportMode = reportMode;
	}

	public String getMapType() {
		return mapType;
	}

	public void setMapType(String mapType) {
		this.mapType = mapType;
	}

	public String getZoomLevel() {
		return zoomLevel;
	}

	public void setZoomLevel(String zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

	public String getCoordinate1() {
		return coordinate1;
	}

	public void setCoordinate1(String coordinate1) {
		this.coordinate1 = coordinate1;
	}

	public String getCoordinate2() {
		return coordinate2;
	}

	public void setCoordinate2(String coordinate2) {
		this.coordinate2 = coordinate2;
	}

	public String getGpsCoordinate() {
		return gpsCoordinate;
	}

	public void setGpsCoordinate(String gpsCoordinate) {
		this.gpsCoordinate = gpsCoordinate;
	}

	public int getInstructCode() {
		return instructCode;
	}

	public void setInstructCode(int instructCode) {
		this.instructCode = instructCode;
	}

}
