package com.jimi.web.model;

import java.util.Map;

public class DeviceCount extends GenericDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String relationId; // 用户关系ID。 唯一标识

	private String userId; // 用户ID

	private int repertory = 0; // (包含下级用户)进货

	private int stock = 0; // (单个用户)库存

	private int active = 0; // (包含下级用户)激活

	private int noactive = 0; // (包含下级用户)未激活

	private int onLineNum = 0; // 在线

	private int noOnlineNum = 0; // 离线

	private int userNum = 0;

	private int perActive = 0; // 单个用户(激活)

	private int perNoActive = 0;// 单个用户(未激活)

	private Map<String, Integer> onLineMap; // 在线设备

	private String numStr;// 包含下级的设备统计（格式：总数，激活，未激活）

	private int aboutToExpire;// 即将到期

	private int expired;// 已到期

	public DeviceCount() {

	}

	public DeviceCount(String userId) {
		this.userId = userId;
	}

	public int getPerActive() {
		return perActive;
	}

	public void setPerActive(int perActive) {
		this.perActive = perActive;
	}

	public int getPerNoActive() {
		return perNoActive;
	}

	public void setPerNoActive(int perNoActive) {
		this.perNoActive = perNoActive;
	}

	public String getRelationId() {
		return relationId;
	}

	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getRepertory() {
		return repertory;
	}

	public void setRepertory(int repertory) {
		this.repertory = repertory;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getNoactive() {
		return noactive;
	}

	public void setNoactive(int noactive) {
		this.noactive = noactive;
	}

	public int getOnLineNum() {
		return onLineNum;
	}

	public void setOnLineNum(int onLineNum) {
		this.onLineNum = onLineNum;
	}

	public int getNoOnlineNum() {
		return noOnlineNum;
	}

	public void setNoOnlineNum(int noOnlineNum) {
		this.noOnlineNum = noOnlineNum;
	}

	public Map<String, Integer> getOnLineMap() {
		return onLineMap;
	}

	public void setOnLineMap(Map<String, Integer> onLineMap) {
		this.onLineMap = onLineMap;
	}

	public String getNumStr() {
		return numStr;
	}

	public void setNumStr(String numStr) {
		this.numStr = numStr;
	}

	public int getUserNum() {
		return userNum;
	}

	public void setUserNum(int userNum) {
		this.userNum = userNum;
	}

	public int getExpired() {
		return expired;
	}

	public void setExpired(int expired) {
		this.expired = expired;
	}

	public int getAboutToExpire() {
		return aboutToExpire;
	}

	public void setAboutToExpire(int aboutToExpire) {
		this.aboutToExpire = aboutToExpire;
	}

}
