package com.jimi.web.model;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class DeviceLoginInfo{

	/**
	 * 
	 */
	private String imei;    	//设备IMEI
	
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date firstTime;	//第一次登陆
	
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date lastTime;	//最后一次登录
	
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date offlineTime;	//离线时间
	
	private String type; //登录&离开
	
	private String cumulant;	//累积时间
	
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date createAt=new Date();	// 创建时间
	
	private double lat; // 纬度

	private double lng; // 经度

	private String addr; // 地点
	
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public Date getFirstTime() {
		return firstTime;
	}
	public void setFirstTime(Date firstTime) {
		this.firstTime = firstTime;
	}
	public Date getLastTime() {
		return lastTime;
	}
	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}
	public Date getOfflineTime() {
		return offlineTime;
	}
	public void setOfflineTime(Date offlineTime) {
		this.offlineTime = offlineTime;
	}
	public String getCumulant() {
		return cumulant;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setCumulant(String cumulant) {
		this.cumulant = cumulant;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
}
