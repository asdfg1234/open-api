package com.jimi.web.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 实体类
 * 数据库表名称：asset_device
 * </pre>
 */
public class Device extends GenericDomain {
    private static final long serialVersionUID = 1L;

    /**
     * 字段名称：设备IMEI号（15位）
     * 
     * 数据库字段信息:imei VARCHAR(15)
     */
    private String imei;

    /**
     * 字段名称：所属用户ID
     * 
     * 数据库字段信息:userId VARCHAR(32)
     */
    private String userId;

    /**
     * 字段名称：用户分组ID
     * 
     * 数据库字段信息:orgId VARCHAR(32)
     */
    private String orgId;
    
    /**
     * 字段名称：设备名称
     * 
     * 数据库字段信息:deviceName VARCHAR(100)
     */
    private String deviceName;

    /**
     * 字段名称：设备类型
     * 
     * 数据库字段信息:mcType VARCHAR(32)
     */
    private String mcType;
    
    /**
     * 字段名称：机型使用范围
     * 
     * 数据库字段信息:mcType VARCHAR(32)
     */
    private String mcTypeUseScope;

    /**
     * 字段名称：SIM卡号
     * 
     * 数据库字段信息:sim VARCHAR(50)
     */
    private String sim;

    /**
     * 字段名称：轨迹存储时间（月）
     * 
     * 数据库字段信息:storager INT(10)
     */
    private Integer storager;

    /**
     * 字段名称：到期时间
     * 
     * 数据库字段信息:expiration DATETIME(19)
     */
    private Timestamp expiration;
    
    /**
     * 字段名称：激活时间
     * 
     * 数据库字段信息:activationTime DATETIME(19)
     */
    private Timestamp activationTime;

    /**
     * 字段名称：备注
     * 
     * 数据库字段信息:reMark VARCHAR(255)
     */
    private String reMark;

    /**
     * 字段名称：箱号
     * 
     * 数据库字段信息:sn VARCHAR(32)
     */
    private String sn;

    /**
     * 字段名称：车辆名称
     * 
     * 数据库字段信息:vehicleName VARCHAR(50)
     */
    private String vehicleName;

    /**
     * 字段名称：车辆图标
     * 
     * 数据库字段信息:vehicleIcon VARCHAR(32)
     */
    private String vehicleIcon;

    /**
     * 字段名称：车牌号
     * 
     * 数据库字段信息:vehicleNumber VARCHAR(32)
     */
    private String vehicleNumber;
    
    private String plateNum;

    /**
     * 字段名称：车架号
     * 
     * 数据库字段信息:carFrame VARCHAR(32)
     */
    private String carFrame;

    /**
     * 字段名称：司机名称
     * 
     * 数据库字段信息:driverName VARCHAR(50)
     */
    private String driverName;

    /**
     * 字段名称：司机电话
     * 
     * 数据库字段信息:driverPhone VARCHAR(32)
     */
    private String driverPhone;

    /**
     * 字段名称：司机身份证号码
     * 
     * 数据库字段信息:idCard VARCHAR(32)
     */
    private String idCard;
    
    /**
     * 安装时间	
     * 数据库字段信息:installTime DATETIME
     */
    private String installTime;
    
    /**
     * 安装地址	
     * 数据库字段信息:installAddress VARCHAR(255)
     */
    private String installAddress;
    
    /**
     * 安装公司	
     * 数据库字段信息:installCompany VARCHAR(255)
     */
    private String installCompany;
    /**
     * 安装位置	
     * 数据库字段信息:installPosition VARCHAR(255)
     */
    private String installPosition;
    /**
     * 安装人员	
     * 数据库字段信息:installPersonnel VARCHAR(255)
     */
    private String installPersonnel;
    
    /**
     * 安装图片	
     * 数据库字段信息:installImage VARCHAR(1000)
     */
    private String installImage;

    private String iccid;
    
    private String imsi;
    
	/**
     * 安装图片集合
     * 多个图片用','分割,仅用页面展示
     */
    private List<String> installImageList;
    
    //平台过期时间段
    private String startExpiration;
    private String endExpiration;

	private String relationId;
	
	/**
	 * 设备所属用户的 app修改设备权限（0：无  1：有）
	 */
	private String belongUserAppUpdateDevFlag;
    
    
    private String rowClass;
    private String tdClass;
    private String expirationStr;
    private String orgName;
    
    private String devName;
    
    //激活时间段
    private String startActivationExpiration;
    private String endActivationExpiration;
    /**
     * 绑定者
     */
    private String bindUserId;
    
    /**
     * 绑定者账号
     */
    private String bindUserAccount;
    /**
     * 绑定者姓名
     */
    private String bindUserName;
    
    /**
     * 绑定者组id
     */
    private String bindUserOrgId;
    
    /**
     * 绑定组名称
     */
    private String bindUserOrgName;
    
    /**
     * 超速速度
     */
    private String overSpeed;
    
    private String parentId;
    
    private String fullParent;
    
    private String isActivation;  //是否激活 .查询条件
    
    private String vehicleModels;
    
    private String status;
    
    private String clientNbr;
    
    private String clientPwd;
    
    private String camNumber;
    
    private String url;
    
    private String account;
    
    private String email;
    
    private String userName;
    
    private Integer show;
    
    private String appFlag;
    
    /**
     * 根据IMEI批量查询，传入多个imei，以','分隔
     * 
     **/
    private String []  imeis;
    
    private String userIds;
    
    private String [] userIdArr;
    
    private Map<String, Object> statusMap;
    
    /*
     * 用户的到期时间
     */
    private Timestamp userExpiration;
    
    private String devUUID;
    
    private String userExpirationStr;
    
    private String loginUserId;

	/**
     * 用户过期时间月数
     */
    private String month;

	private Timestamp afterDaysTime;
    
    private Timestamp beforeDaysTime;
    
    //设备过期时间段
    private String startUserExpiration;
    private String endUserExpiration;
    
    private int online;   //是否在线
    
    private String accStatu;
    private String aCount;      	//设备总数
    private String aCountWeek;      //7天过期
    private String aCountMonth;     //60天过期
    private String aCountLife;      //已过期
    //到期设备搜索条件,用于搜索该用户及其下级用户的设备
    private String devUserId;
    
    private String language;
    
    private String exportType;
    
    private Integer isAttention;//设备是否关注(0是1否)
    
    private Timestamp parentUserExpiration; //上级用户的到期时间
	
	private Integer parentMonth;//上级用户的月份
	
	//设备管理搜索用字段,按设备名称或设备IMEI搜索
	private String imeiOrDevNam;
	
	//设备管理搜索用字段,按设备型号精确搜索
	private String mcType2;
	
	//设备状态、0全部状态、1正常使用、2未激活、3即将到期、4已过期
	private Integer dvStauts;
	
	private String userType;//用户类型
	
	private String deviceAlias;	//别名
    
    private String accFlag;
    	
    private String isBind;	//是否绑定
    
    private String expireStartTime;		//过期状态筛选   30天内
    
    private String expireEndTime;		//过期状态筛选   30天内
    
    private String subSearchId;			//下级用户搜索条件id
    
    private String imeiLike;
    
    private String isExpire;			//已过期
    
    //离线设备安装时间
    private String offLineStartTime;
    private String offLineEndTime;
    
    private String orgDevCount;//设备所在分组的设备总数
    
    private int limitStart=-1;//搜索条件 查询结果的起始行数 -1表示不可用
    
    private int limitEnd=-1;//搜索条件 查询结果的结束行数 -1表示不可用
    
    public final static String ANTEBAO_USERID = "12979";//安特保用户ID
    public final static String ANTEBAO_FULLPARENT = "1,12871,13366,13273,";//安特保全父ID
    
    private String imeiKeyword;//搜索条件,模糊搜索Imei号
    
	public String getImeiKeyword() {
		return imeiKeyword;
	}

	public void setImeiKeyword(String imeiKeyword) {
		this.imeiKeyword = imeiKeyword;
	}

	public int getLimitStart() {
		return limitStart;
	}

	public void setLimitStart(int limitStart) {
		this.limitStart = limitStart;
	}

	public int getLimitEnd() {
		return limitEnd;
	}

	public void setLimitEnd(int limitEnd) {
		this.limitEnd = limitEnd;
	}

	public String getOrgDevCount() {
		return orgDevCount;
	}

	public void setOrgDevCount(String orgDevCount) {
		this.orgDevCount = orgDevCount;
	}

	public String getImeiLike() {
		return imeiLike;
	}

	public void setImeiLike(String imeiLike) {
		this.imeiLike = imeiLike;
	}

	public String getAccFlag() {
		return accFlag;
	}

	public void setAccFlag(String accFlag) {
		this.accFlag = accFlag;
	}

	public String getImeiOrDevNam() {
		return imeiOrDevNam;
	}

	public void setImeiOrDevNam(String imeiOrDevNam) {
		this.imeiOrDevNam = imeiOrDevNam;
	}
	
	public String getMcType2() {
		return mcType2;
	}

	public void setMcType2(String mcType2) {
		this.mcType2 = mcType2;
	}

	public Integer getIsAttention() {
		return isAttention;
	}

	public void setIsAttention(Integer isAttention) {
		this.isAttention = isAttention;
	}
	
	public Integer getDvStauts() {
		return dvStauts;
	}

	public void setDvStauts(Integer dvStauts) {
		this.dvStauts = dvStauts;
	}



	/**
	 * 是否终生
	 * 0    是
	 * 1    否
	 * 
	 * @author Li.Shangzhi
	 * @time 2016-05-11 14:45:59
	 */
	private String isLife;
	
	/**
	 * 电机发动机号
	 */
	private String engineNumber;
	

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getaCount() {
		return aCount;
	}

	public void setaCount(String aCount) {
		this.aCount = aCount;
	}

	public String getaCountWeek() {
		return aCountWeek;
	}

	public void setaCountWeek(String aCountWeek) {
		this.aCountWeek = aCountWeek;
	}

	public String getaCountMonth() {
		return aCountMonth;
	}

	public void setaCountMonth(String aCountMonth) {
		this.aCountMonth = aCountMonth;
	}

	public String getaCountLife() {
		return aCountLife;
	}

	public void setaCountLife(String aCountLife) {
		this.aCountLife = aCountLife;
	}

	public String getBindUserId() {
		return bindUserId;
	}

	public void setBindUserId(String bindUserId) {
		this.bindUserId = bindUserId;
	}


	public void setIsLife(String isLife) {
		this.isLife = isLife;
	}

	public Device() {
    }
    
	public String getUserExpirationStr() {
		return userExpirationStr;
	}

	public void setUserExpirationStr(String userExpirationStr) {
		this.userExpirationStr = userExpirationStr;
	}
	
	public String getStartUserExpiration() {
		return startUserExpiration;
	}


	public void setStartUserExpiration(String startUserExpiration) {
		this.startUserExpiration = startUserExpiration;
	}


	public String getEndUserExpiration() {
		return endUserExpiration;
	}


	public void setEndUserExpiration(String endUserExpiration) {
		this.endUserExpiration = endUserExpiration;
	}

    public String getStartExpiration() {
		return startExpiration;
	}

	public void setStartExpiration(String startExpiration) {
		this.startExpiration = startExpiration;
	}

	public String getEndExpiration() {
		return endExpiration;
	}

	public void setEndExpiration(String endExpiration) {
		this.endExpiration = endExpiration;
	}
    public Device(String deviceName){
    	this.deviceName = deviceName;
    }
    public Device(String userId,String [] imeis) {
    	this.userId=userId;
    	this.imeis=imeis;
    }
    public String getImei() {
        return this.imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getIsLife() {
		return isLife;
	}

	public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMcType() {
        return this.mcType;
    }

    public void setMcType(String mcType) {
        this.mcType = mcType;
    }

    public String getSim() {
        return this.sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public Integer getStorager() {
		return storager;
	}
    
	public void setStorager(Integer storager) {
		this.storager = storager;
	}
	public Timestamp getExpiration() {
        return this.expiration;
    }

    public void setExpiration(Timestamp expiration) {
        this.expiration = expiration;
    }

    public Timestamp getActivationTime() {
        return this.activationTime;
    }

    public void setActivationTime(Timestamp activationTime) {
        this.activationTime = activationTime;
    }

    public String getReMark() {
        return this.reMark;
    }

    public void setReMark(String reMark) {
        this.reMark = reMark;
    }

    public String getSn() {
        return this.sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getVehicleName() {
        return this.vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleIcon() {
        return this.vehicleIcon;
    }

    public void setVehicleIcon(String vehicleIcon) {
        this.vehicleIcon = vehicleIcon;
    }

    public String getVehicleNumber() {
        return this.vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getCarFrame() {
        return this.carFrame;
    }

    public void setCarFrame(String carFrame) {
        this.carFrame = carFrame;
    }

    public String getDriverName() {
        return this.driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return this.driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getIdCard() {
        return this.idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    
	public String getRowClass() {
		return rowClass;
	}
	
	public void setRowClass(String rowClass) {
		this.rowClass = rowClass;
	}
	
	public String getExpirationStr() {
		return expirationStr;
	}
	
	public void setExpirationStr(String expirationStr) {
		this.expirationStr = expirationStr;
	}
	
	public String getTdClass() {
		return tdClass;
	}
	
	public void setTdClass(String tdClass) {
		this.tdClass = tdClass;
	}
	public String [] getImeis() {
		return imeis;
	}
	public void setImeis(String []  imeis) {
		this.imeis = imeis;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getDevName() {
		return devName;
	}
	public void setDevName(String devName) {
		this.devName = devName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getIsActivation() {
		return isActivation;
	}
	public void setIsActivation(String isActivation) {
		this.isActivation = isActivation;
	}
	public String getRelationId() {
		return relationId;
	}
	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}
	public String getVehicleModels() {
		return vehicleModels;
	}
	public void setVehicleModels(String vehicleModels) {
		this.vehicleModels = vehicleModels;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getClientNbr() {
		return clientNbr;
	}
	public void setClientNbr(String clientNbr) {
		this.clientNbr = clientNbr;
	}
	public String getClientPwd() {
		return clientPwd;
	}
	public void setClientPwd(String clientPwd) {
		this.clientPwd = clientPwd;
	}
	public String getCamNumber() {
		return camNumber;
	}
	public void setCamNumber(String camNumber) {
		this.camNumber = camNumber;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
    public Integer getShow() {
		return show;
	}

	public void setShow(Integer show) {
		this.show = show;
	}

	public Map<String, Object> getStatusMap() {
		return statusMap;
	}

	public void setStatusMap(Map<String, Object> statusMap) {
		this.statusMap = statusMap;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPlateNum() {
		return plateNum;
	}

	public void setPlateNum(String plateNum) {
		this.plateNum = plateNum;
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}


	public Timestamp getUserExpiration() {
		return userExpiration;
	}
	
	public String getDevUUID() {
		return devUUID;
	}

	public void setDevUUID(String devUUID) {
		this.devUUID = devUUID;
	}

	public void setUserExpiration(Timestamp userExpiration) {
		this.userExpiration = userExpiration;
	}
	

	public String getBindUserOrgId() {
		return bindUserOrgId;
	}

	public void setBindUserOrgId(String bindUserOrgId) {
		this.bindUserOrgId = bindUserOrgId;
	}

	public String getBindUserAccount() {
		return bindUserAccount;
	}

	public void setBindUserAccount(String bindUserAccount) {
		this.bindUserAccount = bindUserAccount;
	}

	public String getBindUserName() {
		return bindUserName;
	}

	public void setBindUserName(String bindUserName) {
		this.bindUserName = bindUserName;
	}

	public String getBindUserOrgName() {
		return bindUserOrgName;
	}

	public void setBindUserOrgName(String bindUserOrgName) {
		this.bindUserOrgName = bindUserOrgName;
	}

	public String getFullParent() {
		return fullParent;
	}

	public void setFullParent(String fullParent) {
		this.fullParent = fullParent;
	}

	public Timestamp getAfterDaysTime() {
		return afterDaysTime;
	}

	public void setAfterDaysTime(Timestamp afterDaysTime) {
		this.afterDaysTime = afterDaysTime;
	}

	public Timestamp getBeforeDaysTime() {
		return beforeDaysTime;
	}

	public void setBeforeDaysTime(Timestamp beforeDaysTime) {
		this.beforeDaysTime = beforeDaysTime;
	}
	
	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getOnline() {
		return online;
	}

	public void setOnline(int online) {
		this.online = online;
	}

	public String getAccStatu() {
		return accStatu;
	}

	public void setAccStatu(String accStatu) {
		this.accStatu = accStatu;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDevUserId() {
		return devUserId;
	}

	public void setDevUserId(String devUserId) {
		this.devUserId = devUserId;
	}

	public String getOverSpeed() {
		return overSpeed;
	}

	public void setOverSpeed(String overSpeed) {
		this.overSpeed = overSpeed;
	}

	public String getAppFlag() {
		return appFlag;
	}

	public void setAppFlag(String appFlag) {
		this.appFlag = appFlag;
	}
	public Timestamp getParentUserExpiration() {
		return parentUserExpiration;
	}

	public void setParentUserExpiration(Timestamp parentUserExpiration) {
		this.parentUserExpiration = parentUserExpiration;
	}

	public Integer getParentMonth() {
		return parentMonth;
	}

	public void setParentMonth(Integer parentMonth) {
		this.parentMonth = parentMonth;
	}

	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getExportType() {
		return exportType;
	}

	public void setExportType(String exportType) {
		this.exportType = exportType;
	}

	public String getMcTypeUseScope() {
		return mcTypeUseScope;
	}

	public void setMcTypeUseScope(String mcTypeUseScope) {
		this.mcTypeUseScope = mcTypeUseScope;
	}

	public String getInstallImage() {
		return installImage;
	}
	

	public void setInstallImage(String installImage) {
		this.installImage = installImage;
	}

	public String getInstallTime() {
		return installTime;
	}
	

	public void setInstallTime(String installTime) {
		this.installTime = installTime;
	}
	

	public String getInstallAddress() {
		return installAddress;
	}
	

	public void setInstallAddress(String installAddress) {
		this.installAddress = installAddress;
	}
	

	public String getInstallCompany() {
		return installCompany;
	}
	

	public void setInstallCompany(String installCompany) {
		this.installCompany = installCompany;
	}
	

	public String getInstallPosition() {
		return installPosition;
	}
	

	public void setInstallPosition(String installPosition) {
		this.installPosition = installPosition;
	}
	

	public String getInstallPersonnel() {
		return installPersonnel;
	}
	

	public void setInstallPersonnel(String installPersonnel) {
		this.installPersonnel = installPersonnel;
	}

	public List<String> getInstallImageList() {
		return installImageList;
	}
	

	public void setInstallImageList(List<String> installImageList) {
		this.installImageList = installImageList;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

	public String getDeviceAlias() {
		return deviceAlias;
	}

	public void setDeviceAlias(String deviceAlias) {
		this.deviceAlias = deviceAlias;
	}

	public String [] getUserIdArr() {
		return userIdArr;
	}

	public void setUserIdArr(String [] userIdArr) {
		this.userIdArr = userIdArr;
	}

	public String getIsBind() {
		return isBind;
	}

	public void setIsBind(String isBind) {
		this.isBind = isBind;
	}

	public String getStartActivationExpiration() {
		return startActivationExpiration;
	}

	public void setStartActivationExpiration(String startActivationExpiration) {
		this.startActivationExpiration = startActivationExpiration;
	}

	public String getEndActivationExpiration() {
		return endActivationExpiration;
	}

	public void setEndActivationExpiration(String endActivationExpiration) {
		this.endActivationExpiration = endActivationExpiration;
	}

	public String getExpireStartTime() {
		return expireStartTime;
	}

	public void setExpireStartTime(String expireStartTime) {
		this.expireStartTime = expireStartTime;
	}

	public String getExpireEndTime() {
		return expireEndTime;
	}

	public void setExpireEndTime(String expireEndTime) {
		this.expireEndTime = expireEndTime;
	}

	public String getSubSearchId() {
		return subSearchId;
	}

	public void setSubSearchId(String subSearchId) {
		this.subSearchId = subSearchId;
	}

	public String getIsExpire() {
		return isExpire;
	}

	public void setIsExpire(String isExpire) {
		this.isExpire = isExpire;
	}
	
	public String getIccid() {
		return iccid;
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getOffLineStartTime() {
		return offLineStartTime;
	}

	public void setOffLineStartTime(String offLineStartTime) {
		this.offLineStartTime = offLineStartTime;
	}

	public String getOffLineEndTime() {
		return offLineEndTime;
	}

	public void setOffLineEndTime(String offLineEndTime) {
		this.offLineEndTime = offLineEndTime;
	}

	public String getBelongUserAppUpdateDevFlag() {
		return belongUserAppUpdateDevFlag;
	}

	public void setBelongUserAppUpdateDevFlag(String belongUserAppUpdateDevFlag) {
		this.belongUserAppUpdateDevFlag = belongUserAppUpdateDevFlag;
	}
	
}