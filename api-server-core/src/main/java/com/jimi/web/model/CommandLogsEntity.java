/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.jimi.commons.DatetimeUtil;

/**
 * @FileName CommandLogsEntity.java
 * @Description:
 *
 * @Date 2017年7月25日 上午10:48:37
 * @author li.shangzhi
 * @version 1.0
 */
public class CommandLogsEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 编号id
	 */
	private Integer id;
	/**
	 * 流水号
	 */
	private String taskId;
	/**
	 * codeId
	 */
	private String codeId;
	/**
	 * 指令编码
	 */
	private String code;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 指令状态0：执行失败,1：执行成功,3 : 待发送,4:已取消
	 */
	private String isExecute;
	/**
	 * 创建时间
	 */
	private Date creationDate;
	/**
	 * 创建时间（开始时间，用于搜索）
	 */
	private Date startCreationDate;
	/**
	 * 创建时间（结束时间，用于搜索）
	 */
	private Date endCreationDate;
	/**
	 * 发送时间
	 */
	private Date sendTime;
	/**
	 * 发送时间（开始时间，用于搜索）
	 */
	private Date startSendTime;
	/**
	 * 发送时间（结束时间，用于搜索）
	 */
	private Date endSendTime;
	/**
	 * 发送者
	 */
	private String sender;
	/**
	 * 平台：web app
	 */
	private String platform;
	/**
	 * 修改时间
	 */
	private Date updationDate;
	/**
	 * 修改时间（开始时间，用于搜索）
	 */
	private Date startUpdationDate;
	/**
	 * 修改时间（结束时间，用于搜索）
	 */
	private Date endUpdationDate;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 修改人
	 */
	private String updatedBy;
	/**
	 * 接收：imei
	 */
	private String receiveDevice;
	/**
	 * 0在线 1离线
	 */
	private String isOffLine;
	/**
	 * 编号id
	 */
	private String idsource;

	public CommandLogsEntity() {
	}

	public CommandLogsEntity(String sender, String receiveDevice, String code, String codeId, String content, String platform,
			String isExecute, String createdBy, String updatedBy,String isOffLine,String idsource,String taskId) {
		this.sender = sender;
		this.receiveDevice = receiveDevice;
		this.code = code;
		this.codeId = codeId;
		this.isExecute = isExecute;
		this.content = content;
		this.platform = platform;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.isOffLine = isOffLine;
		this.idsource = idsource;
		this.taskId = taskId;
	}

	public void setId(Integer id) {
		id = id == null ? 0 : id;
		this.id = id;
	}

	public Integer getId() {
		return this.id == null ? 0 : this.id;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskId() {
		return this.taskId == null ? null : this.taskId.trim();
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getCodeId() {
		return this.codeId == null ? null : this.codeId.trim();
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code == null ? null : this.code.trim();
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return this.content == null ? null : this.content.trim();
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	public String getIsExecute() {
		return this.isExecute == null ? null : this.isExecute.trim();
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setStartCreationDate(Date startCreationDate) {
		this.startCreationDate = startCreationDate;
	}

	public Date getStartCreationDate() {
		return this.startCreationDate;
	}

	public void setEndCreationDate(Date endCreationDate) {
		this.endCreationDate = endCreationDate;
	}

	public Date getEndCreationDate() {
		return this.endCreationDate;
	}

	public String getStringCreationDate() {
		if (this.creationDate == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.creationDate, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public Date getSendTime() {
		return this.sendTime;
	}

	public void setStartSendTime(Date startSendTime) {
		this.startSendTime = startSendTime;
	}

	public Date getStartSendTime() {
		return this.startSendTime;
	}

	public void setEndSendTime(Date endSendTime) {
		this.endSendTime = endSendTime;
	}

	public Date getEndSendTime() {
		return this.endSendTime;
	}

	public String getStringSendTime() {
		if (this.sendTime == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.sendTime, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSender() {
		return this.sender == null ? null : this.sender.trim();
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getPlatform() {
		return this.platform == null ? null : this.platform.trim();
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

	public Date getUpdationDate() {
		return this.updationDate;
	}

	public void setStartUpdationDate(Date startUpdationDate) {
		this.startUpdationDate = startUpdationDate;
	}

	public Date getStartUpdationDate() {
		return this.startUpdationDate;
	}

	public void setEndUpdationDate(Date endUpdationDate) {
		this.endUpdationDate = endUpdationDate;
	}

	public Date getEndUpdationDate() {
		return this.endUpdationDate;
	}

	public String getStringUpdationDate() {
		if (this.updationDate == null) {
			return null;
		}
		return DatetimeUtil.DateToString(this.updationDate, DatetimeUtil.LONG_DATE_TIME_PATTERN);
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return this.createdBy == null ? null : this.createdBy.trim();
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy() {
		return this.updatedBy == null ? null : this.updatedBy.trim();
	}

	public void setReceiveDevice(String receiveDevice) {
		this.receiveDevice = receiveDevice;
	}

	public String getReceiveDevice() {
		return this.receiveDevice == null ? null : this.receiveDevice.trim();
	}

	public void setIsOffLine(String isOffLine) {
		this.isOffLine = isOffLine;
	}

	public String getIsOffLine() {
		return this.isOffLine == null ? null : this.isOffLine.trim();
	}

	public void setIdsource(String idsource) {
		this.idsource = idsource;
	}

	public String getIdsource() {
		return this.idsource == null ? null : this.idsource.trim();
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
