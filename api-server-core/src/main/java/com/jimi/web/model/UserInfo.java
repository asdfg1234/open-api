package com.jimi.web.model;

import java.util.List;

public class UserInfo extends GenericDomain {

	public enum UserTypeEnum {

		ADMIN("0"), // admin
		SALE("11"), // 销售
		AGENT("8"), // 代理商
		NORMAL("9"), // 普通用户
		APP("3"), // app用户
		EXPERIENCE("12");// 体验账号

		private String code;

		private UserTypeEnum(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

	}

	private int oppFlag;

	private String nickName;

	private String isBatchSendIns;

	private String isBatchSendFM;

	private String updateDevFlag;

	private String appUpdateDevFlag;

	private String mibi;

	private String sms;

	private Integer parentFlag;

	private String timeZones;

	private String passWord;

	private String Language;

	private List<UserInfo> userInfoList;

	private String webLogin;

	private String appLogin;

	/**
	 * 设备总数 ，用来做用户设备统计
	 */
	private Integer devTotal;

	// 查询用到的搜索条件
	private String keyword;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	private String userId;
	/**
	 * 账号
	 */
	private String account;
	/**
	 * 显示名
	 */
	private String name;
	/**
	 * 类型
	 */
	private Integer type;
	/**
	 * 父ID
	 */
	private String parentId;

	/**
	 * old_parentId
	 */
	private String oldParentId;

	/**
	 * 全父ID
	 */
	private String fullParentId;

	/**
	 * 老的全父ID
	 */
	private String oldFullParentId;

	/**
	 * 电话
	 */
	private String phone;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 公司
	 */
	private String companyName;
	/**
	 * 联系人
	 */
	private String contact;

	/**
	 * 父节点名称
	 */
	private String parentName;

	/**
	 * 父节点账号
	 */
	private String parentAccount;

	/**
	 * 父节点联系方式
	 */
	private String parentPhone;

	/**
	 * 父节点类型
	 */
	private String parentType;
	/**
	 * 父节点联系人
	 */
	private String parentContact;

	public String getParentContact() {
		return parentContact;
	}

	public void setParentContact(String parentContact) {
		this.parentContact = parentContact;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getFullParentId() {
		return fullParentId;
	}

	public void setFullParentId(String fullParentId) {
		this.fullParentId = fullParentId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getIsBatchSendIns() {
		return isBatchSendIns;
	}

	public void setIsBatchSendIns(String isBatchSendIns) {
		this.isBatchSendIns = isBatchSendIns;
	}

	public String getIsBatchSendFM() {
		return isBatchSendFM;
	}

	public void setIsBatchSendFM(String isBatchSendFM) {
		this.isBatchSendFM = isBatchSendFM;
	}

	public String getUpdateDevFlag() {
		return updateDevFlag;
	}

	public void setUpdateDevFlag(String updateDevFlag) {
		this.updateDevFlag = updateDevFlag;
	}

	public String getAppUpdateDevFlag() {
		return appUpdateDevFlag;
	}

	public void setAppUpdateDevFlag(String appUpdateDevFlag) {
		this.appUpdateDevFlag = appUpdateDevFlag;
	}

	public String getMibi() {
		return mibi;
	}

	public void setMibi(String mibi) {
		this.mibi = mibi;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public Integer getParentFlag() {
		return parentFlag;
	}

	public void setParentFlag(Integer parentFlag) {
		this.parentFlag = parentFlag;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public List<UserInfo> getUserInfoList() {
		return userInfoList;
	}

	public void setUserInfoList(List<UserInfo> userInfoList) {
		this.userInfoList = userInfoList;
	}

	public String getTimeZones() {
		return timeZones;
	}

	public void setTimeZones(String timeZones) {
		this.timeZones = timeZones;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public int getOppFlag() {
		return oppFlag;
	}

	public void setOppFlag(int oppFlag) {
		this.oppFlag = oppFlag;
	}

	public String getWebLogin() {
		return webLogin;
	}

	public void setWebLogin(String webLogin) {
		this.webLogin = webLogin;
	}

	public String getAppLogin() {
		return appLogin;
	}

	public void setAppLogin(String appLogin) {
		this.appLogin = appLogin;
	}

	public String getOldParentId() {
		return oldParentId;
	}

	public void setOldParentId(String oldParentId) {
		this.oldParentId = oldParentId;
	}

	public String getOldFullParentId() {
		return oldFullParentId;
	}

	public void setOldFullParentId(String oldFullParentId) {
		this.oldFullParentId = oldFullParentId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParentAccount() {
		return parentAccount;
	}

	public void setParentAccount(String parentAccount) {
		this.parentAccount = parentAccount;
	}

	public String getParentPhone() {
		return parentPhone;
	}

	public void setParentPhone(String parentPhone) {
		this.parentPhone = parentPhone;
	}

	public String getParentType() {
		return parentType;
	}

	public void setParentType(String parentType) {
		this.parentType = parentType;
	}

	public Integer getDevTotal() {
		return devTotal;
	}

	public void setDevTotal(Integer devTotal) {
		this.devTotal = devTotal;
	}

	@Override
	public String toString() {
		return "UserInfo [userId=" + userId + ", account=" + account + ", name=" + name + ", type=" + type + ", parentId=" + parentId
				+ ", fullParentId=" + fullParentId + ", phone=" + phone + ", email=" + email + ", companyName=" + companyName
				+ ", contact=" + contact + "]";
	}

}
