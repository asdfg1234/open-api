/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.api.impl;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jimi.framework.annotation.log.LoggerProfile;
import com.jimi.framework.base.BaseApi;
import com.jimi.framework.bean.BeanUtil;
import com.jimi.web.api.IUserApiService;
import com.jimi.web.comm.DeviceLocationInfo;
import com.jimi.web.dto.input.UserInfoInputDto;
import com.jimi.web.dto.output.CoreUserOutputDto;
import com.jimi.web.dto.output.DeviceInfoOutputDto;
import com.jimi.web.dto.output.DeviceLocationInfoOutputDto;
import com.jimi.web.model.CoreUser;
import com.jimi.web.model.Device;
import com.jimi.web.service.IDeviceService;
import com.jimi.web.service.IUserService;
import com.jimi.web.util.DeviceUtil;
import com.jimi.web.util.TrackApiUtils;

/**
 * @FileName UserApiServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月13日 下午1:46:26
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class UserApiServiceImpl extends BaseApi implements IUserApiService {

	@Resource
	private IUserService userService;

	@Resource
	private IDeviceService deviceService;
	
	@Value("${language}")
	private String language;
	
	@Override
	@LoggerProfile(methodNote = "IUserApiService.getAccount")
	public CoreUserOutputDto getAccount(String account) {
		CoreUser coreUser = userService.getUserInfo(account);
		if (null == coreUser) {
			logger.info("找不到账号，账号为：{}", account);
			return null;
		}
		return (CoreUserOutputDto) BeanUtil.convertBean(coreUser, CoreUserOutputDto.class);
	}
	
	@Override
	@LoggerProfile(methodNote = "IUserApiService.checkUserRange")
	public CoreUserOutputDto checkUserRange(String userId, String account) {
		// 查询是不是子账号
		CoreUser coreUser = userService.checkUserRange(userId, account);
		if (null == coreUser) {
			logger.info("找不到子账号，userId：{}，子账号为：{}", userId, account);
			return null;
		}
		return (CoreUserOutputDto) BeanUtil.convertBean(coreUser, CoreUserOutputDto.class);
	}

	@Override
	@LoggerProfile(methodNote = "IUserApiService.getAccounts")
	public List<CoreUserOutputDto> getAccounts(String userId) {
		List<CoreUser> userList = userService.getAllChildsUser(userId);
		if (null != userList && userList.size() > 0) {
			return (List<CoreUserOutputDto>) BeanUtil.convertBeanList(userList, CoreUserOutputDto.class);
		}
		return Collections.emptyList();
	}

	@Override
	@LoggerProfile(methodNote = "IUserApiService.getDevices")
	public List<DeviceInfoOutputDto> getDevices(String userId) {
		List<Device> list = deviceService.getDeviceByUserId(userId);
		if (null != list && list.size() > 0) {
			return (List<DeviceInfoOutputDto>) BeanUtil.convertBeanList(list, DeviceInfoOutputDto.class);
		}
		return Collections.emptyList();
	}

	@Override
	@LoggerProfile(methodNote = "IUserApiService.getAllLocations")
	public List<DeviceLocationInfoOutputDto> getAllLocations(String userId, String mapType) {
		List<Device> deviceList = deviceService.getDeviceByUserId(userId);
		if (null != deviceList && !deviceList.isEmpty()) {
			List<DeviceLocationInfo> infoLst = DeviceUtil.getDeviceLocationInfo(deviceList, mapType, language);
			return (List<DeviceLocationInfoOutputDto>) BeanUtil.convertBeanList(infoLst, DeviceLocationInfoOutputDto.class);
		}
		return Collections.emptyList();
	}

	@Override
	@LoggerProfile(methodNote = "IUserApiService.saveUser")
	public String saveUser(UserInfoInputDto userInfoInputDto) {
		String code = TrackApiUtils.createPlatUser(userInfoInputDto);
		logger.info("创建平台账号操作完成，返回码：{}", code);
		return code;
	}

}
