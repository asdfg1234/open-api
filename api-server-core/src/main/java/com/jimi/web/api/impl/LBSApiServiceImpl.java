/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月17日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.api.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jimi.exception.SysApiException;
import com.jimi.framework.annotation.filter.ReqFilter;
import com.jimi.framework.annotation.log.LoggerProfile;
import com.jimi.framework.base.BaseApi;
import com.jimi.framework.bean.BeanUtil;
import com.jimi.web.api.ILBSApiService;
import com.jimi.web.dto.output.LbsPointOutputDto;
import com.jimi.web.model.lbs.LbsPoint;
import com.jimi.web.service.IDeviceService;
import com.jimi.web.util.TrackApiUtils;

/**
 * @FileName LBSApiServiceImpl.java
 * @Description:
 *
 * @Date 2017年4月17日 上午11:44:30
 * @author li.shangzhi
 * @version 1.0
 */
@Service
public class LBSApiServiceImpl extends BaseApi implements ILBSApiService {

	@Resource
	private IDeviceService deviceService;

	@Override
	@ReqFilter(keySpel = "'IMEI_'+#imei", limit = 1, time = 10000)
	@LoggerProfile(methodNote = "ILBSApiService.getAddress")
	public LbsPointOutputDto getAddress(String imei, String lbs, String wifi) throws SysApiException {
		LbsPoint lbsPoint = TrackApiUtils.analysisLbsOrWifi(imei, lbs, wifi);
		return (LbsPointOutputDto) BeanUtil.convertBean(lbsPoint, LbsPointOutputDto.class);
	}

}
