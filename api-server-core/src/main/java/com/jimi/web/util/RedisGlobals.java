/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月19日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.util;

/**
 * @FileName RedisGlobals.java
 * @Description:
 *
 * @Date 2017年4月19日 下午2:35:23
 * @author li.shangzhi
 * @version 1.0
 */
public class RedisGlobals {

	/** 心跳包 redis Key */
	public static final String TRACKER_HB = "TRACKER_HB";

	/** GPS数据 redis Key */
	public static final String TRACKER_GPS = "TRACKER_GPS";

	/** 设备在线离线数据 */
	public static final String DC_DEVICE_LOGININFO = "DC_DEVICE_LOGININFO";

	/** 设备对应的机型和应用ID */
	public static final String DC_IMEI_APPID = "DC_IMEI_APPID";

	/** 机型在线短消息指令协议号 */
	public static final String TRACKER_SMS_PRO_NO_Map_PRO = "TRACKER_SMS_PRO_NO_Map_PRO";

	/**
	 * 指令序号key
	 */
	public static final String CmdSqNo = "SEQUENCE_INC_KEY";

	/**
	 * 设备统计信息
	 */
	public static final String TRACKER_DEVICE_COUNT = "TRACKER_DEVICE_COUNT";

	/**
	 * 用户拥有的所有机型（包含下级）
	 */
	public static final String TUQIANG_USER_MCTYPE = "TUQIANG_USER_MCTYPE";
	
	/**告警时间设置*/
	public static final String TRACKER_ALTER_TIME = "TRACKER_ALTER_TIME";
	
	/** 告警设置记忆 */
	public static final String TRACKER_ALTER_SET = "TRACKER_ALTER_SET";
}
