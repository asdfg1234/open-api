/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月19日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.util;

/**
 * @FileName Globals.java
 * @Description:
 *
 * @Date 2017年4月19日 下午2:18:44
 * @author li.shangzhi
 * @version 1.0
 */
public class Globals {

	public static final class DeviceToCode {
		public static final int Command_execution_success = 255; // 成功

		public static final int CommandsData_error = 240;// 数据错误

		public static final int CommandsNot_support = 243; // 不支持

		public static final int CommandsDevice_Busy = 252;// 设备忙

		public static final int CommandsDevice_interrupts = 238;// 设备中断

		public static final int CommandsTime_out = 225;// 超时！

		public static final int CommandsParameter_error = 226;// 参数错误

		public static final int CommandsCommand_was_not_executed_correctly = 227;// 指令未正确执行

		public static final int CommandsDevice_NONET = 228;// 设备没在线

		public static final int CommandsDevice_NONETTRUE = 229;// 网络错误（连接断开，等）

		public static final int CommandsUnknown_error = 404;//
	}

	public static final class InstructionCode {
		// 指令超时时间
		public static final int SENDCOMMANDTIME = 30;// 单位秒

		public static final String OFFLINE_STATUS_FAILED = "0";// 离线指令状态:离线失败

		public static final String OFFLINE_STATUS_ASK = "5";// 离线指令状态:已存在离线指令，页面询问是否覆盖已有离线指令

		public static final String OFFLINE_STATUS_TOSEND = "3";// 离线指令状态:待发送

		public static final String EXCUTE_STATUS_FAILED = "0";// 指令执行状态:执行失败

		public static final String EXCUTE_STATUS_SUCCESS = "1";// 指令执行状态:执行成功

		public static final String STATUS_YES = "1";

		public static final String STATUS_NO = "0";

		/**
		 * 常规指令
		 */
		public static final String NORMAL_INS = "normalIns";

		/**
		 * 自定义指令
		 */
		public static final String CUSTOM_INS = "customIns";
	}

	// 指令日志相关
	public static final class CommandLog {
		public static final String PLATFORM_WEB = "WEB";
		public static final String PLATFORM_APP = "APP";

	}
}
