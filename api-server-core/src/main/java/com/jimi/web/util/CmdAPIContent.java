/*
 * Created: 2013-6-9 下午8:09:04
 * ==================================================================================================
 * API规范。
 * Cicove Technology Corp. Ltd. License, Version 1.0 
 * Copyright (c) 2010-2013 Cicove Tech. Co.,Ltd.   
 * Published by R&D Department, All rights reserved.
 * For the convenience of communicating and reusing of codes, 
 * Any java names,variables as well as comments should be made according to the regulations strictly.
 *
 * ==================================================================================================
 * This software consists of contributions made by Cicove R&D.
 * @author: li.shangzhi
 * @file: APIContent.java
 */
package com.jimi.web.util;

import java.io.Serializable;

public class CmdAPIContent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2127409162712908650L;

	public CmdAPIContent() {
	}

	private boolean ok = true;

	/**
	 * 返回的数据包
	 */
	private Object data;

	/**
	 * 错误码，请查globs
	 */
	private int code = 0;
	/**
	 * 消息处理
	 */
	private String msg;

	private String cmdSeqNo;

	public String getCmdSeqNo() {
		return cmdSeqNo;
	}

	public void setCmdSeqNo(String cmdSeqNo) {
		this.cmdSeqNo = cmdSeqNo;
	}

	/**
	 * 返回数据成功，设置数据。
	 * 
	 * @param data
	 */
	public CmdAPIContent(Object data) {
		this.data = data;
	}

	public CmdAPIContent(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public void setCode(int code) {

		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
