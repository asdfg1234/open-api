/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月19日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.util;

/**
 * @FileName EelctricFenceCode.java
 * @Description:
 *
 * @Date 2017年4月19日 下午4:56:06
 * @author li.shangzhi
 * @version 1.0
 */
public class EelctricFenceCode {

	/**
	 * 超过机型支持围栏数
	 */
	public static final int MORE_THAN_SET_NUMBER = 41001;

	/**
	 * 围栏名已存在
	 */
	public static final int YES_GEOZONE = 41002;

	/**
	 * 设备不在线
	 */
	public static final int DEVICE_NONET = 41003;

	/**
	 * 电子围栏操作失败
	 */
	public static final int GEOZONE_ERROR = 41004;
}
