/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年7月5日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jimi.web.dto.input.UserInfoInputDto;
import com.jimi.web.model.Device;
import com.jimi.web.model.GpsPoint;
import com.jimi.web.model.lbs.LbsPoint;
import com.jimi.web.util.http.Result;
import com.jimi.web.util.http.SendRequest;

/**
 * @FileName TrackApiUtils.java
 * @Description:
 *
 * @Date 2017年7月5日 上午10:41:34
 * @author li.shangzhi
 * @version 1.0
 */
@Component
public class TrackApiUtils {

	private static final Logger logger = LoggerFactory.getLogger(TrackApiUtils.class);

	private static final String SUCCESS = "0";

	private static String gpsWebUrl;

	private static String lbsWebUrl;

	private static String lbsWebToken;

	private static String tuqiangWebUrl;

	private static String tuqiangWebToken;

	private static String tuqiangWebVer;

	@Value("${gps.web.url}")
	public void setGpsWebUrl(String gpsWebUrl) {
		TrackApiUtils.gpsWebUrl = gpsWebUrl;
	}

	@Value("${lbs.web.url}")
	public void setLbsWebUrl(String lbsWebUrl) {
		TrackApiUtils.lbsWebUrl = lbsWebUrl;
	}

	@Value("${lbs.web.token}")
	public void setLbsWebToken(String lbsWebToken) {
		TrackApiUtils.lbsWebToken = lbsWebToken;
	}

	@Value("${tuqiang.web.url}")
	public void setTuqiangWebUrl(String tuqiangWebUrl) {
		TrackApiUtils.tuqiangWebUrl = tuqiangWebUrl;
	}

	@Value("${tuqiang.web.token}")
	public void setTuqiangWebToken(String tuqiangWebToken) {
		TrackApiUtils.tuqiangWebToken = tuqiangWebToken;
	}

	@Value("${tuqiang.web.ver}")
	public void setTuqiangWebVer(String tuqiangWebVer) {
		TrackApiUtils.tuqiangWebVer = tuqiangWebVer;
	}

	/**
	 * @Title: createPlatUser
	 * @Description: 调用途强Api接口创建平台账号
	 * @return 返回错误码
	 * @author li.shangzhi
	 * @date 2017年7月17日 下午2:15:04
	 */
	public static String createPlatUser(UserInfoInputDto userInfoInputDto) {
		Map<String, String> params = Maps.newHashMap();
		params.put("accessToken", tuqiangWebToken);
		params.put("method", "addPlateUser");
		params.put("ver", tuqiangWebVer);
		params.put("loginUserId", userInfoInputDto.getUserId());
		params.put("type", userInfoInputDto.getAccountType());
		params.put("nickName", userInfoInputDto.getNickName());
		try {
			Des des = new Des("JiMiTrackSolid"); // DES加密、秘钥固定
			params.put("account", des.encrypt(userInfoInputDto.getAccount()));
			params.put("password", des.encrypt(userInfoInputDto.getPassword()));
		} catch (Exception e) {
			logger.error("TrackApiUtils.createPlatUser()加密账号、密码异常。{}", e.getMessage(), e);
		}
		params.put("phone", userInfoInputDto.getPhone());
		params.put("email", userInfoInputDto.getEmail());
		params.put("contact", userInfoInputDto.getContact());
		params.put("companyName", userInfoInputDto.getCompanyName());
		params.put("webLogin", "1");// 新增用户web端登录权限（0:关闭，1：打开）
		params.put("appLogin", "1");// 新增用户app端登陆权限（0:关闭，1：打开）
		params.put("isBatchSendIns", "0");// 新增用户批量下发指令权限（0:关闭，1：打开）
		params.put("isBatchSendFM", "0");// 新增用户批量下发工作模式权限（0:关闭，1：打开）
		params.put("updateDevFlag", "0");// 新增用户web端修改设备权限（0:关闭，1：打开）
		params.put("appUpdateDevFlag", "0");// 新增用户app端修改设备权限（0:关闭，1：打开）
		JSONObject jsonObj = sendTrackApiRequest(tuqiangWebUrl, params);
		if (null != jsonObj) {
			return jsonObj.getString("code");
		}
		return null;
	}
	
	/**
	 * @Title: moveEuipment 
	 * @Description: 转移/销售设备
	 * @param srcUserId 原用户ID
	 * @param destUserId 目标用户ID
	 * @param imeis 设备imei集合，逗号隔开(单次操作做多500个设备)
	 * @return 返回错误码
	 * @author li.shangzhi
	 * @date 2017年7月17日 下午4:10:21
	 */
	public static String moveEuipment(String srcUserId, String destUserId, String imeis) {
		Map<String, String> params = Maps.newHashMap();
		params.put("accessToken", tuqiangWebToken);
		params.put("method", "moveEuipment");
		params.put("ver", tuqiangWebVer);
		params.put("loginUserId", srcUserId);
		params.put("targetUserId", destUserId);
		params.put("imeis", imeis);
		JSONObject jsonObj = sendTrackApiRequest(tuqiangWebUrl, params);
		if (null != jsonObj) {
			return jsonObj.getString("code");
		}
		return null;
	}
	
	/**
	 * @Title: updateEquipment 
	 * @Description: 修改设备信息
	 * @param device 设备对象
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年8月15日 下午4:25:31
	 */
	public static String updateEquipment(Device device) {
		Map<String, String> params = Maps.newHashMap();
		params.put("accessToken", tuqiangWebToken);
		params.put("method", "updateEquipment");
		params.put("ver", tuqiangWebVer);
		params.put("imei", device.getImei());
		params.put("deviceName", device.getDeviceName());
		params.put("vehicleName", device.getVehicleName());
		params.put("vehicleIcon", device.getVehicleIcon());
		params.put("vehicleNumber", device.getVehicleNumber());
		params.put("vehicleModels", device.getVehicleModels());
		params.put("driverName", device.getDriverName());
		params.put("driverPhone", device.getDriverPhone());
		JSONObject jsonObj = sendTrackApiRequest(tuqiangWebUrl, params);
		if (null != jsonObj) {
			return jsonObj.getString("code");
		}
		return null;
	}

	/**
	 * @Title: getTrack
	 * @Description:获取轨迹信息
	 * @param imei
	 * @param startTime
	 *            开始时间， 格式：yyyy-mm-dd hh:mm:ss
	 * @param endTime
	 *            结束时间，格式：yyyy-mm-dd hh:mm:ss
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月17日 下午3:04:42
	 */
	public static List<GpsPoint> getTrack(String imei, String startTime, String endTime) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("_method_", "trackByTime");
		paramMap.put("imei", imei);
		paramMap.put("startTime", startTime);
		paramMap.put("endTime", endTime);

		JSONObject jsonObj = sendTrackApiRequest(gpsWebUrl, paramMap);
		if (null != jsonObj) {
			String code = jsonObj.getString("code");
			if (SUCCESS.equals(code)) {
				return JSONObject.parseArray(jsonObj.getString("data"), GpsPoint.class);
			}
		}
		return Lists.newArrayList();
	}

	/**
	 * @Title: analysisLbsOrWifi
	 * @Description: 分析lbs或wifi地址，得到经纬度信息
	 * @param imei
	 *            设备imei
	 * @param lbs
	 *            LBS信息组(mcc,mnc,lac,cell,rssi)最多7个，每组五项，每一项不能为空，顺序不能改变
	 *            MCC移动国家代码，中国460 MNC移动网络码，移动0 LAC基站分区信息, 2312 23222
	 *            CELL基站编码23222 RSSI信号量-70
	 * @param wifi
	 *            mac地址，没有中间的冒号,大小写不敏感|rssi信号强度，示例：mac1,rssi1|mac2,rssi2
	 * @return
	 * @author li.shangzhi
	 * @date 2017年7月17日 下午3:17:48
	 */
	public static LbsPoint analysisLbsOrWifi(String imei, String lbs, String wifi) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("imei", imei);
		paramMap.put("token", lbsWebToken);
		paramMap.put("lbs", lbs);
		paramMap.put("wifi", wifi);
		JSONObject jsonObj = sendTrackApiRequest(lbsWebUrl, paramMap);
		if (null != jsonObj) {
			String code = jsonObj.getString("code");
			if (SUCCESS.equals(code)) {
				return JSONObject.parseObject(jsonObj.getString("data"), LbsPoint.class);
			}
		}
		return null;
	}

	private static JSONObject sendTrackApiRequest(String url, Map<String, String> params) {
		try {
			logger.info("调用track-api，url地址：{}，参数列表：{}", url, params);
			Result rst = SendRequest.sendPost(url, null, params, "utf-8");
			String json = rst.getHtml(rst, "utf-8");
			logger.info("调用track-api成功。返回结果：{}", json);
			return JSONObject.parseObject(json);
		} catch (IOException | InterruptedException | ExecutionException e) {
			logger.error("调用track-api异常。url：{}，参数：{}，异常信息：{}", url, params, e.getMessage(), e);
		}
		return null;
	}
	
}
