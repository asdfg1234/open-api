/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.comm;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * @FileName DeviceType.java
 * @Description:
 *
 * @Date 2017年4月13日 下午6:03:10
 * @author li.shangzhi
 * @version 1.0
 */
public class DeviceType {

	/** 不接acc的设备机型 */
	public enum NoAccMcType {
		GT300A("GT300A"), GT03C("GT03C"), GT300("GT300"), GT02A("GT02A"), TR02("TR02"), GT200("GT200"), GT710("GT710"), GT700("GT700"), GT300S(
				"GT300S"), GT300SW("GT300SW"), GT360("GT360"), GT300N("GT300N");
		private String value;

		private NoAccMcType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public static boolean ifExis(String value) {
			for (NoAccMcType nmt : NoAccMcType.values()) {
				if (nmt.getValue().equals(value)) {
					return true;
				}
			}
			return false;
		}
	}

	/** 不需要设置录音时间的设备机型 */
	public enum SetTimeMcType {
		GT500L("GT500L"), GT500S("GT500S");
		private String value;

		private SetTimeMcType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public static boolean ifSetTime(String value) {
			for (SetTimeMcType timeMcType : SetTimeMcType.values()) {
				if (timeMcType.getValue().equals(value)) {
					return true;
				}
			}
			return false;
		}
	}

	/** 不接控制台的车型 */
	public enum ConsoleVehicleType {
		AOTOMOBILE("aotomobile"), ELECTROMOBILE("electromobile");
		private String value;

		private ConsoleVehicleType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public static boolean ifExis(String value) {
			for (ConsoleVehicleType nvt : ConsoleVehicleType.values()) {
				if (nvt.getValue().equals(value)) {
					return true;
				}
			}
			return false;
		}
	}

	/** 支持录音的机型 */
	public enum Record {
		GT300L("GT300L"), GT500S("GT500S"), GT360S("GT360S"), GT500L("GT500L");
		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		private Record(String value) {
			this.value = value;
		}

		public static boolean ifExis(String value) {
			for (Record rd : Record.values()) {
				if (rd.getValue().equals(value)) {
					return true;
				}
			}
			return false;
		}
	}
	
	/**
	 * 需要根据登录次数计算电量的设备
	 * @author Administrator
	 *
	 */
	public enum SubdivideElecQuantityMcType {
		GT710("GT710", 2);
		private String mcType;
		private int times;

		public String getType() {
			return mcType;
		}

		public void setType(String mcType) {
			this.mcType = mcType;
		}

		public int getTimes() {
			return times;
		}

		public void setTimes(int times) {
			this.times = times;
		}

		private SubdivideElecQuantityMcType(String mcType, int times) {
			this.mcType = mcType;
			this.times = times;
		}

		public static Integer checkType(String type) {
			for (SubdivideElecQuantityMcType sem : SubdivideElecQuantityMcType.values()) {
				if (sem.getType().equals(type)) {
					return sem.getTimes();
				}
			}
			return null;
		}

		public static String calculationElecQuantity(String electricity, int benchmark, Integer total) {
			try {
				if (null == total) {
					total = 0;
				}
				if (total > 0 && NumberUtils.toInt(electricity, 0) > 0) {
					if (total > 200) {
						total = 200;
					}
					float percent = (float) (total / benchmark) / 10;
					electricity = String.valueOf(NumberUtils.toFloat(electricity) - percent);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return electricity;
		}
	}

	/** 有电量的机型 */
	public enum ElecQuantityMcType {
		GT710("GT710"),GT700("GT700"),GT360("GT360"),JV03("JV03"),GT360S("GT360S"),GT360A("GT360A"),
		GT300N("GT300N"),GT300S("GT300S"),GT300SW("GT300SW"),GT03A("GT03A"),GT720("GT720"),
		GT300A("GT300A"),GT03C("GT03C"),GT300("GT300"),JI03("JI03"),GT730("GT730"),GT740("GT740"),
		GT300SUPER("GT300+")/*此处因为考虑到机型已经定制好,结合枚举特性,所以没有使用GT300+*/,GT300L("GT300L"),GT350("GT350"),
		GT370("GT370"),TR370("TR370"),BD309W("BD309W"),
		TR03A("TR03A"),TR03C("TR03C"),TR03("TR03")
		;
    	private String value;
		private ElecQuantityMcType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
		public static boolean ifExis(String value){
		for (ElecQuantityMcType nvt : ElecQuantityMcType.values()) {
			if(nvt.getValue().equals(value)){
				return true;
			}
		}
			return false;
		}
		
		public static String getElecQuantity(String mcType,HBProtocol hbProtocol){
			String eq = "";//不具备电压属性
			if(StringUtils.isNotBlank(mcType)){
				if(ElecQuantityMcType.GT700.getValue().equals(mcType)
						|| ElecQuantityMcType.GT710.getValue().equals(mcType)
						|| ElecQuantityMcType.GT740.getValue().equals(mcType)){
					eq = getElecQuantityForGT7(null !=hbProtocol?hbProtocol.getBatteryPowerVal():"");
				}else if(ElecQuantityMcType.GT360.getValue().equals(mcType)
							|| ElecQuantityMcType.GT370.getValue().equals(mcType)
						 	|| ElecQuantityMcType.TR370.getValue().equals(mcType)
						 	|| ElecQuantityMcType.GT360S.getValue().equals(mcType)
						 	|| ElecQuantityMcType.GT360A.getValue().equals(mcType)){
					eq = getElecQuantityForGT360(null != hbProtocol?hbProtocol.getBatteryPowerVal():"");
				}else if(ElecQuantityMcType.GT300N.getValue().equals(mcType)
						|| ElecQuantityMcType.GT300S.getValue().equals(mcType)
						|| ElecQuantityMcType.GT300SW.getValue().equals(mcType)
						|| ElecQuantityMcType.GT300L.getValue().equals(mcType)){
					eq = getElecQuantityForBatteryPowerVal(null !=hbProtocol?hbProtocol.getBatteryPowerVal():"");
				}else if(ElecQuantityMcType.GT300A.getValue().equals(mcType)
						|| ElecQuantityMcType.GT03C.getValue().equals(mcType)
						|| ElecQuantityMcType.GT300.getValue().equals(mcType)
						|| ElecQuantityMcType.GT300SUPER.getValue().equals(mcType)
						|| ElecQuantityMcType.GT350.getValue().equals(mcType)
						|| ElecQuantityMcType.JI03.getValue().equals(mcType)
						|| ElecQuantityMcType.JV03.getValue().equals(mcType)
						|| ElecQuantityMcType.GT03A.getValue().equals(mcType)
						|| ElecQuantityMcType.TR03A.getValue().equals(mcType)
						|| ElecQuantityMcType.TR03C.getValue().equals(mcType)
						|| ElecQuantityMcType.TR03.getValue().equals(mcType)
						|| ElecQuantityMcType.BD309W.getValue().equals(mcType)
						){
					eq = getElecQuantityForPowerLevel(hbProtocol!=null?hbProtocol.getPowerLevel():"");
				}else if (ElecQuantityMcType.GT720.getValue().equals(mcType)){
					eq = getElecQuantityForGT720(hbProtocol!=null?hbProtocol.getBatteryPowerVal():"");
				}else if (ElecQuantityMcType.GT730.getValue().equals(mcType)){
					eq = getElecQuantityForGT730(hbProtocol!=null?hbProtocol.getPowerLevel():"");
				}
			}
			Integer benchmark = SubdivideElecQuantityMcType.checkType(mcType);
			if(null != benchmark){
				eq = SubdivideElecQuantityMcType.calculationElecQuantity(eq, benchmark,null != hbProtocol? hbProtocol.getLoginTimes():null);
			}
			return eq;
		}
		
		public static String getElecQuantityForGT7(String batteryPowerVal){
			String eq = "100";
			if(StringUtils.isBlank(batteryPowerVal)){
				return eq;
			}
			try {
			double pv = Double.valueOf(batteryPowerVal);
			if(pv < 2.3){
				eq = "0";
			}else if(2.3 <= pv && pv < 2.4){
				eq = "10";
			}else if(2.4 <= pv && pv < 2.55){
				eq = "20";
			}else if(2.55 <= pv && pv < 2.6){
				eq = "30";
			}else if(2.6 <= pv && pv < 2.66){
				eq = "40";
			}else if(2.66 <= pv && pv < 2.7){
				eq = "50";
			}else if(2.7 <= pv && pv < 2.77){
				eq = "60";
			}else if(2.77 <= pv && pv < 2.82){
				eq = "70";
			}else if(2.82 <= pv && pv < 2.88){
				eq = "80";
			}else if(2.88 <= pv && pv < 2.93){
				eq = "90";
			}else if(pv >= 2.93){
				eq = "100";
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
			return eq;
		}
		
		public static String getElecQuantityForGT730(String batteryPowerVal){
			String eq = "100";
			if(StringUtils.isBlank(batteryPowerVal)){
				return eq;
			}
			try {
			double pv = Double.valueOf(batteryPowerVal);
			if(pv < 3.59){
				eq = "0";
			}else if(3.59 <= pv && pv < 3.63){
				eq = "5";
			}else if(3.63 <= pv && pv < 3.67){
				eq = "20";
			}else if(3.67 <= pv && pv < 3.7){
				eq = "30";
			}else if(3.7 <= pv && pv < 3.74){
				eq = "40";
			}else if(3.74 <= pv && pv < 3.79){
				eq = "50";
			}else if(3.74 <= pv && pv < 3.85){
				eq = "60";
			}else if(3.85 <= pv && pv < 3.92){
				eq = "70";
			}else if(3.92 <= pv && pv < 4){
				eq = "80";
			}else if(4 <= pv && pv < 4.05){
				eq = "90";
			}else if(pv >= 4.05){
				eq = "100";
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
			return eq;
		}
		
		public static String getElecQuantityForGT720(String batteryPowerVal){
			String eq = "100";
			if(StringUtils.isBlank(batteryPowerVal)){
				return eq;
			}
			try {
			double pv = Double.valueOf(batteryPowerVal);
			if(pv < 2.6){
				eq = "0";
			}else if(2.6 <= pv && pv < 2.7){
				eq = "10";
			}else if(2.7 <= pv && pv < 2.8){
				eq = "20";
			}else if(2.8 <= pv && pv < 2.9){
				eq = "30";
			}else if(2.9 <= pv && pv < 3){
				eq = "40";
			}else if(3 <= pv && pv < 3.1){
				eq = "50";
			}else if(3.1 <= pv && pv < 3.2){
				eq = "60";
			}else if(3.2 <= pv && pv < 3.3){
				eq = "70";
			}else if(3.3 <= pv && pv < 3.4){
				eq = "80";
			}else if(3.4 <= pv && pv < 3.5){
				eq = "90";
			}else if(pv >= 3.5){
				eq = "100";
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
			return eq;
		}
		
		public static String getElecQuantityForGT360(String batteryPowerVal){
			String eq = "100";
			if(StringUtils.isBlank(batteryPowerVal)){
				return eq;
			}
			try {
			double pv = Double.valueOf(batteryPowerVal);
			if(pv>4.08){
				eq = "100";
			}else if(pv> 3.90){
				eq = "80";
			}else if(pv>3.78){
				eq = "60";
			}else if(pv>3.70){
				eq = "40";
			}else if(pv>3.64){
				eq = "20";
			}else if(pv>3.59){
				eq = "5";
			}else if(pv>3.58){
				eq = "1";
			}else{
				eq = "0";
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
			return eq;
		}
	}

	/** 有电压的机型 */
	public enum PowerValForMcType {
		ET200("ET200");
		private String value;

		private PowerValForMcType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public static boolean ifExis(String value) {
			for (PowerValForMcType nvt : PowerValForMcType.values()) {
				if (nvt.getValue().equals(value)) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * 根据心跳包里的内电电压等级显示电量
	 * 
	 * @param batteryPowerVal
	 * @return
	 */
	public static String getElecQuantityForBatteryPowerVal(String batteryPowerVal) {
		String eq = "100";
		if (StringUtils.isBlank(batteryPowerVal)) {
			return eq;
		}
		try {
			double pv = Double.valueOf(batteryPowerVal);
			if (pv > 4.00) {
				eq = "100";
			} else if (pv > 3.84) {
				eq = "80";
			} else if (pv > 3.76) {
				eq = "60";
			} else if (pv > 3.70) {
				eq = "40";
			} else if (pv > 3.65) {
				eq = "20";
			} else if (pv > 3.60) {
				eq = "5";
			} else if (pv > 3.58) {
				eq = "1";
			} else {
				eq = "0";
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return eq;
	}

	/**
	 * 根据心跳包里的电压等级显示电量
	 * 
	 * @param batteryPowerVal
	 * @return
	 */
	public static String getElecQuantityForPowerLevel(String powerLevel) {
		String eq = "100";
		if (StringUtils.isBlank(powerLevel)) {
			return eq;
		}
		try {
			int pv = Integer.valueOf(powerLevel);
			if (pv < 1) {
				eq = "0";
			} else if (pv == 1) {
				eq = "1";
			} else if (pv == 2) {
				eq = "5";
			} else if (pv == 3) {
				eq = "15";
			} else if (pv == 4) {
				eq = "40";
			} else if (pv == 5) {
				eq = "70";
			} else if (pv >= 6) {
				eq = "100";
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return eq;
	}

	/** 没有行程的机型 */
	public enum NoTripMcType {
		GT300A("GT300A"), /* GT03C("GT03C"), */GT300("GT300"), GT3000("GT300+"), GT710("GT710"), GT700("GT700"), GT300L("GT300L"), GT300S(
				"GT300S"), GT300SW("GT300SW"), GT360("GT360"), GT300N("GT300N"), JV03("JV03"), GT350("GT350"), BD309W("BD309W"), GP100(
				"GP100"), GS503("GS503"), GT360A("GT360A"), GT360S("GT360S"), GT370("GT370"), GT500S("GT500S"), GT720S("GT720S"), GT730(
				"GT730"), GT740("GT740"), GW100("GW100"), GW110("GW110"), TR370("TR370");
		private NoTripMcType(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public static boolean ifExis(String value) {
			for (NoTripMcType ntmt : NoTripMcType.values()) {
				if (ntmt.getValue().equals(value)) {
					return true;
				}
			}
			return false;
		}
	}

	/** 支持寻车功能的机型 */
	public static final String FIND_CAR_FLAG = "V20";
}
