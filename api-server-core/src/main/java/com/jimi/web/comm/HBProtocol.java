package com.jimi.web.comm;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 心跳包
 */
public class HBProtocol implements Serializable {
	private static final long serialVersionUID = 2589069397363196999L;

	private String deviceImei;
	
	private String deviceInfo; // 终端信息

	private String ext; // 预留扩展位

	private String AAE;

	private String gPSSignal;//// GSM信号强度等级

	private String powerLevel; // 电压等级

	private String seqNo;

	private String sessionId;

	private Date time; // 上报时间

	private String powerValue;  //外电电压
	
	private String batteryPowerVal;//内电电压
	
	private String sleep;   //休眠
	
	private String accStatus; // acc状态 0:OFF(关闭) 1:ON(打开)
	private String fortifyStatus; // 设防状态 设防 0 关闭; 1 打开
	private String oilEleStatus; // 油电状态 油电控制 0 接通油电，1 断开油电；默认值为：0

	private Integer loginTimes;   //登录次数。 超长待机设备计算电量使用
	
	public HBProtocol() {
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getAAE() {
		return AAE;
	}

	public void setAAE(String aAE) {
		AAE = aAE;
	}

	public String getgPSSignal() {
		return gPSSignal;
	}

	public void setgPSSignal(String gPSSignal) {
		this.gPSSignal = gPSSignal;
	}

	public String getPowerLevel() {
		return powerLevel;
	}

	public void setPowerLevel(String powerLevel) {
		this.powerLevel = powerLevel;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getAccStatus() {
		return accStatus;
	}

	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}

	public String getFortifyStatus() {
		return fortifyStatus;
	}

	public void setFortifyStatus(String fortifyStatus) {
		this.fortifyStatus = fortifyStatus;
	}

	public String getOilEleStatus() {
		return oilEleStatus;
	}

	public void setOilEleStatus(String oilEleStatus) {
		this.oilEleStatus = oilEleStatus;
	}

	public String getDeviceImei() {
		return deviceImei;
	}

	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}

	public String getPowerValue() {
		return powerValue;
	}

	public void setPowerValue(String powerValue) {
		this.powerValue = powerValue;
	}

	public String getBatteryPowerVal() {
		return batteryPowerVal;
	}

	public void setBatteryPowerVal(String batteryPowerVal) {
		this.batteryPowerVal = batteryPowerVal;
	}

	public String getSleep() {
		return sleep;
	}

	public void setSleep(String sleep) {
		this.sleep = sleep;
	}

	public Integer getLoginTimes() {
		return loginTimes;
	}

	public void setLoginTimes(Integer loginTimes) {
		this.loginTimes = loginTimes;
	}

}
