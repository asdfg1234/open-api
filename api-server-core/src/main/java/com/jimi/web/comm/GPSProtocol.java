package com.jimi.web.comm;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * GPS数据
 */
public class GPSProtocol implements Serializable {

	public enum PosType {

		GPS("卫星定位", "GPS"), LBS("基站定位", "LBS"), WIFI("WIFI定位", "WIFI"), BEACON("蓝牙定位", "BEACON");

		private String desc;

		private String value;

		private PosType(String desc, String value) {
			this.desc = desc;
			this.value = value;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public static String getDescByVlaue(String value) {
			String result = "";
			if (StringUtils.isNotBlank(value)) {
				for (PosType pt : PosType.values()) {
					if (pt.getValue().equals(value)) {
						result = pt.getDesc();
					}
				}
			}
			return result;
		}

	}

	private static final long serialVersionUID = 4130148357566996541L;

	private String acc; // ACC状态 ACC低为00 ， ACC高为01 acc 0 未通电, 1 通电

	private String deviceImei; // 设备Imei,定长15字节

	private String direction; // 移动的方位角度 0-360 -1代表未知 100.12

	private Date gateTime; // 网关收到数据的时间

	private String gpsInfo; // 卫星数

	private String gpsMode; // 上传模式: 0,实时; 1,补传;

	private String gpsSpeed;// 公里/小时

	private String locDesc;// 如果是蓝牙定位,缓存所携带的位置信息

	private Date gpsTime; // GPS定位的定位时间(GPS定位类型会上传速度)
	
	private Date otherPosTime; // 其他类型定位的定位时间(其他定位类型不会上传速度)

	private double latitude; // 纬度

	private double longitude; // 经度

	/**
	 * 数据上报模式 0定时上报 1定距上报 2拐点上传 3 ACC状态改变上传 4 从运动变静止状态补传最后一个有效定位点 5
	 * 网络断开重连后，上报最后一个有效定位点 6 星历更新强制上传GPS点 7 按键上传定位点 8 开机强制上传定位点 9
	 * 开机由LBS基站信息转换定位点
	 */
	private String posMethod;
	
	private String posType;   //定位类型  LBS,WIFI,GPS
	
	private String region; //所在省市

	public Date getOtherPosTime() {
		return otherPosTime;
	}

	public void setOtherPosTime(Date otherPosTime) {
		this.otherPosTime = otherPosTime;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public GPSProtocol() {
	}

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getLocDesc() {
		return locDesc;
	}

	public void setLocDesc(String locDesc) {
		this.locDesc = locDesc;
	}

	public String getDeviceImei() {
		return deviceImei;
	}

	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public Date getGateTime() {
		return gateTime;
	}

	public void setGateTime(Date gateTime) {
		this.gateTime = gateTime;
	}

	public String getGpsInfo() {
		return gpsInfo;
	}

	public void setGpsInfo(String gpsInfo) {
		this.gpsInfo = gpsInfo;
	}

	public String getGpsMode() {
		return gpsMode;
	}

	public void setGpsMode(String gpsMode) {
		this.gpsMode = gpsMode;
	}

	public String getGpsSpeed() {
		return gpsSpeed;
	}

	public void setGpsSpeed(String gpsSpeed) {
		this.gpsSpeed = gpsSpeed;
	}

	public Date getGpsTime() {
		return gpsTime;
	}

	public void setGpsTime(Date gpsTime) {
		this.gpsTime = gpsTime;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getPosMethod() {
		return posMethod;
	}

	public void setPosMethod(String posMethod) {
		this.posMethod = posMethod;
	}

	public String getPosType() {
		return posType;
	}

	public void setPosType(String posType) {
		this.posType = posType;
	}

}
