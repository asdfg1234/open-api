/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月13日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.web.comm;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @FileName DeviceInfo.java
 * @Description:
 *
 * @Date 2017年4月13日 下午2:24:26
 * @author li.shangzhi
 * @version 1.0
 */
public class DeviceLocationInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 设备imei号
	 */
	private String imei;

	/**
	 * 设备名称
	 */
	private String deviceName;

	/**
	 * 设备型号
	 */
	private String mcType;

	/**
	 * 机型使用范围
	 */
	private String mcTypeUseScope;

	/**
	 * 车辆图标
	 */
	private String icon;

	/**
	 * 状态0,离线; 1,在线
	 */
	private String status;

	/**
	 * 定位类型 卫星定位-GPS, 基站定位-LBS, WIFI定位-WIFI, 蓝牙定位-BEACON
	 */
	private String posType;

	/**
	 * 纬度
	 */
	private double lat;

	/**
	 * 经度
	 */
	private double lng;

	/**
	 * 心跳时间
	 */
	private Date hbTime;

	/**
	 * 设备静止时间
	 */
	private String idelTiem;

	/**
	 * acc状态 0:OFF(关闭) 1:ON(打开)
	 */
	private String accStatus;

	/**
	 * GSM信号强度等级
	 */
	private String gpsSignal;

	/**
	 * 电压等级
	 */
	private String powerLevel;

	/**
	 * 外电电压
	 */
	private String powerValue;

	/**
	 * 内电电压
	 */
	private String batteryPowerVal;

	/**
	 * 设防状态 设防 0 关闭; 1 打开
	 */
	private String fortifyStatus;

	/**
	 * 油电状态 油电控制 0 接通油电，1 断开油电；默认值为：0
	 */
	private String oilEleStatus;

	/**
	 * 速率，单位:km/h
	 */
	private String speed;

	/**
	 * 卫星数
	 */
	private String gpsNum;

	/**
	 * 上传模式: 0,实时; 1,补传
	 */
	private String gpsMode;

	/**
	 * GPS定位时间
	 */
	private Date gpsTime;

	/**
	 * 移动的方位角度 0-360 -1代表未知 100.12
	 */
	private String direction;

	private String tripFlag; // 是否有行程  1-有 0-无

	private String recordFlag;// 是否支持录音  1-支持 0-不支持

	private String activationFlag; // 是否激活 1-激活 0-未激活

	private String expireFlag; // 是否过期  1-过期 0-未过期

	private String electQuantity;// 电量

	private String surplusTime;// 电量使用剩余时间

	private String locDesc;// 如果是蓝牙定位,缓存所携带的位置信息

	private String isBind;// 是否被绑定 0:未绑定 1:已绑定

	private String isSetTime; // 是否需要设置时间 0：不需要要 1：要

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getMcType() {
		return mcType;
	}

	public void setMcType(String mcType) {
		this.mcType = mcType;
	}

	public String getMcTypeUseScope() {
		return mcTypeUseScope;
	}

	public void setMcTypeUseScope(String mcTypeUseScope) {
		this.mcTypeUseScope = mcTypeUseScope;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPosType() {
		return posType;
	}

	public void setPosType(String posType) {
		this.posType = posType;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public Date getHbTime() {
		return hbTime;
	}

	public void setHbTime(Date hbTime) {
		this.hbTime = hbTime;
	}

	public String getIdelTiem() {
		return idelTiem;
	}

	public void setIdelTiem(String idelTiem) {
		this.idelTiem = idelTiem;
	}

	public String getAccStatus() {
		return accStatus;
	}

	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}

	public String getGpsSignal() {
		return gpsSignal;
	}

	public void setGpsSignal(String gpsSignal) {
		this.gpsSignal = gpsSignal;
	}

	public String getPowerLevel() {
		return powerLevel;
	}

	public void setPowerLevel(String powerLevel) {
		this.powerLevel = powerLevel;
	}

	public String getPowerValue() {
		return powerValue;
	}

	public void setPowerValue(String powerValue) {
		this.powerValue = powerValue;
	}

	public String getBatteryPowerVal() {
		return batteryPowerVal;
	}

	public void setBatteryPowerVal(String batteryPowerVal) {
		this.batteryPowerVal = batteryPowerVal;
	}

	public String getFortifyStatus() {
		return fortifyStatus;
	}

	public void setFortifyStatus(String fortifyStatus) {
		this.fortifyStatus = fortifyStatus;
	}

	public String getOilEleStatus() {
		return oilEleStatus;
	}

	public void setOilEleStatus(String oilEleStatus) {
		this.oilEleStatus = oilEleStatus;
	}

	public String getSpeed() {
		return speed;
	}

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public String getGpsNum() {
		return gpsNum;
	}

	public void setGpsNum(String gpsNum) {
		this.gpsNum = gpsNum;
	}

	public String getGpsMode() {
		return gpsMode;
	}

	public void setGpsMode(String gpsMode) {
		this.gpsMode = gpsMode;
	}

	public Date getGpsTime() {
		return gpsTime;
	}

	public void setGpsTime(Date gpsTime) {
		this.gpsTime = gpsTime;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getTripFlag() {
		return tripFlag;
	}

	public void setTripFlag(String tripFlag) {
		this.tripFlag = tripFlag;
	}

	public String getRecordFlag() {
		return recordFlag;
	}

	public void setRecordFlag(String recordFlag) {
		this.recordFlag = recordFlag;
	}

	public String getActivationFlag() {
		return activationFlag;
	}

	public void setActivationFlag(String activationFlag) {
		this.activationFlag = activationFlag;
	}

	public String getExpireFlag() {
		return expireFlag;
	}

	public void setExpireFlag(String expireFlag) {
		this.expireFlag = expireFlag;
	}

	public String getElectQuantity() {
		return electQuantity;
	}

	public void setElectQuantity(String electQuantity) {
		this.electQuantity = electQuantity;
	}

	public String getSurplusTime() {
		return surplusTime;
	}

	public void setSurplusTime(String surplusTime) {
		this.surplusTime = surplusTime;
	}

	public String getLocDesc() {
		return locDesc;
	}

	public void setLocDesc(String locDesc) {
		this.locDesc = locDesc;
	}

	public String getIsBind() {
		return isBind;
	}

	public void setIsBind(String isBind) {
		this.isBind = isBind;
	}

	public String getIsSetTime() {
		return isSetTime;
	}

	public void setIsSetTime(String isSetTime) {
		this.isSetTime = isSetTime;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
