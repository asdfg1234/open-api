/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.mybatis.dialect;

/**
 * @FileName MysqlDialect.java
 * @Description: MySql分页处理类
 *
 * @Date 2017年4月25日 上午9:50:29
 * @author li.shangzhi
 * @version 1.0
 */
public class MysqlDialect extends Dialect {

	@Override
	public String getLimitString(String sql, int skipResults, int maxResults) {
		if (skipResults > 0) {
			return sql + " limit " + skipResults + "," + maxResults;
		}
		return sql + " limit " + maxResults;
	}

}
