/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月7日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.mybatis.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @FileName MybatisConfiguration.java
 * @Description: 途强数据库
 *
 * @Date 2017年4月7日 下午2:29:57
 * @author li.shangzhi
 * @version 1.0
 */
@Configuration
//扫描 Mapper 接口并容器管理
@MapperScan(basePackages = {"com.jimi.web.**.dao"}, sqlSessionFactoryRef = "slaveSqlSessionFactory")
@EnableTransactionManagement
public class SlaveConfiguration {

	@Autowired
	@Qualifier(value = "slaveDataSource")
	private DataSource slaveDataSource;

	@Bean(name = "slaveSqlSessionFactory")
	public SqlSessionFactory slaveSqlSessionFactory() throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(slaveDataSource);
		sqlSessionFactoryBean.setTypeAliasesPackage("com.jimi.web.*.*.dao");
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath*:com/jimi/web/**/dao/mapper/*Mapper.xml"));
		sqlSessionFactoryBean.getObject().getConfiguration().setMapUnderscoreToCamelCase(true);
		return sqlSessionFactoryBean.getObject();
	}

	/**
	 * 配置事务管理器
	 */
	@Bean(name = "slaveTransactionManager")
	public PlatformTransactionManager slaveTransactionManager() throws Exception {
		return new DataSourceTransactionManager(slaveDataSource);
	}
}
