/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.utils;

import org.apache.ibatis.session.RowBounds;

import com.jimi.dto.base.PageModel;

/**
 * @FileName RowBoundUtils.java
 * @Description: 分页工具
 *
 * @Date 2017年4月25日 下午5:18:24
 * @author li.shangzhi
 * @version 1.0
 */
public class RowBoundUtils {

	@SuppressWarnings("rawtypes")
	public static RowBounds rowBounds(PageModel pageModel) {
		if (pageModel == null) {
			pageModel = new PageModel();
		}
		RowBounds rowBounds = new RowBounds((pageModel.getPage() - 1) * pageModel.getLimit(), pageModel.getLimit());
		return rowBounds;
	}

}
