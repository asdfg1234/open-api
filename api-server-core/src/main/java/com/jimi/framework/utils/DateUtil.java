/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;

/**
 * @FileName DateUtil.java
 * @Description: 时间操作工具
 *
 * @Date 2017年4月25日 下午5:21:47
 * @author li.shangzhi
 * @version 1.0
 */
public class DateUtil {

	public static final String VIEW_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm";
	public static final String LONG_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	public String formatDate(java.util.Date date) {
		return formatDateByFormat(date, "yyyy-MM-dd");
	}

	public static String formatDateByFormat(java.util.Date date, String format) {
		String result = "";
		if (date != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				result = sdf.format(date);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	public static java.util.Date parseDate(java.sql.Date date) {
		return date;
	}

	public static java.sql.Date parseSqlDate(java.util.Date date) {
		if (date != null) {
			return new java.sql.Date(date.getTime());
		} else {
			return null;
		}
	}

	public static String format(java.util.Date date, String format) {
		String result = "";
		try {
			if (date != null) {
				java.text.DateFormat df = new java.text.SimpleDateFormat(format);
				result = df.format(date);
			}
		} catch (Exception e) {
		}
		return result;
	}

	public static String format(java.util.Date date) {
		return format(date, "yyyy/MM/dd");
	}

	public static String format1(java.util.Date date) {
		return format(date, "yyyy-MM-dd");
	}

	public static int getYear(java.util.Date date) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(date);
		return c.get(java.util.Calendar.YEAR);
	}

	public static int getMonth(java.util.Date date) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(date);
		return c.get(java.util.Calendar.MONTH) + 1;
	}

	public static int getDay(java.util.Date date) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(date);
		return c.get(java.util.Calendar.DAY_OF_MONTH);
	}

	public static int getHour(java.util.Date date) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(date);
		return c.get(java.util.Calendar.HOUR_OF_DAY);
	}

	public static int getMinute(java.util.Date date) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(date);
		return c.get(java.util.Calendar.MINUTE);
	}

	public static int getSecond(java.util.Date date) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(date);
		return c.get(java.util.Calendar.SECOND);
	}

	public static long getMillis(java.util.Date date) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(date);
		return c.getTimeInMillis();
	}

	public static int getWeek(java.util.Date date) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(date);
		int dayOfWeek = c.get(java.util.Calendar.DAY_OF_WEEK);
		dayOfWeek = dayOfWeek - 1;
		if (dayOfWeek == 0) {
			dayOfWeek = 7;
		}
		return dayOfWeek;
	}

	public static String getDate(java.util.Date date) {
		return format(date, "yyyy/MM/dd");
	}

	public static String getDate(java.util.Date date, String formatStr) {
		return format(date, formatStr);
	}

	public static String getTime(java.util.Date date) {
		return format(date, "HH:mm:ss");
	}

	public static String getDateTime(java.util.Date date) {
		return format(date, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 日期相加
	 * 
	 * @param date
	 *            Date
	 * @param day
	 *            int
	 * @return Date
	 */
	public static java.util.Date addDate(java.util.Date date, int day) {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) + ((long) day) * 24 * 3600 * 1000);
		return c.getTime();
	}

	/**
	 * 日期相减
	 * 
	 * @param date
	 *            Date
	 * @param date1
	 *            Date
	 * @return int
	 */
	public static int diffDate(java.util.Date date, java.util.Date date1) {
		return (int) ((getMillis(date) - getMillis(date1)) / (24 * 3600 * 1000));
	}

	/**
	 * 日期相减(返回秒值)
	 * 
	 * @param date
	 *            Date
	 * @param date1
	 *            Date
	 * @return int
	 * @author
	 */
	public static Long diffDateTime(java.util.Date date, java.util.Date date1) {
		return (Long) ((getMillis(date) - getMillis(date1)) / 1000);
	}

	public static Date getDate(String date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			return sdf.parse(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static Date getDate1(String date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sdf.parse(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static Date getMaxTimeByStringDate(String date) {
		try {
			String maxTime = date + " 23:59:59";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sdf.parse(maxTime);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 获得当前时间
	 * 
	 * @return
	 */
	public static Date getCurrentDateTime() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String result = DateUtil.getDateTime(date);
		try {
			return sdf.parse(result);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static String getCurrentDateTimeToStr() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(getCurrentDateTime());
	}

	public static Long getWmsupdateDateTime() {
		Calendar cl = Calendar.getInstance();

		return cl.getTimeInMillis();
	}

	/** logger日志对象 */
	protected final static Logger logger = Logger.getLogger(DateUtil.class);

	/**
	 * 私有构造器防止new
	 */
	private DateUtil() {

	}

	/**
	 * 得到当前年月,如2009-02
	 * 
	 * @return String
	 */
	public static String getCurYearMonth() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 得到当前年,如2009
	 * 
	 * @return String
	 */
	public static int getCurYear() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * 得到当前年,如2009
	 * 
	 * @return String
	 */
	public static int getCurDay() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 得到当月份,如9
	 * 
	 * @return String
	 */
	public static int getCurMonth() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.MONTH) + 1;
	}

	/**
	 * 得到当前月的第一天，yyyy-MM-dd格式
	 *
	 * @return String
	 */
	public static String getFirstDayOfCurMonth() {
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.DATE, 1);
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 得到当前月的最后一天，yyyy-MM-dd格式
	 *
	 * @return String
	 */
	public static String getLastDayOfCurMonth() {
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.DATE, 1);
		calendar.roll(Calendar.DATE, -1);
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 得到上个月的第一天，yyyy-MM-dd格式
	 *
	 * @return String
	 */
	public static String getFirstDayOfLastMonth() {
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		calendar.set(Calendar.DATE, 1);
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 去年同期这个月的最后一天，yyyy-MM-dd格式
	 *
	 * @return String
	 */
	public static String getTongQiLastDate() {
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
		calendar.set(Calendar.DATE, 1);
		calendar.roll(Calendar.DATE, -1);
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 去年同期这个月的第一天，yyyy-MM-dd格式
	 *
	 * @return String
	 */
	public static String getTongQiFirstDate() {
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
		calendar.set(Calendar.DATE, 1);
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 得到上个月的最后一天，yyyy-MM-dd格式
	 *
	 * @return String
	 */
	public static String getLastDayOfLastMonth() {
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		calendar.set(Calendar.DATE, 1);
		calendar.roll(Calendar.DATE, -1);
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 得到当前年月日
	 *
	 * @return String
	 */
	public static String getCurrentDay(String pattern) {
		SimpleDateFormat simpleFormate = new SimpleDateFormat(pattern);
		return simpleFormate.format(new Date());
	}

	/**
	 * 获得当前时间
	 * 
	 * @param pattern
	 * @return
	 */
	public static Date getCurrentDate(String pattern) {
		Date date = new Date();
		String format = "yyyy-MM-dd";
		if (StringUtils.isNotBlank(pattern)) {
			format = pattern;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String result = DateUtil.getDateTime(date);
		try {
			return sdf.parse(result);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取昨天日期
	 * 
	 * @param pattern
	 * @return
	 */
	public static String getYesterDay(String pattern) {
		SimpleDateFormat simpleFormate = new SimpleDateFormat(pattern);
		Calendar ca = Calendar.getInstance();
		ca.setTime(new Date());
		ca.add(Calendar.DAY_OF_YEAR, -1);
		return simpleFormate.format(ca.getTime());
	}
	
	/**
	 * 获取明天日期
	 * 
	 * @param pattern
	 * @return
	 */
	public static String getTomorrowDay(String pattern) {
		SimpleDateFormat simpleFormate = new SimpleDateFormat(pattern);
		Calendar ca = Calendar.getInstance();
		ca.setTime(new Date());
		ca.add(Calendar.DAY_OF_YEAR, 1);
		return simpleFormate.format(ca.getTime());
	}

	/**
	 * 得到当前月,如02,12
	 *
	 * @return String
	 */
	public static String getMonth() {
		Calendar calendar = Calendar.getInstance();
		String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		if (month.length() == 1) {
			month = "0" + month;
		}
		return month;
	}

	/**
	 * 格式化日期为指定格式
	 *
	 * @param date
	 *            Date 需要格式化的日期
	 * @param s
	 *            String 目标格式字符串
	 * @return String 格式化的日期
	 */
	public static String formatDateTime(Date date, String s) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat(s);
		try {
			return simpledateformat.format(date);
		} catch (Exception ex) {
			logger.error("格式化日期为指定格式时发生异常", ex);
		}
		return null;
	}

	/**
	 * 解析日期时间字符串，得到Date对象
	 *
	 * @param input
	 *            日期时间字符串
	 * @param pattern
	 *            格式
	 * @return
	 */
	public static Date parseDate(String input, String pattern) {
		try {
			SimpleDateFormat simpledateformat = new SimpleDateFormat(pattern);
			return simpledateformat.parse(input);
		} catch (ParseException ex) {
			logger.error("解析日期时间字符串时发生异常", ex);
		}
		return null;
	}

	/**
	 * Timestamp 转换成 String类型
	 * 
	 * @param timestamp
	 * @param pattern
	 * @return
	 */
	public static String timestampToString(Timestamp timestamp, String pattern) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat(pattern);
		return simpledateformat.format(timestamp);
	}

	/**
	 * String 转换成 Timestamp类型
	 * 
	 * @param dateTime
	 * @param pattern
	 * @return
	 * @throws Exception
	 */
	public static Timestamp stringToTimestamp(String dateTime, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = df.parse(dateTime);
		} catch (ParseException e) {
			logger.error("String 转换成 Timestamp类型出现异常", e);
		}
		Timestamp ts = new Timestamp(date.getTime());
		return ts;
	}

	/**
	 * 获取某天的前后几天的日期
	 * 
	 * @param dateTime
	 * @return
	 */
	public static String getDayByDateTime(String dateTime, int next) {
		String result = "";
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date ldate = df.parse(dateTime);
			result = df.format(new Date(ldate.getTime() + next * 24 * 60 * 60 * 1000)).toString();
		} catch (ParseException e) {
			logger.error("获取某天的前一天时发生异常", e);
		}
		return result;
	}

	/**
	 * 获取某天的同期日期
	 * 
	 * @param dateTime
	 * @return
	 */
	public static String getTongQiDayByDateTime(String dateTime) {
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
		Calendar ca = Calendar.getInstance();
		try {
			ca.setTime(simpleFormate.parse(dateTime));
			ca.add(Calendar.YEAR, -1);
		} catch (ParseException e) {
			logger.error("获取某天的前一天时发生异常", e);
		}
		return simpleFormate.format(ca.getTime());
	}

	/**
	 * 获取今天开始时间
	 * 
	 * @return
	 */
	public static String getToDayStartTime() {
		return getCurrentDay("yyyy-MM-dd") + " 00:00:00";
	}

	/**
	 * 获取今天结束时间
	 * 
	 * @return
	 */
	public static String getToDayEndTime() {
		return getCurrentDay("yyyy-MM-dd") + " 23:59:59";
	}

	/**
	 * 获取昨天开始时间
	 * 
	 * @return
	 */
	public static String getYesterDayStartTime() {
		return getYesterDay("yyyy-MM-dd") + " 00:00:00";
	}

	/**
	 * 获取昨天结束时间
	 * 
	 * @return
	 */
	public static String getYesterDayEndTime() {
		return getYesterDay("yyyy-MM-dd") + " 23:59:59";
	}
	
	/**
	 * 获取明天结束时间
	 * 
	 * @return
	 */
	public static String getTomorrowDayEndTime() {
		return getTomorrowDay("yyyy-MM-dd") + " 23:59:59";
	}
	
	/**
	 * 获取明天开始时间
	 * 
	 * @return
	 */
	public static String getTomorrowDayStartTime() {
		return getTomorrowDay("yyyy-MM-dd") + " 00:00:00";
	}

	/**
	 * 获取某月有多少天
	 * 
	 * @param month
	 *            月份
	 * @return
	 */
	public static int getDayCountByMonth(int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, month - 1);
		return cal.getActualMaximum(Calendar.DATE);
	}

	/**
	 * 根据指定时间获取年份
	 * 
	 * @return 如:2010
	 */
	public static int getYearByDT(String dateTime, String pattern) {
		SimpleDateFormat simpleFormate = new SimpleDateFormat(pattern);
		Calendar ca = Calendar.getInstance();
		try {
			ca.setTime(simpleFormate.parse(dateTime));
		} catch (ParseException e) {
			logger.error("根据指定时间获取年份时发生异常", e);
		}
		return ca.get(Calendar.YEAR);
	}

	/**
	 * 根据指定时间获取月份
	 * 
	 * @return 如:05
	 */
	public static int getMonthByDT(String dateTime, String pattern) {
		SimpleDateFormat simpleFormate = new SimpleDateFormat(pattern);
		Calendar ca = Calendar.getInstance();
		try {
			ca.setTime(simpleFormate.parse(dateTime));
		} catch (ParseException e) {
			logger.error("根据指定时间获取月份时发生异常", e);
		}
		return ca.get(Calendar.MONTH) + 1;
	}

	/**
	 * 根据指定时间获取天数
	 * 
	 * @return 如:05
	 */
	public static int getDayByDT(String dateTime, String pattern) {
		SimpleDateFormat simpleFormate = new SimpleDateFormat(pattern);
		Calendar ca = Calendar.getInstance();
		try {
			ca.setTime(simpleFormate.parse(dateTime));
		} catch (ParseException e) {
			logger.error("根据指定时间获取月份时发生异常", e);
		}
		return ca.get(Calendar.DATE);
	}

	/**
	 * 根据时间获取上一个月
	 * 
	 * @param dataTime
	 * @return
	 */
	public static String getLastMonthByDT(String dateTime, String pattern) {
		String strYear = String.valueOf(getYearByDT(dateTime, pattern));
		String strMonth = changeLen(String.valueOf(getMonthByDT(dateTime, pattern) - 1));
		String strDay = changeLen(String.valueOf(getDayByDT(dateTime, pattern)));
		return strYear + "-" + strMonth + "-" + strDay;
	}

	/**
	 * 根据时间获取去年同一月
	 * 
	 * @param dataTime
	 * @return
	 */
	public static String getLastYearByDT(String dateTime, String pattern) {
		String strYear = String.valueOf(getYearByDT(dateTime, pattern) - 1);
		String strMonth = changeLen(String.valueOf(getMonthByDT(dateTime, pattern)));
		String strDay = changeLen(String.valueOf(getDayByDT(dateTime, pattern)));
		return strYear + "-" + strMonth + "-" + strDay;
	}

	/**
	 * 修改1-9数字位数,如1转换成01
	 * 
	 * @param value
	 * @return
	 */
	public static String changeLen(String value) {
		if (value.length() == 1) {
			value = "0" + value;
		}
		return value;
	}

	/**
	 * 得到当前年月,如200902
	 * 
	 * @return String
	 */
	public static String getCurYearMonthYYYYMM() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyyMM");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 得到当前年的上一月,如当前月为200902，则返回200901
	 * 
	 * @return String
	 */
	public static String getLastYearMonthYYYYMM() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyyMM");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 得到当前年的上一年,如当前年为2009，则返回2008
	 * 
	 * @return String
	 */
	public static String getLastYearYYYY(String year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
		SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy");
		return simpleFormate.format(calendar.getTime()).trim();
	}

	/**
	 * 获得给定日期的月份的最后一天
	 * 
	 * @param date
	 *            指定的日期
	 * @return 指定日期月份的最后一天
	 */
	public static int getLastDayOfTheMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * This method generates a string representation of a date/time in the
	 * format you specify on input
	 * 
	 * @param aMask
	 *            the date pattern the string is in
	 * @param strDate
	 *            a string representation of a date
	 * @return a converted Date object
	 * @see java.text.SimpleDateFormat
	 * @throws ParseException
	 *             when String doesn't match the expected format
	 */
	public static Date convertStringToDate(String aMask, String strDate) throws ParseException {
		SimpleDateFormat df;
		Date date;
		df = new SimpleDateFormat(aMask);

		try {
			date = df.parse(strDate);
		} catch (ParseException pe) {
			// log.error("ParseException: " + pe);
			throw new ParseException(pe.getMessage(), pe.getErrorOffset());
		}

		return (date);
	}

	/**
	 * 给指定日期加几天
	 * 
	 * @param date
	 *            指定的日期
	 * @param numDays
	 *            需要往后加的天数
	 * @return 加好后的日期
	 */
	public static Date addDaysToDate(Date date, int numDays) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, numDays);
		return c.getTime();
	}

	/**
	 * 给指定日期加几个月
	 * 
	 * @param date
	 *            指定的日期
	 * @param numMonths
	 *            需要往后加的月数
	 * @return 加好后的日期
	 */
	public static Date addMonthsToDate(Date date, int numMonths) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, numMonths);
		return c.getTime();
	}

	/**
	 * 将一个指定的日期格式化成指定的格式
	 * 
	 * @param date
	 *            指定的日期
	 * @param pattern
	 *            指定的格式
	 * @return 格式化好后的日期字符串
	 */
	public static String formatDate(Date date, String pattern) {
		return DateFormatUtils.format(date, pattern);
	}

	public static Date formatDateToStartDateTime(String strDate) {
		java.util.Date date = null;
		try {
			date = convertStringToDate(LONG_DATE_TIME_PATTERN, strDate + " 00:00:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static Date formatDateToEndDateTime(String strDate) {
		java.util.Date date = null;
		try {
			date = convertStringToDate(LONG_DATE_TIME_PATTERN, strDate + " 23:59:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * This method generates a string representation of a date based on the
	 * System Property 'dateFormat' in the format you specify on input
	 * 
	 * @param aDate
	 *            A date to convert
	 * @return a string representation of the date
	 */
	public static String convertDateToString(Date aDate) {
		return getDateTime(getDatePattern(), aDate);
	}

	/**
	 * 日期转为数字
	 */
	public static String convertDateToString(String aMask, Date aDate) {
		SimpleDateFormat df = null;
		String returnValue = "";

		if (aDate != null) {
			df = new SimpleDateFormat(aMask);
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * This method generates a string representation of a date's date/time in
	 * the format you specify on input
	 * 
	 * @param aMask
	 *            the date pattern the string is in
	 * @param aDate
	 *            a date object
	 * @return a formatted string representation of the date
	 * 
	 * @see java.text.SimpleDateFormat
	 */
	public static String getDateTime(String aMask, Date aDate) {
		SimpleDateFormat df = null;
		String returnValue = "";

		if (aDate != null) {
			df = new SimpleDateFormat(aMask);
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * Return default datePattern (MM/dd/yyyy)
	 * 
	 * @return a string representing the date pattern on the UI
	 */
	public static String getDatePattern() {
		return "yyyy-MM-dd";
	}

	public static int getMaxDaybyYearAndMonth(int year, int month) {
		int[] days = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (2 == month && 0 == (year % 4) && (0 != (year % 100) || 0 == (year % 400))) {
			days[1] = 29;
		}
		return (days[month - 1]);
	}

	/**
	 * 获得显示格式时间 到分钟
	 * 
	 * @param date
	 * @author yan.b
	 * @return
	 */
	public static String viewformat(Date date) {
		return convertDateToString(VIEW_DATE_TIME_PATTERN, date);
	}

	/**
	 * 计算两个日期之间的天数
	 * 
	 * @param start
	 * @param end
	 * @author yan.b
	 * @return
	 */
	public static int calculateDaysBetween(Date start, Date end) {
		Calendar aCalendar = Calendar.getInstance();
		// 里面可以直接插入date类型
		aCalendar.setTime(start);
		// 计算此日期是一年中的哪一天
		int day1 = aCalendar.get(Calendar.DAY_OF_YEAR);
		aCalendar.setTime(end);
		int day2 = aCalendar.get(Calendar.DAY_OF_YEAR);
		// 求出两日期相隔天数
		int days = day2 - day1;
		return days;
	}
	
	/**
	 * @Title: getIntervalDays
	 * @Description: 获取指定时间与当前服务器时间差
	 * @param date
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月14日 下午6:18:01
	 */
	public static int getIntervalDays(Date date) {
		long range = 24 * 60 * 60 * 1000;
		long ei = System.currentTimeMillis() - date.getTime();
		return (int) (ei / range);
	}

	/**
	 * @Title: getIntervalDays
	 * @Description: 获取开始与结束时间差
	 * @param begin
	 * @param end
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月14日 下午6:18:47
	 */
	public static int getIntervalDays(Date begin, Date end) {
		long range = 24 * 60 * 60 * 1000;
		long ei = end.getTime() - begin.getTime();
		return (int) (ei / range);
	}
	
	/**
	 * @Title: getResidueDay
	 * @Description: 获取当天剩余时间
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月21日 下午4:01:49
	 */
	public static long getResidueDay() {
		Calendar curDate = Calendar.getInstance();
		Calendar tommorowDate = new GregorianCalendar(curDate.get(Calendar.YEAR), curDate.get(Calendar.MONTH),
				curDate.get(Calendar.DATE) + 1, 0, 0, 0);
		return tommorowDate.getTimeInMillis() - curDate.getTimeInMillis();
	}

	/**
	 * Test
	 * 
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) {
		try {
			System.out.println("当前年月===" + getCurYearMonth());
			System.out.println("当前年份===" + getCurYear());
			System.out.println("当月第一天===" + getFirstDayOfCurMonth());
			System.out.println("当月最后一天===" + getLastDayOfCurMonth());
			System.out.println("上月第一天===" + getFirstDayOfLastMonth());
			System.out.println("上月最后一天===" + getLastDayOfLastMonth());
			System.out.println("去年同期第一天===" + getTongQiFirstDate());
			System.out.println("去年同期最后一天===" + getTongQiLastDate());
			System.out.println("当天yyyy-MM-dd===" + getCurrentDay("yyyy-MM-dd"));
			System.out.println("当月===" + getMonth());
			System.out.println("今天开始时间===" + getToDayStartTime());
			System.out.println("今天结束时间===" + getToDayEndTime());
			System.out.println("昨天开始时间===" + getYesterDayStartTime());
			System.out.println("昨天结束时间===" + getYesterDayEndTime());
			System.out.println("某月有多少天===" + getDayCountByMonth(1));
			System.out.println("某天前一天===" + getDayByDateTime("2010-08-07", 2));
			System.out.println("某天前一天===" + getTongQiDayByDateTime("2010-08-07"));
			stringToTimestamp("2010-08-13", "yyyy-MM-dd");
			System.out.println("2009-01-11获取Year===" + getYearByDT("2009-01-11", "yyyy-MM-dd"));
			System.out.println("2009-01-11获取Month===" + getMonthByDT("2009-01-11", "yyyy-MM-dd"));
			System.out.println("2009-01-11获取Day===" + getDayByDT("2009-01-11", "yyyy-MM-dd"));
			System.out.println("1999-05-12的上一个月===" + getLastMonthByDT("1999-05-12", "yyyy-MM-dd"));
			System.out.println("1999-05-12的去年本期===" + getLastYearByDT("1999-05-12", "yyyy-MM-dd"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			System.out.println(getWeek(getDate("2008-06-29")));
			Date date = DateUtil.getCurrentDateTime();
			System.out.println(date);

			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.MONTH, 3);

			System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxx===" + convertDateToString(c.getTime()));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
