/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月12日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @FileName MD5Utils.java
 * @Description:
 *
 * @Date 2017年4月12日 下午6:11:08
 * @author li.shangzhi
 * @version 1.0
 */
public class MD5Utils {

	public static  String getMD5ofStr(String inbuf) {
		return getMD5ofBytes(inbuf.getBytes());
	}
	
	public static String getMD5ofBytes(byte[] inbuf) {
		return DigestUtils.md5Hex(inbuf);
	}

}
