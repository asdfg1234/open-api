package com.jimi.framework.logger.support;

import com.jimi.framework.utils.UUIDGenerator;
import org.apache.commons.lang.StringUtils;

/**
 * 线程日志跟踪类(单例)
 * @author jimi
 */
public class LogTracer {
    private static final LogTracer logTracer = new LogTracer();
    private ThreadLocal<String> threadLocal = new ThreadLocal<String>();

    private LogTracer() {}

    public static LogTracer getInstance(){
        return logTracer;
    }

    public void removeTraceId() {
        threadLocal.remove();
    }

    public String getTraceId() {
    	String strTraceId = threadLocal.get();
    	if(StringUtils.isBlank(strTraceId)){
    		setTraceId();
    	}
    	return threadLocal.get();
    }
    
    private void setTraceId(){
    	threadLocal.set(UUIDGenerator.get32LowCaseUUID());
    }
}