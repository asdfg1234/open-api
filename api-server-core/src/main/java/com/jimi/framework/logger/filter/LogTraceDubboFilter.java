package com.jimi.framework.logger.filter;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import com.jimi.framework.logger.support.LogTracer;

@Activate(group = { Constants.PROVIDER })
public class LogTraceDubboFilter implements Filter {

	@Override
	public Result invoke(Invoker<?> invoker, Invocation invocation)
			throws RpcException {
		RpcContext context = RpcContext.getContext();
		boolean isConsumerSide = context.isConsumerSide();
		try{
			Result result = invoker.invoke(invocation);
			return result;
		}catch(RpcException e){
			throw e;
		}finally{
			// 不是消费者
			if(!isConsumerSide){
				LogTracer.getInstance().removeTraceId();
			}
		}
	}
}
