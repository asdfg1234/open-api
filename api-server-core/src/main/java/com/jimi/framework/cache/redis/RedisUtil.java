/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.cache.redis;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import com.jimi.framework.context.SpringContextHolder;

/**
 * @FileName RedisUtil.java
 * @Description: redis标准操作工具
 *
 * @Date 2017年4月10日 上午9:26:07
 * @author li.shangzhi
 * @version 1.0
 */
public class RedisUtil {

	public enum ModuleType {

		OAUTH("oauth"), DEVICES("devices"), USER("user"), LBS("lbs");

		private String name;

		ModuleType(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	private static final Logger logger = LoggerFactory.getLogger(RedisUtil.class);

	private static RedisTemplate<String, String> redisTemplate = SpringContextHolder.getBean("redisTemplate");

	private static ValueOperations<String, String> opsForValue() {
		redisTemplate.setValueSerializer(new JsonRedisSerializable<Object>());
		return redisTemplate.opsForValue();
	}

	public static String getKey(ModuleType moduleType, String key) {
		String redisKey = "jimiOpenApi." + moduleType.getName() + ":" + key;
		logger.info("操作的redis Key为：{}", redisKey);
		return redisKey;
	}
	
	// 满足个人app校验，临时写法
	public static String getAppString(ModuleType moduleType, String key) {
		String redisKey = "SmartHome." + moduleType.getName() + ":" + key;
		logger.info("操作的redis Key为：{}", redisKey);
		return opsForValue().get(redisKey);
	}

	public static String getString(ModuleType moduleType, String key) {
		return opsForValue().get(getKey(moduleType, key));
	}

	public static int getInt(ModuleType moduleType, String key) {
		int result = 0;
		String val = opsForValue().get(getKey(moduleType, key));
		if (StringUtils.isNotBlank(val)) {
			try {
				result = Integer.valueOf(val);
			} catch (Exception ex) {
				logger.error("[RedisUtil]转换数据[String->int]时发生异常,val=" + val, ex);
			}
		}
		return result;
	}

	public static void set(ModuleType moduleType, String key, String value) {
		long expire = redisTemplate.getExpire(getKey(moduleType, key));
		if (expire > -1) {
			if (expire == 0) {
				expire = 1;
			}
			opsForValue().set(getKey(moduleType, key), value, expire, TimeUnit.SECONDS);
		} else {
			opsForValue().set(getKey(moduleType, key), value);
		}
	}

	public static void set(ModuleType moduleType, String key, String value, int timeout, TimeUnit unit) {
		opsForValue().set(getKey(moduleType, key), value, timeout, unit);
	}

	public static void set(ModuleType moduleType, String key, int value) {
		long expire = redisTemplate.getExpire(getKey(moduleType, key));
		if (expire > -1) {
			if (expire == 0) {
				expire = 1;
			}
			opsForValue().set(getKey(moduleType, key), String.valueOf(value), expire, TimeUnit.SECONDS);
		} else {
			opsForValue().set(getKey(moduleType, key), String.valueOf(value));
		}
	}

	public static void set(ModuleType moduleType, String key, int value, int timeout, TimeUnit unit) {
		opsForValue().set(getKey(moduleType, key), String.valueOf(value), timeout, unit);
	}

	public static void delete(ModuleType moduleType, String key) {
		redisTemplate.delete(getKey(moduleType, key));
	}

	public static void expire(ModuleType moduleType, String key, int timeout, TimeUnit unit) {
		redisTemplate.expire(getKey(moduleType, key), timeout, unit);
	}
}
