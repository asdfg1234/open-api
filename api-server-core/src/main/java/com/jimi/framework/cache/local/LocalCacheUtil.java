/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.cache.local;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.alibaba.fastjson.JSON;
import com.google.common.cache.CacheStats;

/**
 * @FileName LocalCacheUtil.java
 * @Description: 本地缓存通用接口
 *
 * @Date 2017年4月10日 上午9:32:00
 * @author li.shangzhi
 * @version 1.0
 */
public class LocalCacheUtil {

	private static LocalCacheUtil localCacheUtil;

	private LocalCacheUtil() {
	}

	public static LocalCacheUtil getInstance() {
		if (localCacheUtil == null) {
			localCacheUtil = new LocalCacheUtil();
		}
		return localCacheUtil;
	}

	/**
	 * @Title: getLocalCache
	 * @Description: 根据key获取本地缓存值——对象类型
	 * @param key
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:42:42
	 */
	public Object getLocalCache(String key) {
		Object[] obj = (Object[]) LocalCache.getInstance().getIfPresent(key);
		if (obj != null && obj.length > 0) {
			return obj[0];
		}
		return null;
	}

	/**
	 * @Title: getLocalCacheForString
	 * @Description: 根据key获取本地缓存值——String类型
	 * @param key
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:43:12
	 */
	public String getLocalCacheForString(String key) {
		Object value = this.getLocalCache(key);
		if (null != value) {
			return JSON.toJSONString(value);
		}
		return null;
	}

	/**
	 * @Title: getCreateTime
	 * @Description: 获取本地缓存创建时间——Date类型
	 * @param key
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:43:39
	 */
	public Date getCreateTime(String key) {
		Object[] obj = (Object[]) LocalCache.getInstance().getIfPresent(key);
		if (obj != null && obj.length > 0) {
			return (Date) obj[1];
		}
		return null;
	}

	/**
	 * @Title: getCreateTimeForString
	 * @Description: 获取本地缓存创建时间——String类型
	 * @param key
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:43:51
	 */
	public String getCreateTimeForString(String key) {
		Date createTime = this.getCreateTime(key);
		if (null != createTime) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sdf.format(createTime);
		}
		return null;
	}

	/**
	 * @Title: getLocalExpiration
	 * @Description: 获取本地缓存失效时间
	 * @param key
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:44:24
	 */
	public long getLocalExpiration(String key) {
		Object[] obj = (Object[]) LocalCache.getInstance().getIfPresent(key);
		if (obj != null && obj.length > 0) {
			return Long.parseLong(obj[2].toString());
		}
		return 0;
	}

	/**
	 * @Title: putLocalCache
	 * @Description: 存放本地缓存
	 * @param key
	 * @param value
	 * @param createTime
	 * @param localExpiration
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:44:35
	 */
	public void putLocalCache(String key, Object value, Date createTime, long localExpiration) {
		Object[] object = new Object[3];
		object[0] = value;
		object[1] = createTime;
		object[2] = localExpiration;
		LocalCache.getInstance().put(key, object);
	}

	/**
	 * @Title: delLocalCache
	 * @Description: 根据key清除本地缓存
	 * @param key
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:44:50
	 */
	public void delLocalCache(String key) {
		LocalCache.getInstance().invalidate(key);
	}

	/**
	 * @Title: delManyLocalCache
	 * @Description: 批量清除本地缓存(字符串数组)
	 * @param keys
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:45:01
	 */
	public void delManyLocalCache(String[] keys) {
		final String[] fkeys = keys;
		LocalCache.getInstance().invalidateAll(new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				List<String> lst = Arrays.asList(fkeys);
				return lst.iterator();
			}
		});
	}

	/**
	 * @Title: delAllLocalCache
	 * @Description: 清除本地所有缓存
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:45:11
	 */
	public void delAllLocalCache() {
		LocalCache.getInstance().invalidateAll();
	}

	/**
	 * @Title: createLocalCache
	 * @Description: 重新构建本地缓存对象
	 * @param maximumSize
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:45:23
	 */
	public void createLocalCache(long maximumSize) {
		new LocalCache(maximumSize);
	}

	/**
	 * @Title: updateLocalExpiration
	 * @Description: 设置本地缓存失效时间
	 * @param key
	 * @param localExpiration
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:47:12
	 */
	public void updateLocalExpiration(String key, long localExpiration) {
		Object[] obj = (Object[]) LocalCache.getInstance().getIfPresent(key);
		if (obj != null && obj.length > 0) {
			this.putLocalCache(key, obj[0], (Date) obj[1], localExpiration);
		}
	}

	/**
	 * @Title: getRemainTime
	 * @Description: 获取本地缓存剩余时间
	 * @param key
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:47:22
	 */
	public long getRemainTime(String key) {
		long createTime = this.getCreateTime(key).getTime();
		long localExpiration = this.getLocalExpiration(key);
		long nowTime = new Date().getTime();
		long cTime = createTime + localExpiration * 1000 - nowTime;
		if (cTime >= 0) {
			return cTime;
		}
		return 0;
	}

	/**
	 * @Title: getKeysSize
	 * @Description: 获取本地缓存中key数量
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:47:34
	 */
	public long getKeysSize() {
		Set<String> keys = LocalCache.getInstance().asMap().keySet();
		if (null != keys) {
			return keys.size();
		}
		return 0;
	}

	/**
	 * @Title: getCacheStats
	 * @Description: 获得所有缓存的命中统计
	 * @return
	 * @author li.shangzhi
	 * @date 2017年4月10日 上午9:47:44
	 */
	public String getCacheStats() {
		CacheStats stats = LocalCache.getInstance().stats();
		StringBuffer sb = new StringBuffer(200);
		sb.append("[CacheStats]").append("requestCount=").append(stats.requestCount()).append(","); // 返回Cache的lookup方法查找缓存的次数，不论查找的值是否被缓存
		sb.append("hitCount=").append(stats.hitCount()).append(","); // 返回Cache的lookup方法命中缓存的次数
		sb.append("hitRate=").append(stats.hitRate()).append(",");// 返回缓存请求的命中率，命中次数除以请求次数
		sb.append("missCount=").append(stats.missCount()).append(",");// 返回缓存请求的未命中的次数
		sb.append("missRate=").append(stats.missRate()).append(",");// 返回缓存请求未命中的比率，未命中次数除以请求次数
		sb.append("evictionCount=").append(stats.evictionCount());// 返回缓存中条目被移除的次数
		return sb.toString();
	}

	public static void main(String[] args) throws Exception {
		new Thread(new Runnable() {
			@Override
			public void run() {
				LocalCacheUtil localcacheUtil = LocalCacheUtil.getInstance();
				localcacheUtil.putLocalCache("jimi1", 1, new Date(), 5 * 60);
				localcacheUtil.putLocalCache("jimi2", 2, new Date(), 5 * 60);
				System.out.println("keys_jimi1:" + localcacheUtil.getLocalExpiration("jimi1"));
				System.out.println("keys_jimi2:" + localcacheUtil.getLocalExpiration("jimi2"));
				System.out.println("value_jimi1:" + localcacheUtil.getLocalCache("jimi1"));
				System.out.println("value_jimi2:" + localcacheUtil.getLocalCache("jimi2"));

				localcacheUtil.updateLocalExpiration("jimi1", 200);
				System.out.println("keys_jimi1:" + localcacheUtil.getLocalExpiration("jimi1"));
				System.out.println("keys_jimi2:" + localcacheUtil.getLocalExpiration("jimi2"));
				System.out.println("value_jimi1:" + localcacheUtil.getLocalCache("jimi1"));
				System.out.println("value_jimi2:" + localcacheUtil.getLocalCache("jimi2"));
				System.out.println("time_jimi1:" + localcacheUtil.getRemainTime("jimi1"));
				System.out.println("time_jimi2:" + localcacheUtil.getRemainTime("jimi2"));
				System.out.println("keysize:" + localcacheUtil.getKeysSize());

				localcacheUtil.delLocalCache("jimi1");
				System.out.println("keysize:" + localcacheUtil.getKeysSize());
			}
		}).start();
	}
}