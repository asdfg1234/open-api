/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.cache.local;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

/**
 * @FileName LocalCache.java
 * @Description: 本地缓存
 *
 * @Date 2017年4月10日 上午9:31:27
 * @author li.shangzhi
 * @version 1.0
 */
public class LocalCache {

	private static final Logger logger = LoggerFactory.getLogger(LocalCache.class);
	private static Cache<String, Object> localCache = null;

	public static Cache<String, Object> getInstance() {
		if (localCache == null) {
			localCache = CacheBuilder.newBuilder()
			// 设置大小，条目数
					.maximumSize(30000)
					// // 设置失效时间，创建时间
					// .expireAfterWrite(1000, TimeUnit.SECONDS)
					// // 设置时效时间，最后一次被访问
					// .expireAfterAccess(1000, TimeUnit.SECONDS)
					// 设置要统计缓存的命中率
					.recordStats()
					// 移除缓存的移除通知
					.removalListener(new RemovalListener<String, Object>() {
						@Override
						public void onRemoval(RemovalNotification<String, Object> notification) {
							logger.debug("本地缓存移除,key=" + notification.getKey());
						}
					}).build();
		}
		return localCache;
	}

	/**
	 * 构造方法-可设置大小
	 * 
	 * @param maximumSize
	 */
	public LocalCache(long maximumSize) {
		localCache.cleanUp();
		localCache = CacheBuilder.newBuilder()
		// 设置大小，条目数
				.maximumSize(maximumSize)
				// 设置要统计缓存的命中率
				.recordStats()
				// 移除缓存的移除通知
				.removalListener(new RemovalListener<String, Object>() {
					@Override
					public void onRemoval(RemovalNotification<String, Object> notification) {
						logger.debug("本地缓存移除,key=" + notification.getKey());
					}
				}).build();
	}

}
