/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
*/

package com.jimi.framework.cache.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import redis.clients.jedis.JedisPoolConfig;

/**
 * @FileName RedisConfiguration.java
 * @Description: redis配置
 *
 * @Date 2017年4月10日 上午9:21:26
 * @author li.shangzhi
 * @version 1.0
 */
@Configuration
public class RedisConfiguration implements EnvironmentAware{

	private Logger logger = LoggerFactory.getLogger(RedisConfiguration.class);
	
	private static String host="host";
	private static String port="port";
	//Redis服务器连接密码（默认为空）
	private static String password="password";
	//Redis数据库索引（默认为0）
	private static String database="database";
	//离线指令存储库
	private static String cmdDbIndex="cmd.dbindex";
	//最大分配的对象数
	private static String poolMaxActive="pool.maxActive";
	//最小能够保持idle状态的对象数
	private static String poolMinIdle="pool.minIdle";
	//最大能够保持idle状态的对象数
	private static String poolMaxIdle="pool.maxIdle";
	//当池内没有返回对象时，最大等待时间
	private static String poolMaxWait="pool.maxWait";
	//连接超时时间（毫秒）
	private static String timeout="timeout";
	//当调用borrow Object方法时，是否进行有效性检查
	private static String poolTestOnBorrow="pool.testOnBorrow";

	private RelaxedPropertyResolver redisResolver;
	
	private RelaxedPropertyResolver tuqiangRedisResolver;
	
	@Override
	public void setEnvironment(Environment environment) {
		this.redisResolver = new RelaxedPropertyResolver(environment, "api.redis.");
		
		this.tuqiangRedisResolver = new RelaxedPropertyResolver(environment, "tuqiang.redis.");
	}
	
	/**
	 * @Title: jedisPoolConfig 
	 * @Description: 开放API使用的redis连接池
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年8月9日 下午3:11:54
	 */
	@Bean(name = "jedisPoolConfig")
	@Primary
	public JedisPoolConfig jedisPoolConfig() {
		logger.debug("Configruing API JedisPoolConfig");
		JedisPoolConfig jedisPool = new JedisPoolConfig();
		loadJedisPoolConfig(jedisPool, redisResolver);
		return jedisPool;
	}
	
	/**
	 * @Title: jedisConnectionFactory 
	 * @Description: 开放API使用的redis连接工厂
	 * @param jedisPoolConfig
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年8月9日 下午3:12:24
	 */
	@Bean(name = "jedisConnectionFactory")
	@Primary
	public JedisConnectionFactory jedisConnectionFactory(@Qualifier("jedisPoolConfig") JedisPoolConfig jedisPoolConfig) {
		logger.debug("Configruing API JedisConnectionFactory");
		JedisConnectionFactory jedisConnection = new JedisConnectionFactory();
		loadJedisConnectionFactory(jedisConnection, redisResolver);
		jedisConnection.setPoolConfig(jedisPoolConfig);
		jedisConnection.setUsePool(true);
		return jedisConnection;
	}
	
	/**
	 * @Title: tuqiangJedisPoolConfig 
	 * @Description: 途强使用的redis连接池
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年8月9日 下午3:12:46
	 */
	@Bean(name = "tuqiangJedisPoolConfig")
	public JedisPoolConfig tuqiangJedisPoolConfig() {
		logger.debug("Configruing tuqiang JedisPoolConfig");
		JedisPoolConfig jedisPool = new JedisPoolConfig();
		loadJedisPoolConfig(jedisPool, tuqiangRedisResolver);
		return jedisPool;
	}
	
	/**
	 * @Title: tuqiangJedisConnectionFactory 
	 * @Description: 途强使用的redis连接工厂，用于获取心跳和GPS
	 * @param jedisPoolConfig
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年8月9日 下午3:13:11
	 */
	@Bean(name = "tuqiangJedisConnectionFactory")
	public JedisConnectionFactory tuqiangJedisConnectionFactory(@Qualifier("tuqiangJedisPoolConfig") JedisPoolConfig jedisPoolConfig) {
		logger.debug("Configruing tuqiang JedisConnectionFactory");
		JedisConnectionFactory jedisConnection = new JedisConnectionFactory();
		loadJedisConnectionFactory(jedisConnection, tuqiangRedisResolver);
		jedisConnection.setPoolConfig(jedisPoolConfig);
		jedisConnection.setUsePool(true);
		
		jedisConnection.setDatabase(Integer.valueOf(tuqiangRedisResolver.getProperty(database)));
		return jedisConnection;
	}
	
	/**
	 * @Title: tuqiangCmdJedisConnectionFactory 
	 * @Description: 途强使用的redis连接工厂，用于指令操作
	 * @param jedisPoolConfig
	 * @return 
	 * @author li.shangzhi
	 * @date 2017年8月9日 下午3:13:45
	 */
	@Bean(name = "tuqiangCmdJedisConnectionFactory")
	public JedisConnectionFactory tuqiangCmdJedisConnectionFactory(@Qualifier("tuqiangJedisPoolConfig") JedisPoolConfig jedisPoolConfig) {
		logger.debug("Configruing tuqiang JedisConnectionFactory");
		JedisConnectionFactory jedisConnection = new JedisConnectionFactory();
		loadJedisConnectionFactory(jedisConnection, tuqiangRedisResolver);
		jedisConnection.setPoolConfig(jedisPoolConfig);
		jedisConnection.setUsePool(true);
		
		jedisConnection.setDatabase(Integer.valueOf(tuqiangRedisResolver.getProperty(cmdDbIndex)));
		return jedisConnection;
	}
	
	private void loadJedisPoolConfig(JedisPoolConfig jedisPool, RelaxedPropertyResolver resolver) {
		jedisPool.setMaxTotal(Integer.valueOf(resolver.getProperty(poolMaxActive)));
		jedisPool.setMinIdle(Integer.valueOf(resolver.getProperty(poolMinIdle)));
		jedisPool.setMaxIdle(Integer.valueOf(resolver.getProperty(poolMaxIdle)));
		jedisPool.setMaxWaitMillis(Integer.valueOf(resolver.getProperty(poolMaxWait)));
		jedisPool.setTestOnBorrow(Boolean.valueOf(resolver.getProperty(poolTestOnBorrow)));
	}
	
	private void loadJedisConnectionFactory(JedisConnectionFactory jedisConnection, RelaxedPropertyResolver resolver) {
		jedisConnection.setHostName(resolver.getProperty(host));
		jedisConnection.setPort(Integer.valueOf(resolver.getProperty(port)));
		jedisConnection.setPassword(resolver.getProperty(password));
		jedisConnection.setTimeout(Integer.valueOf(resolver.getProperty(timeout)));
	}
	
	@Bean(name = "stringRedisSerializer")
	public StringRedisSerializer stringRedisSerializer() {
		return new StringRedisSerializer();
	}
	
	@Bean(name = "jsonRedisSerializable")
	public JsonRedisSerializable jsonRedisSerializable() {
		return new JsonRedisSerializable();
	}
	
	@Bean(name = "redisTemplate")
	@Primary
	public RedisTemplate redisTemplate(
			@Qualifier("jedisConnectionFactory") JedisConnectionFactory jedisConnectionFactory,
			@Qualifier("stringRedisSerializer") StringRedisSerializer stringRedisSerializer,
			@Qualifier("jsonRedisSerializable") JsonRedisSerializable jsonRedisSerializable) {
		jedisConnectionFactory.setDatabase(Integer.valueOf(redisResolver.getProperty(database)));
		RedisTemplate redisTemplate = new RedisTemplate();
		redisTemplate.setConnectionFactory(jedisConnectionFactory);
		redisTemplate.setKeySerializer(stringRedisSerializer);
		redisTemplate.setValueSerializer(jsonRedisSerializable);
		redisTemplate.setHashKeySerializer(stringRedisSerializer);
		redisTemplate.setHashValueSerializer(stringRedisSerializer);
		return redisTemplate;
	}
	
	@Bean(name = "tuqiangRedisTemplate")
	public RedisTemplate tuqiangRedisTemplate0(
			@Qualifier("tuqiangJedisConnectionFactory") JedisConnectionFactory jedisConnectionFactory,
			@Qualifier("stringRedisSerializer") StringRedisSerializer stringRedisSerializer,
			@Qualifier("jsonRedisSerializable") JsonRedisSerializable jsonRedisSerializable) {
		RedisTemplate redisTemplate = new RedisTemplate();
		redisTemplate.setConnectionFactory(jedisConnectionFactory);
		redisTemplate.setKeySerializer(stringRedisSerializer);
		redisTemplate.setValueSerializer(jsonRedisSerializable);
		redisTemplate.setHashKeySerializer(stringRedisSerializer);
		redisTemplate.setHashValueSerializer(stringRedisSerializer);
		return redisTemplate;
	}
	
	@Bean(name = "tuqiangCmdRedisTemplate")
	public RedisTemplate tuqiangRedisTemplate10(
			@Qualifier("tuqiangCmdJedisConnectionFactory") JedisConnectionFactory jedisConnectionFactory,
			@Qualifier("stringRedisSerializer") StringRedisSerializer stringRedisSerializer,
			@Qualifier("jsonRedisSerializable") JsonRedisSerializable jsonRedisSerializable) {
		RedisTemplate redisTemplate = new RedisTemplate();
		redisTemplate.setConnectionFactory(jedisConnectionFactory);
		redisTemplate.setKeySerializer(stringRedisSerializer);
		redisTemplate.setValueSerializer(jsonRedisSerializable);
		redisTemplate.setHashKeySerializer(stringRedisSerializer);
		redisTemplate.setHashValueSerializer(stringRedisSerializer);
		return redisTemplate;
	}
	
}
