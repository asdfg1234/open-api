/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月10日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.cache.redis;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.util.Assert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @FileName JsonRedisSerializable.java
 * @Description: fastJson序列化处理
 *
 * @Date 2017年4月10日 上午9:16:26
 * @author li.shangzhi
 * @version 1.0
 * @param <T>
 */
public class JsonRedisSerializable<T> implements RedisSerializer<T> {

	private final Charset charset;

	public JsonRedisSerializable() {
		this(Charset.forName("UTF8"));
	}

	public JsonRedisSerializable(Charset charset) {
		Assert.notNull(charset);
		this.charset = charset;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T deserialize(byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		String string = new String(bytes, charset);
		T result = (T) JSON.parse(string);
		if (result instanceof JSONArray) {
			List<T> tempList = new ArrayList<T>();
			Iterator<T> iterator = ((List<T>) result).iterator();
			while (iterator.hasNext()) {
				T t = iterator.next();
				tempList.add(t);
			}
			result = (T) tempList;
		}
		return (string == null ? null : result);
	}

	@Override
	public byte[] serialize(T object) {
		if (object == null) {
			return null;
		}
		String string = JSON.toJSONString(object, SerializerFeature.WriteClassName);
		return (string == null ? null : string.getBytes(charset));
	}

}
