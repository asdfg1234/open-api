/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.annotation.cache;

import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.RedisTemplate;

import com.jimi.framework.context.SpringContextHolder;

/**
 * @FileName RedisCacheUtil.java
 * @Description: 
 *
 * @Date 2017年4月25日 下午7:04:09
 * @author li.shangzhi
 * @version 1.0
 */
public class RedisCacheUtil {

	private static RedisCacheUtil commentRedisCacheUtil;
	private static RedisTemplate<String, Object> redisTemplate;

	private RedisCacheUtil() {
	};

	public static RedisCacheUtil getInstance() {
		if (commentRedisCacheUtil == null) {
			commentRedisCacheUtil = new RedisCacheUtil();
			redisTemplate = SpringContextHolder.getBean("redisTemplate");
		}
		return commentRedisCacheUtil;
	}

	public void put(String key, Object value, String[] nameSpace, long expiration) {
		redisTemplate.opsForValue().set(key, value, expiration, TimeUnit.SECONDS);
	}

	public Object get(String key, String[] nameSpace) {
		return redisTemplate.opsForValue().get(key);
	}

}
