/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月11日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.annotation.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @FileName ReqFilter.java
 * @Description: 接口请求拦截器(防重复、大并发、业务锁定等请求拦截)
 *
 * @Date 2017年4月11日 上午10:51:50
 * @author li.shangzhi
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Inherited
public @interface ReqFilter {

	/**
	 * key表达式(使用SPEL)
	 */
	String keySpel();

	/**
	 * 禁用时间(时间单位：毫秒),默认900000=15分钟
	 */
	int time() default 900000;

	/**
	 * 限制频率数,默认4000000
	 */
	int limit() default 4000000;

}