/*
 * COPYRIGHT. ShenZhen JiMi Technology Co., Ltd. 2017.
 * ALL RIGHTS RESERVED.
 *
 * No part of this publication may be reproduced, stored in a retrieval system, or transmitted,
 * on any form or by any means, electronic, mechanical, photocopying, recording, 
 * or otherwise, without the prior written permission of ShenZhen JiMi Network Technology Co., Ltd.
 *
 * Amendment History:
 * 
 * Date                   By              Description
 * -------------------    -----------     -------------------------------------------
 * 2017年4月25日    li.shangzhi         Create the class
 * http://www.jimilab.com/
 */

package com.jimi.framework.bean;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.jimi.framework.logger.Logger;
import com.jimi.framework.logger.LoggerFactory;

/**
 * @FileName BeanUtil.java
 * @Description: 对象拷贝工具类
 *
 * @Date 2017年4月25日 下午5:13:56
 * @author li.shangzhi
 * @version 1.0
 */
public class BeanUtil {

	protected final static Logger logger = LoggerFactory.getLogger(BeanUtil.class);

	/**
	 * One type of List, converting to other types of List
	 * 
	 * @param srcList
	 * @param destType
	 * @return
	 */
	public static List<?> convertBeanList(List<?> srcList, Class<?> destType) {
		if (srcList == null) {
			return new ArrayList<Object>();
		}
		List<Object> list = new ArrayList<Object>();
		for (Object obj : srcList) {
			Object destObj = convertBean(obj, destType);
			if (destObj != null) {
				list.add(destObj);
			}
		}
		return list;
	}

	/**
	 * A type of object, cast to another type of object , And copies of the same
	 * attributes inside
	 * 
	 * @param src
	 * @param type
	 * @return
	 */
	public static Object convertBean(Object src, Class<?> type) {
		if (src == null) {
			return null;
		}
		Object desObj = null;
		try {
			desObj = type.newInstance();
			copyBean(src, desObj);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return desObj;
	}

	/**
	 * Copies of the same name as the property of the bean
	 * 
	 * @param src
	 * @param dest
	 */
	public static void copyBean(Object src, Object dest) {
		try {
			BeanUtils.copyProperties(src, dest);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object getPropertyValue(Object src, String property) {
		Object result = null;
		try {
			Class cls = src.getClass();
			property = property.substring(0, 1).toUpperCase() + property.substring(1, property.length());
			Method method = cls.getMethod("get" + property, null);

			if (method != null) {
				result = method.invoke(src, null);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}

}
